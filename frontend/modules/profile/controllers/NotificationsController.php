<?php

namespace frontend\modules\profile\controllers;

use Yii;
use common\models\Notification;
use frontend\modules\profile\models\NotificationsSearch;
use frontend\modules\profile\models\NotificationsUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use common\models\EventsBookings;
use common\models\NotificationUser;

/**
 * NotificationsController implements the CRUD actions for Notification model.
 */
class NotificationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

        
    public function beforeAction($action) {
        if ((\Yii::$app->request->pathInfo != 'profile/default/account-deleted') && Yii::$app->user->identity->checkDeleteAccount()) {
            return $this->redirect('/profile/default/account-deleted');
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationsUserSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->load(Yii::$app->request->queryParams);

        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionBookingChangeStatus($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findEBModel($id);
        $modelN = $this->findNUModel(Yii::$app->request->get('not_id'));
        
        $status = false;
        if (Yii::$app->request->get('type') == 'active') {
            $model->status = EventsBookings::STATUS_CONFIRM;
            $status = true;
        } elseif (Yii::$app->request->get('type') == 'deactive') {
            $model->status = EventsBookings::STATUS_REJECTED;
            $status = true;
        }

        $model->save();
        $modelN->status = Notification::STATUS_READ;
        $modelN->save();

        return ['status' => $status];
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findNUModel($id);
        if ($model->status != NotificationUser::STATUS_READ) {
            $model->status = NotificationUser::STATUS_READ;
            $model->save();
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notification();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findNUModel($id)
    {
        if (($model = \common\models\NotificationUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findEBModel($id)
    {
        if (($model = EventsBookings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
