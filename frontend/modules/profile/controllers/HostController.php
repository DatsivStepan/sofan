<?php

namespace frontend\modules\profile\controllers;

use Yii,
    yii\filters\AccessControl,
    yii\web\Controller,
    common\helpers\ImageHelper,
    common\helpers\UploadHelper,
    common\models\Host,
    frontend\models\HostForm,
    yii\web\Response;

class HostController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    public function beforeAction($action) {
        if ((\Yii::$app->request->pathInfo != 'profile/default/account-deleted') && Yii::$app->user->identity->checkDeleteAccount()) {
            return $this->redirect('/profile/default/account-deleted');
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Display createHost page and hostCreated page
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param int $step
     * @return string
     */
    public function actionUpdate($id, $step = 1)
    {
        $modelH = $this->findModel($id);
        $return = false;
        $model = new HostForm();
        $model->setAttributes($modelH->getAttributes());
        switch ($step) {
            case 1 :
                $model->scenario = HostForm::SCENARIO_ADDRESS;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        $step++;
                        $return = true;
                    }
                }
                break;
            case 2 :
                $model->scenario = HostForm::SCENARIO_LOCATION;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        $model->saveImages($model->image);
                        $step++;
                        $return = true;
                    }
                }
                break;
            case 3 :
                $model->scenario = HostForm::SCENARIO_DEVICE;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        $model->saveImages($model->image);
                        $step++;
                        $return = true;
                    }
                }
                break;
            case 4 :
                $model->scenario = HostForm::SCENARIO_COMFORT;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        Yii::$app->session->setFlash('saveSuccess');
                        return $this->redirect(['/profile/default/hosts']);
                    }
                }
                break;
        }

        if ($return) {
            return $this->redirect(['update',
                'id' => $model->id,
                'step' => $step
            ]);
        }
        if ($step <= 4) {
            Yii::$app->params['hostStep'] = $step;
        }

        return $this->render('update', [
            'model' => $model,
            'step' => $step
        ]);
    }

    /**
     * Create host in 4 steps
     * Each step saves to session
     *
     * @param int $step
     * @return string
     */
    public function actionCreate($step = 1)
    {
        Yii::$app->params['with_out_footer'] = true;

        $post = Yii::$app->request->post();
        if ((isset($post['HostForm']) && isset($post['HostForm']['step']))) {
            $step = $post['HostForm']['step'];
        }

        $session = Yii::$app->getSession();

        $model = new HostForm();
        $model->step = $step;

        if (!isset($session['host_form_data'])) {
            $session['host_form_data'] = [
                'user_id' => Yii::$app->user->identity->id,
                'image' => ''
            ];
        } else {
            $model->setAttributes($session['host_form_data']);
        }

        switch ($step) {
            case 1 :

                if ($model->load(Yii::$app->request->post())) {
                    $model->setScenario(HostForm::SCENARIO_ADDRESS);
                    if ($model->validate()) {

                        $session['host_form_data'] = array_merge(
                            $session['host_form_data'], [
                                'country' => $model->country,
                                'city' => $model->city,
                                'lat' => $model->lat,
                                'lng' => $model->lng,
                                'street_number' => $model->street_number,
                                'street_name' => $model->street_name,
                                'zip_code' => $model->zip_code,
                                'province' => $model->province,
                            ]
                        );
                        $model->step++;
                        if ($step <= 4) {
                            Yii::$app->params['hostStep'] = $model->step;
                        }

                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                }
                break;

            case 2 :

                if ($model->load(Yii::$app->request->post())) {
                    $model->setScenario(HostForm::SCENARIO_LOCATION);
                    if ($model->validate()) {

                        $session['host_form_data'] = array_merge(
                            $session['host_form_data'], [
                                'location_type' => $model->location_type,
                                'location_seats' => $model->location_seats,
                                'location_size' => $model->location_size,
                                'location_pets' => $model->location_pets,
                                'image' => $model->image,
                            ]
                        );
                        $model->step++;
                        if ($step <= 4) {
                            Yii::$app->params['hostStep'] = $model->step;
                        }

                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                }
                break;

            case 3 :

                if ($model->load(Yii::$app->request->post())) {
                    $model->setScenario(HostForm::SCENARIO_DEVICE);
                    if ($model->validate()) {

                        $session['host_form_data'] = array_merge(
                            $session['host_form_data'], [
                                'device_type' => $model->device_type,
                                'device_size' => $model->device_size,
                                'device_resolution' => $model->device_resolution,
                                'image' => $model->image,
                            ]
                        );
                        $model->step++;
                        if ($step <= 4) {
                            Yii::$app->params['hostStep'] = $model->step;
                        }

                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                }
                break;

            case 4 :

                if ($model->load(Yii::$app->request->post())) {
                    $model->setScenario(HostForm::SCENARIO_COMFORT);
                    if ($model->validate()) {

                        $session['host_form_data'] = array_merge(
                            $session['host_form_data'], [
                                'wifi' => $model->wifi,
                                'food' => $model->food,
                                'balcony' => $model->balcony,
                                'conditioner' => $model->conditioner,
                                'title' => $model->title,
                            ]
                        );

                        if ($model->save()) {
                            unset($session['host_form_data']);

                            return $this->render('index', [
                                'show_success_screen' => true,
                                'model' => $model
                            ]);
                        }
                    }
                }
        }
        if ($step <= 4) {
            Yii::$app->params['hostStep'] = $model->step;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Upload host image
     * @return string
     */
    public function actionUploadImg()
    {
        $result = [];
        list($success, $rez) = UploadHelper::saveImage(Host::IMAGE_UPLOAD_PATH);
        if ($success) {
            foreach ($rez as $r) {

                $file = UploadHelper::getUploadPath(Host::IMAGE_UPLOAD_PATH . "/" . $r[1]);
                ImageHelper::CropToFit($file, $file, 400, 400);
                $url = UploadHelper::getUploadUrl(Host::IMAGE_UPLOAD_PATH . '/' . $r[1]);
                $result[] = ['img' => $r[1], 'src' => $url];
            }
            return json_encode(['success' => 1, 'result' => $result]);
        } else return json_encode(['success' => 0, 'error' => $rez]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionGetHostData($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $data = [
            'street_number' => $model->street_number,
            'street_name' => $model->street_name,
            'lat' => $model->lat,
            'lng' => $model->lng,
        ];

        return ['status' => true, 'data' => $data];
    }

    public function actionDelete($id)
    {
        $this->layout = '/modal';
        $model = $this->findModel($id);
        $model->scenario = 'delete';

        if ($model->load(Yii::$app->request->post())){
            $type = Yii::$app->request->post('type');
            if ($model->eventsModel && ($type == 'question')) {
                    return $this->render('_delete_e', [
                    'model' => $model,
                ]);
            } else {
                $model->deleteWithLog();
            }
            Yii::$app->session->setFlash('success');
        }

        return $this->render('_delete_q', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Host model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Host the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Host::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
