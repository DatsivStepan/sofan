<?php

namespace frontend\modules\profile\controllers;

use Yii;
use common\models\Messages;
use frontend\modules\profile\models\MessagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\MessagesDialog;
use yii\filters\AccessControl;
/**
 * MessagesController implements the CRUD actions for messages model.
 */
class MessagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    public function beforeAction($action) {
        if ((\Yii::$app->request->pathInfo != 'profile/default/account-deleted') && Yii::$app->user->identity->checkDeleteAccount()) {
            return $this->redirect('/profile/default/account-deleted');
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionCheckDialog($id)
    {
        $user = $this->findUserModel($id);
        $model = MessagesDialog::find()->where([
            'OR',
            ['from_user_id' => $id, 'to_user_id' => Yii::$app->user->id],
            ['from_user_id' => Yii::$app->user->id, 'to_user_id' => $id],
        ])->one();

        if (!$model) {
            $model = new MessagesDialog();
            $model->from_user_id = Yii::$app->user->id;
            $model->to_user_id = $id;
            if ($model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            }
        } else {
            return $this->redirect(['index', 'id' => $model->id]);
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    /**
     * Lists all messages models.
     * @return mixed
     */
    public function actionIndex($id=null)
    {
        $modelI = $this->findUserModel(Yii::$app->user->id);
        $modelNew = new Messages();
        if ($modelNew->load(Yii::$app->request->post())) {
            if ($modelNew->save()) {
                $modelNew = new Messages();
            }
        } 

        $dialogs = $modelI->getAllDialogs();
        $id = $id ? $id : (array_key_exists(0, $dialogs) ? $dialogs[0]->id : null);
        $model = MessagesDialog::findOne($id);
        if ($model) {
            $model->setReadAll($modelI->id);
        }
        
        $searchModel = new MessagesSearch();
        $searchModel->dialog_id = $model->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'modelI' => $modelI,
            'model' => $model,
            'modelNew' => $modelNew,
            'dialogs' => $dialogs,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single messages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new messages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new messages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing messages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing messages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the messages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return messages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Messages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findUserModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
