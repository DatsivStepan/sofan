<?php

namespace frontend\modules\profile\controllers;

use Yii;
use common\models\Events;
use frontend\modules\profile\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use common\models\EventsBookings;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsBookingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    public function beforeAction($action) {
        if ((\Yii::$app->request->pathInfo != 'profile/default/account-deleted') && Yii::$app->user->identity->checkDeleteAccount()) {
            return $this->redirect('/profile/default/account-deleted');
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex($type = null)
    {
        $searchModel = new EventsSearch();
 
        $page = 'index';
        $statusB = [EventsBookings::STATUS_CONFIRM, EventsBookings::STATUS_ON_CONFIRM, EventsBookings::STATUS_REJECTED];
        $status = Events::STATUS_ACTIVE;
        switch ($type) {
            case 'canceled':
                $page = 'canceled';
                $statusB = [EventsBookings::STATUS_REJECTED, EventsBookings::STATUS_ON_CONFIRM];
                $status = Events::STATUS_NOT_ACTIVE;
                break;
            case 'pass':
                $page = 'pass';
                $statusB = EventsBookings::STATUS_CONFIRM;
                $status = Events::STATUS_NOT_ACTIVE;
                break;
        }

        $searchModel->load(Yii::$app->request->queryParams);
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->status = $status;
        $searchModel->statusB = $statusB;
        $dataProvider = $searchModel->searchBookings();

        return $this->render($page, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findBookingModel($id)
    {
        if (($model = EventsBookings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
