<?php

namespace frontend\modules\profile\controllers;

use Yii,
    yii\filters\AccessControl,
    yii\web\Controller,
    yii\web\Response,
    frontend\modules\profile\models\FeedbacksSearch;

class FeedbackController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    public function beforeAction($action) {
        if ((\Yii::$app->request->pathInfo != 'profile/default/account-deleted') && Yii::$app->user->identity->checkDeleteAccount()) {
            return $this->redirect('/profile/default/account-deleted');
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * @return string
     */
    public function actionIndex($type = 'from')
    {
        $searchModel = new FeedbacksSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        if ($type == 'to') {
            $searchModel->filter_to = Yii::$app->user->id;
        } else {
            $searchModel->user_id = Yii::$app->user->id;
        }

        $dataProvider = $searchModel->search();

        return $this->render($type, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
