<?php

namespace frontend\modules\profile\controllers;

use Yii;
use common\models\Events;
use frontend\modules\profile\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use common\models\EventsBookings;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    public function beforeAction($action) {
        if ((\Yii::$app->request->pathInfo != 'profile/default/account-deleted') && Yii::$app->user->identity->checkDeleteAccount()) {
            return $this->redirect('/profile/default/account-deleted');
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex($type = null)
    {
        $searchModel = new EventsSearch();
 
        $page = 'index';
        $status = Events::STATUS_ACTIVE;
        switch ($type) {
            case 'canceled':
                $page = 'canceled';
                $status = Events::STATUS_DELETED;
                break;
            case 'pass':
                $page = 'pass';
                $status = Events::STATUS_NOT_ACTIVE;
                break;
        }

        $searchModel->load(Yii::$app->request->queryParams);
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->status = $status;
        $dataProvider = $searchModel->search();

        return $this->render($page, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null, $step = 1)
    {
        if (!Yii::$app->user->identity->getHostCount()) {
            return $this->redirect(['/profile/host/']);
        }

        $model = $id ? $this->findModel($id) : new Events();
        $model->step = $step;

        $return = false;
        switch ($step) {
            case 1 :
                $model->scenario = Events::SCENARIO_STEP_EVENT;
                $model->user_id = Yii::$app->user->id;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        $step++;
                        $return = true;
                    }
                }
                break;
            case 2 :
                $model->scenario = Events::SCENARIO_STEP_LOCATION;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        $step++;
                        $return = true;
                    }
                }
                break;
            case 3 :
                $model->scenario = Events::SCENARIO_STEP_REG_AND_PAY;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        $step++;
                        $return = true;
                    }
                }
                break;
            case 4 :
                $model->scenario = Events::SCENARIO_STEP_COMFORT_AND_PREFERENCES;
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->saveStepData()) {
                        $step++;
                        $return = true;
                    }
                }
                break;
        }
        if ($step < 5) {
            Yii::$app->params['eventStep'] = $step;
        }

        if ($return) {
            return $this->redirect(['create',
                'id' => $model->id,
                'step' => $step
            ]);
        }
        

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionBookingConfirm($id)
    {
        $this->layout = '/modal';
        $model = $this->findBookingModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveWithLog(EventsBookings::STATUS_CONFIRM)) {
            Yii::$app->session->setFlash('success');
        }

        return $this->render('_booking-confirm', [
            'model' => $model,
        ]);
    }
    
    public function actionGetTvShowData($id)
    {
        $res = ['status' => false, 'date' => null, 'time' => null];
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = \common\models\TvShow::findOne($id);
        if ($model && $model->category_id == 1) {
            $res = [
                'status' => true,
                'date' => $model->date_start,
                'time' => date('H:i', strtotime(date('Y-m-d') . ' ' . $model->time_start))
            ];
        }
        return $res;
    }

    public function actionBookingDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findBookingModel($id);
        $model->status = EventsBookings::STATUS_REJECTED;
        return ['status' => $model->saveWithLog(EventsBookings::STATUS_REJECTED)];
    }
    
    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $step = 1)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['status' => $this->findModel($id)->delete()];
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findBookingModel($id)
    {
        if (($model = EventsBookings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
