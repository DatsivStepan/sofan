<?php

namespace frontend\modules\profile\controllers;

use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\modules\profile\models\HostsSearch;
use yii\web\Response;
use common\models\UserInfo;
use common\models\NotificationUser;
use common\models\Messages;
use common\models\MessagesDialog;
use common\models\VerifyPhone;
use common\models\UserVerification;
use common\models\EmailSender;
use common\models\AccountDelete;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        if ((\Yii::$app->request->pathInfo != 'profile/default/account-deleted') && Yii::$app->user->identity->checkDeleteAccount()) {
            return $this->redirect('/profile/default/account-deleted');
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actions()
    {
        return [
            'uploadPhoto' => [
                'class' => 'budyaga\cropper\actions\UploadAction',
                'url' => '/images/users',
                'path' => '@frontend/web/images/users',
            ]
        ];
    }

    public function actionAvatar()
    {
        $model = $this->findModel();
        $modelUserInfo = UserInfo::findOne(['user_id' => $model->id]);
        $modelUserInfo = $modelUserInfo ? $modelUserInfo : new UserInfo;
        
        if ($modelUserInfo->load(Yii::$app->request->post())) {
            $modelUserInfo->save();
        }
        return $this->render('avatar', [
            'model' => $model,
            'modelInfo' => $modelUserInfo,
        ]);
    }

    public function actionAccountDeleted()
    {
        $model = $this->findModel();
        if (Yii::$app->request->get('action') == 'restore') {
            AccountDelete::deleteAll(['user_id' => $model->id]);
        }
        if (!$model->checkDeleteAccount()){
            return $this->redirect('dashboard');
        }
        return $this->render('account-deleted', [
            'model' => $model,
        ]);
    }

    public function actionDeleteAccount()
    {
        $model = $this->findModel();
        $modelNewD = new AccountDelete();
        $modelNewD->user_id = $model->id;
        if ($modelNewD->load(Yii::$app->request->post())) {
            if ($modelNewD->save()){
                return $this->redirect('account-deleted');
            }
        }
        return $this->render('delete-account', [
            'model' => $model,
            'modelNewD' => $modelNewD,
        ]);
    }

    public function actionConfirmEmail()
    {
        $status = false;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel();
        if ($model->email) {
            $hash = Yii::$app->security->generateRandomString();
            if (EmailSender::sendEmail($model->email, 'Notification', 'email-verify', ['hash' => $hash])) {
                UserVerification::deleteAll(['type' => UserVerification::TYPE_EMAIL, 'user_id' => Yii::$app->user->id]);
                if (UserVerification::saveData(UserVerification::TYPE_EMAIL, $model->email, Yii::$app->user->id, UserVerification::STATUS_NOT_ACTIVE, $hash)) {
                    $status = true;
                }
            }
        }

        return ['status' => $status];
    }
    
    public function actionConfirmPhone()
    {
        $status = false;

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel();
        $type = Yii::$app->request->get('type');

        if ($model->userInfoModel && ($phonePref = $model->userInfoModel->getPhonePref()) && ($phone = $model->userInfoModel->phone)) {
            $phone = $model->userInfoModel->getPhone();
            if ($type == 'start' && VerifyPhone::verificationStart($phonePref, $phone)) {
                $status = true;
            } elseif($type == 'check') {
                $code = Yii::$app->request->get('code');
                if ($code && VerifyPhone::verificationCheck($phonePref, $phone, $code)) {
                    UserVerification::saveData(UserVerification::TYPE_PHONE, $phone, Yii::$app->user->id);
                    $status = true;
                }
            }
        }
        return ['status' => $status];
    }

    public function actionDashboard()
    {
        $model = $this->findModel();

        $notifications = NotificationUser::getNotReadByUser($model->id);
        $massages = MessagesDialog::getNotReadByUser($model->id);

        return $this->render('dashboard', [
            'model' => $model,
            'massagesD' => $massages,
            'notifications' => $notifications,
        ]);
    }
    public function actionVerification()
    {
        $model = $this->findModel();

        if ($hashemail = Yii::$app->request->get('hash-email')) {
            $modelUV = UserVerification::find()->where([
                    'type' => UserVerification::TYPE_EMAIL,
                    'user_id' => Yii::$app->user->id,
                    'hash' => $hashemail
                ])->one();
            if ($modelUV) {
                $modelUV->status = UserVerification::STATUS_ACTIVE;
                $modelUV->save();
            }
        }
        
        return $this->render('verification', [
            'model' => $model,
        ]);
    }

    public function actionPassword()
    {
        $model = $this->findModel();
        $model->scenario = 'password_change';
        if ($model->load(Yii::$app->request->post())){
            if ($model->validatePassword($model->old_password)) {
                $model->setPassword($model->password);
                if ($model->save()) {
                    $model->old_password = null;
                    $model->password = null;
                    $model->password_repeat = null;
                    Yii::$app->session->setFlash('saveSuccess');
                }
            } else {
                Yii::$app->session->setFlash('oldPasswordNotValid');
            }
        }

        return $this->render('password', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        $model = $this->findModel();
        if (!$modelInfo = $model->userInfoModel) {
            $modelInfo = new UserInfo();
            $modelInfo->user_id = $model->id;
        }

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                $model->save();
            }
            if ($modelInfo->load(Yii::$app->request->post())) {
                $modelInfo->save();
            }
        }

        return $this->render('index', [
            'model' => $model,
            'modelInfo' => $modelInfo,
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionHosts()
    {
        $searchModel = new HostsSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search();

        return $this->render('hosts', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionUpdate()
    {
        $model = $this->findModel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel()
    {
        if (($model = User::findOne(Yii::$app->user->id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
