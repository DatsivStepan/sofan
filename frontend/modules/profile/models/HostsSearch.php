<?php

namespace frontend\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Host;

/**
 * HostSearch represents the model behind the search form about `common\models\Host`.
 */
class HostsSearch extends Host
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['street_name', 'street_number'], 'string', 'max' => 100],
            [['country', 'city', 'title'], 'string', 'max' => 255],
            [['location_type', 'location_seats', 'location_size', 'location_pets', 'device_type', 'device_size', 'device_resolution', 'wifi', 'balcony', 'food', 'conditioner'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
                ->where(['status' => null]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id' => $this->id,
            'user_id' => $this->user_id,
//            'host_id' => $this->host_id,
//            'date_start' => $this->date_start,
//            'time_start' => $this->time_start,
//            'max_places_count' => $this->max_places_count,
//            'min_places_count' => $this->min_places_count,
//            'registration_end' => $this->registration_end,
//            'payment_per_person' => $this->payment_per_person,
//            'step' => $this->step,
//            'status' => $this->status,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ]);

//        $query->andFilterWhere(['like', 'live', $this->live])
//            ->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'wifi', $this->wifi])
//            ->andFilterWhere(['like', 'smoke', $this->smoke])
//            ->andFilterWhere(['like', 'food', $this->food])
//            ->andFilterWhere(['like', 'conditioner', $this->conditioner]);

        return $dataProvider;
    }
   
}
