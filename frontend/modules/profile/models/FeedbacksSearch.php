<?php

namespace frontend\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Feedback;

/**
 * EventsSearch represents the model behind the search form about `common\models\Events`.
 */
class FeedbacksSearch extends Feedback
{
    public $filter_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id', 'event_id', 'location', 'hoster', 'organization',
                    'hospitality', 'cleaning', 'fun', 'total', 'status', 'filter_to'
                ],
                'integer'
            ],
            [['created_at', 'updated_at', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
                ->alias('feedback')
                ->joinWith(['eventModel em']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->filter_to) {
            $query->andFilterWhere(['em.user_id' => $this->filter_to]);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'feedback.id' => $this->id,
            'feedback.user_id' => $this->user_id,
        ]);

        return $dataProvider;
    }
    
}
