<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\EventsBookings;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container">
    <?= $this->render('/default/menu_top'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class=" col-12  col-md-3 col-lg-2 left-block margin-bot-left-menu">
            <?= $this->render('_left_menu'); ?>
        </div>
        <div class="col-12 col-md-9 col-lg-10 js-events-index right-block"> 
            <?php if ($dataProvider->getModels()) { ?>
                <?php foreach ($dataProvider->getModels() as $model) { ?>
                    <div class="row">
                        <div class="event-for-two-half offset-2 offset-sm-0 col-8 col-sm-6 col-lg-4" style="padding-top: 15px;">
                            <?= $this->render('_event_template',[
                                'model' => $model
                            ]); ?>
                        </div>
                        <div class="event-for-two-half two-half offset-2 offset-sm-0 col-8 col-sm-6 col-lg-8">
                            <div class="col-12 text-name-block">
                                <h6> <?= Yii::t('profile_e_index', 'Prenotazioni Richieste'); ?></h6>
                            </div>
                            <?php if ($model->getEventsBookings()) { ?>
                                    <?php foreach ($model->getEventsBookings() as $bookings) { ?>
                                        <div class="row info-block">
                                            <div class="col-12 col-lg-4 js-open-booking-modal first-block" data-status="<?= $bookings->status; ?>" data-id="<?= $bookings->id; ?>">
                                                <?php if ($bookings->status == EventsBookings::STATUS_ON_CONFIRM) { ?>
                                                    <img src="/images/ic_error_outline_24px.png" class="icon-right">
                                                <?php } elseif ($bookings->status == EventsBookings::STATUS_CONFIRM) { ?>
                                                    <img src="/images/susses-icon.png" class="icon-right">
                                                <?php } elseif ($bookings->status == EventsBookings::STATUS_REJECTED) { ?>
                                                    <img src="/images/cancel-1.png" class="icon-right">
                                                <?php } ?>
                                                <p><?= $bookings->getName(); ?></p>
                                            </div>
                                            <div class="col-12 col-lg-5 three-block">
                                                <div class="col-12 text-w">
                                                    <?php if ($user = $bookings->userModel) { ?>
                                                        <img src="<?= $user->getAvatar(); ?>">
                                                        <p><?= $user->getUsernameWithSurname(); ?>  <?= $user->getAge() ? ', '.$user->getAge() : ''; ?></p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-2 four-block">
                                                <p><?= $bookings->getDateCreate(); ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <p class="p-no-message text-center"><?= Yii::t('profile_e_index', 'We dont Have .....'); ?></p>
            <?php } ?>

        </div>
    </div>
</div>
<script>
    $(function(){
        
            $(document).on('click', '.js-open-booking-modal', function () {
                var id = $(this).data('id')
                var status = $(this).data('status')
                if (status == '<?= EventsBookings::STATUS_ON_CONFIRM; ?>') {
                    $.confirm({
                        id: 'bookingModal',
                        title: '',
                        type: 'blue',
                        content: "url:<?= Url::to(['/profile/events/booking-confirm']); ?>/" + id,
                        containerFluid: true,
                        columnClass: 'col-11 col-sm-11 col-md-11 col-lg-9 col-xl-7 prenoto-modal-class',
                        buttons: {
                            close: {
                                text: 'Anulla',
                                btnClass: 'btn-default anulla-button'
                            },
                            delete: {
                                text: 'Not confirm',
                                btnClass: 'btn-danger',
                                action: function(button) {
                                    deleteEventBooking(id);
                                    return false;
                                }
                            },
                            ok: {
                                text: 'Confirm',
                                btnClass: 'btn-primary',
                            },
                        }
                    });
                }
            })
            
            function deleteEventBooking(id) {
                $.ajax({
                    url: '<?= Url::to(['/profile/events/booking-delete']) ?>/' + id,
                    method: 'post',
                    dataType: 'json',
                    success: function (response) {
                        $modal.bookingModal.close();
                        location.reload();
                    }
                })
            }
    })
</script>