<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    kartik\checkbox\CheckboxX,
    kartik\widgets\DatePicker,
    kartik\widgets\TimePicker,
    kartik\widgets\Select2,
    common\models\Events;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */

$colSm7 = ['options' => ['class' => 'col-sm-7']];
$colSm5 = ['options' => ['class' => 'col-sm-5']];
$colSm12 = ['options' => ['class' => 'col-sm-12']];
$model->max_places_count = $model->max_places_count ? $model->max_places_count : 1;
$model->min_places_count = $model->min_places_count ? $model->min_places_count : 1;
$model->confirm_status = $model->confirm_status ? $model->confirm_status : Events::STATUS_CONFIRM_YES;
?>
<section class="hai-una2">
    <div class="container-fluid">
        <div id="host_form" class="events-form form-evrnts-label">
            <?php $form = ActiveForm::begin(); ?>
            <div class="block-row">
                <div class="event-block-left adress-host-block">
                    <div class="row no-padding border-bot">
                        <div class="max-whith-block-even max-whith-block col-12 offset-0 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2 no-padding">
                            <h2 class="h2-style-adress"><?= $model->step; ?>. Iscrizione e pagamenti</h2>
                            <div class="col-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'max_places_count', $colSm12)->input('number', ['min' => 1]) ?>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="radio-botton-style">
                                        <?= $form->field($model, 'confirm_status', $colSm12)
                                            ->radioList(
                                                [
                                                    Events::STATUS_CONFIRM_YES => 'Conferma Obbligatoria',
                                                    Events::STATUS_CONFIRM_NO => 'Isc. Aperta'
                                                    ],
                                                [
                                                    'item' => function ($index, $label, $name, $checked, $value) {

                                                        $return = '<label class="modal-radio">';
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" ' . ($checked ? ' checked ' : '') . ' tabindex="3">';
                                                        $return .= '<i></i>';
                                                        $return .= '<span>' . ucwords($label) . '</span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'registration_end', $colSm12)->widget(Select2::className(), [
                                        'data' => Events::$registrationEnd,
                                        'hideSearch' => true,
                                        'options' => [
                                            'placeholder' => Yii::t('app', 'Scadenza Iscrizione'),
                                        ],
                                        'pluginOptions' => [
                                            'dropdownCssClass' => 'options-drop',
                                            'allowClear' => true
                                        ]
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'min_places_count', $colSm12)->input('number', ['min' => 1]) ?>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'payment_per_person', $colSm12)->input('number', ['min' => 1]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row w-100 no-padding no-margin">
                        <div class="max-whith-block-even col-12 offset-0 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2  no-padding">
                            <div class="form-group">
                                <div class="row w-100 no-gutters no-padding justify-content-between">
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <a href="<?= yii\helpers\Url::to(['/profile/events/create/' . $model->id, 'step' => 2]); ?>" class="btn btn-default avanti-btn-a">Indietro</a>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <?= Html::submitButton('Avanti', ['class' => 'btn btn-primary avanti-btn']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event-block-right img-block-upload-stp-3">
                    <?= $this->render('_upload-photo', [
                        'model' => $model,
                        'form' => $form
                    ]); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>