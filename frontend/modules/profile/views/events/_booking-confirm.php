
<?php

use yii\helpers\Html,
    yii\bootstrap\ActiveForm,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    common\models\EventsBookings;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

?>

<?php Pjax::begin(['id' => 'pjax-events-booking', 'enablePushState' => false]); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <script>
        if ($modal.bookingModal) {
            $modal.bookingModal.close();
        }

        const imageURL = '/images/group_192.png';
        swal({
            title: "",
            className: "sweet-alert-css",
            text: "Confirm",
            button: false,
            icon: imageURL,
            timer: 2000,
        }).then((value) => {
            location.reload()
        });
    </script>
<?php } ?>
<section class="status-block">
<?php $form = ActiveForm::begin(['id' => 'events-booking-form-', 'options' => ['data-pjax' => 'pjax-events-booking']]); ?>
    <?= $form->field($model, 'status')->hiddenInput(['value' => EventsBookings::STATUS_CONFIRM])->label(false); ?>
    <div class="js-events-booking-container col-sm-12 no-padding-lg">
        <div class="col-lg-12" style="margin-top:5px;">
            <?php if ($model->bookingItemsModel) { ?>
                <table class="table">
                    <tr class="top-table">
                        <td class="top-left"><b>Name</b></td>
                        <td><b>Gender</b></td>
                        <td class="top-right"><b>Age</b></td>
                    </tr>
                    <?php foreach ($model->bookingItemsModel as $bookingItems) { ?>
                        <tr class="top-table-1">
                            <td> <div><?= $bookingItems->name; ?></div></td>
                            <td><div><?= $bookingItems->gender ? 'M' : 'F'; ?></div></td>
                            <td><div><?= $bookingItems->age ? $bookingItems->age : 0; ?></div></td>
                        </tr>
                    <?php } ?>
                </table>
            <?php } ?>
            <?= $form->field($model, 'why')->widget(Select2::className(), [
                'data' => EventsBookings::$reasonDeletion,
                'pluginOptions' => [
                    'dropdownCssClass' => 'options-drop option-font',
                    'allowClear' => true
                ]
            ])->label(false) ?>
        </div>
    </div>
  </section>  
<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>