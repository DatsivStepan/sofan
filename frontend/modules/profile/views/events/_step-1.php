<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use common\models\TvShowCategories;
use kartik\widgets\Select2,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
$colSm7 = ['options' => ['class' => 'col-12 col-sm-8']];
$colSm5 = ['options' => ['class' => 'col-12 col-sm-4']];
$colSm12 = ['options' => ['class' => 'col-12  col-sm-12']];

$tvArray = TvShowCategories::getAllWithCategory();
$model->tv_show_name = $model->getName();
$tvS = $model->tvShowModel && $model->tvShowModel->category_id ? true : false;
?>
<section class="hai-una2">
    <div class="container-fluid">
        <div id="host_form" class="events-form">
            <?php $form = ActiveForm::begin(); ?>
            <div class="block-row">
                <div class="event-block-left adress-host-block">
                    <div class="row no-padding border-bot">
                        <div class="max-whith-block-even max-whith-block col-12 offset-0 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2 no-padding">
                            <h2 class="h2-style-adress"><?= $model->step; ?>. Evento</h2>
                            <div class="row no-padding">
                                <div class="col-12 col-sm-10">
                                    <div class="row no-padding">
                                        <div class="div-w" style="width: 100%">
                                            <?= $form->field($model, 'tv_show_id')->hiddenInput()->label(false); ?>
                                            <div class="col-12  col-sm-12 field-events-tv_show_name">
                                                <label class="control-label" for="events-tv_show_name">Evento</label>
                                            </div>
                                            <?= $form->field($model, 'tv_show_name', $colSm12)->textInput(['placeholder' => 'EX. Juve'])->label(false); ?>
                                            <div style="position:relative;" class="js-search-tv-block">
                                                <ul class="js-search-tv-ul show-dropdown">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-2 no-padding">
                                    <div class="live-chexbox" style="<?= $tvS == 1 ? '' : 'display:none;'?>">
                                        <?= $form->field($model, 'live', [
                                            'template' => '{input}',
                                            'labelOptions' => ['class' => 'control-label'],
                                        ])
                                            ->widget(CheckboxX::classname(), ['autoLabel' => true, 'pluginOptions' => ['threeState' => false]]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-padding mb-3">
                                <?= $form->field($model, 'date_start', $colSm7)
                                    ->widget(DatePicker::className(), [
                                        'removeButton' => false,
                                        'disabled' => $tvS,
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'yyyy-mm-dd',
                                        ]
                                    ]) ?>

                                <?= $form->field($model, 'time_start', $colSm5)->widget(TimePicker::className(), [
                                    'disabled' => $tvS,
                                    'pluginOptions' => [
                                        'showMeridian' => false,
                                    ]
                                ]) ?>
                            </div>

                            <div class="row no-padding" style="margin-bottom: 7%;">
                                <?= $form->field($model, 'description', $colSm12)->textarea(['rows' => 3, 'placeholder' => 'Discrivi il tuo Evento']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row w-100 no-padding no-margin">
                        <div class="max-whith-block-even col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-ld-8 offset-lg-2 col-xl-8 offset-xl-2  no-padding">
                            <div class="form-group">
                                <div class="row w-100 no-gutters no-padding justify-content-between">
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <a class="btn btn-default indiero-btn">Indietro</a>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <?= Html::submitButton('Avanti', ['class' => 'btn btn-primary avanti-btn']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event-block-right img-block-upload">
                    <?= $this->render('_upload-photo', [
                        'model' => $model,
                        'form' => $form,
                    ]); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>
<script>
    $(function(){
        $(document).click(function(){
            $('.js-search-tv-ul').hide();
        });
        
        $(document).on('change', '#events-live', function(event){
            if ($(this).val() == 1) {
                var tv_show_id = $('#events-tv_show_id').val()
                $.ajax({
                    url: '<?= Url::to(['/profile/events/get-tv-show-data']) ?>/' + tv_show_id,
                    method: 'get',
                    dataType: 'json',
                    success: function (response) {
                        if (response.status) {
                            $('#events-date_start').val(response.date)
                            $('#events-date_start').attr('disabled', true)

                            $('#events-time_start').val(response.time)
                            $('#events-time_start').attr('disabled', true)
                        }
                    }
                });
            } else {
                $('#events-date_start').val('')
                $('#events-date_start').attr('disabled', false)

                $('#events-time_start').val('')
                $('#events-time_start').attr('disabled', false)
            }
        })

        $(document).on('click', '.js-search-tv-ul .js-tv-show', function(event){
            $('.js-search-tv-ul').hide()
            $('.js-search-tv-ul').html('');
            var id = $(this).data('id');
            var category = $(this).data('category');
            if (category == "Sport") {
                $('.live-chexbox').show()
            } else {
                $('.live-chexbox').hide()
            }
            var text = $(this).text();
            $('#events-tv_show_id').val(id);
            $('#events-tv_show_name').val(text);
            event.stopPropagation();
        });

        $(document).on('click', '.js-search-tv-ul .js-tv-show-category, #events-tv_show_name', function(e){
            e.stopPropagation();
        });

        function changeTvShowData(thisElement) {
            $('#events-date_start').val('')
            $('#events-time_start').val('')
            $('#events-date_start').attr('disabled', false)
            $('#events-time_start').attr('disabled', false)
            $('#events-live').val(0);
            $('#events-live').checkboxX('refresh');
            $('.live-chexbox').hide()

            $('#events-tv_show_id').val('')
            var search_val = thisElement.val();
            if (search_val.length > 2) {
                $.ajax({
                    url: '<?= Url::to(['/site/get-tv-show']) ?>/',
                    method: 'get',
                    dataType: 'json',
                    data: {value:search_val},
                    success: function (response) {
                        if (response.status && response.data) {
                            $('.js-search-tv-ul').show();
                            $('.js-search-tv-ul').html('')
                            if (response.data.length != 0) {
                                var group = response.data;
                                $.each(group, function( index, value ) {
                                    if(value){
                                        $('.js-search-tv-ul').append('<li class="js-tv-show-category category-name ' + index.toLowerCase() + '">' + index + '</li>')
                                        $.each(value, function( i, v ) {
                                            $('.js-search-tv-ul').append('<li class="js-tv-show show-item" data-category="' + index + '" data-id="'+i+'">' + v + '</li>')
                                        })
                                    }
                                    //$('.js-search-tv-ul').append('<li class="js-tv-show" data-id="'+index+'">' + value + '</li>')
                                });
                            } else {
                                $('.js-search-tv-ul').append('<li class="text-center">Not found</li>')
                            }
                        } else {
                            $('.js-search-tv-ul').hide()
                            $('.js-search-tv-ul').html('')
                        }
                    }
                });
            } else {
                $('.js-search-tv-ul').hide()
                $('.js-search-tv-ul').html('')
            }
        }

        $(document).on('keyup', '#events-tv_show_name', function(){
            changeTvShowData($(this))
        })
    })
</script>