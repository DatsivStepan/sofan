<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use kartik\widgets\Select2;
use common\models\Events;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
$model->wifi = $model->wifi === null ? ($model->hostModel ? $model->hostModel->wifi : 0) : $model->wifi;
$model->balcony = $model->balcony === null ? ($model->hostModel ? $model->hostModel->balcony : 0) : $model->balcony;
$model->food = $model->food === null ? ($model->hostModel ? $model->hostModel->food : 0) : $model->food;
$model->conditioner = $model->conditioner === null ? ($model->hostModel ? $model->hostModel->conditioner : 0) : $model->conditioner;
?>
<section class="hai-una2">
    <div class="container-fluid">
        <div id="host_form" class="events-form">
            <?php $form = ActiveForm::begin(); ?>
            <div class="block-row">
                <div class="event-block-left adress-host-block">
                    <div class="row w-100 no-padding border-bot padd-border">
                        <div class="max-whith-block-even max-whith-block col-12 offset-0 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2 no-padding">
                            <h2 class="h2-style-adress"><?= $model->step; ?>. Comfort e Preferenze</h2>
                            <div class="row w-100 no-padding no-margin">
                                <div class="col-12 offset-0 col-sm-10 offset-sm-1">
                                    <div class="form-group"></div>
                                    <?= $form->field($model, 'wifi', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'wifi'
                                        ]
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>

                                    <?= $form->field($model, 'conditioner', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'air-conditioner'
                                        ]
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>

                                    <?= $form->field($model, 'food', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'dinner'
                                        ]
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>

                                    <?= $form->field($model, 'balcony', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'balcony'
                                        ],
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>
                                </div>
                            </div>
                            <h2 class="h2-style-adress" style="margin-top: 9%;">5. Fumo</h2>
                            <div class="form-group">
                                <?= $form->field($model, 'smoke')->widget(Select2::className(), [
                                    'data' => [Events::SMOKE_NO => 'NO', Events::SMOKE_YES => 'SÌ'],
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'dropdownCssClass' => 'options-drop',
                                        'allowClear' => true
                                    ]
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row w-100 no-padding no-margin">
                        <div class="max-whith-block-even col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2  no-padding">
                            <div class="form-group">
                                <div class="row w-100 no-gutters no-padding justify-content-between">
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <a href="<?= yii\helpers\Url::to(['/profile/events/create/' . $model->id, 'step' => 3]); ?>"
                                           class="btn btn-default avanti-btn-a">Indietro</a>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <?= Html::submitButton('Avanti', ['class' => 'btn btn-primary avanti-btn']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event-block-right" style="background-color: #E1E8EE;">
                    <div class="row w-100 height_block-w no-padding no-margin align-items-center">
                        <div class="col text-center js-wifi">
                            <img class="img-vacation"
                                 src="<?= $model->wifi ? '/images/wifi_active.png' : '/images/wifi.png' ?>">
                        </div>
                        <div class="col text-center js-air-conditioner">
                            <img class="img-vacation"
                                 src="<?= $model->conditioner ? '/images/air-conditioner_active.png' : '/images/air-conditioner.png' ?>">
                        </div>
                        <div class="col text-center js-dinner">
                            <img class="img-vacation"
                                 src="<?= $model->food ? '/images/dinner_active.png' : '/images/dinner.png' ?>">
                        </div>
                        <div class="col text-center js-balcony">
                            <img class="img-vacation"
                                 src="<?= $model->balcony ? '/images/balcony_active.png' : '/images/balcony.png' ?>">
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>
<script>
    $(function () {
        $(document).on('change', '.js-change-status', function () {
            var val = $(this).find('input').val();
            var src = $(this).data('img')
            if (val == 1) {
                src = src + '_active';
            }
            console.log(src)
            $('.js-' + $(this).data('img')).find('img').attr('src', '/images/' + src + '.png')
        })
    })
</script>