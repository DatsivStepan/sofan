<?php

use yii\helpers\Html,
    yii\helpers\Url;
use kartik\social\FacebookPlugin;
use kartik\social\GooglePlugin;
use \bigpaulie\social\share\Share;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */

?>
<style type="text/css">
    .footer{
        display: none;
    }
</style>
<section class="hai-una2">
    <div class="container-fluid">
        <div class="row no-padding">
            <div class="col-12 col-sm-12 no-padding-rigth padding-mob">
                <div class="block-row ">
                    <div class="event-block-left adress-host-block">
                        <div class="row w-100 no-padding no-margin">
                            <div class="col-12 offset-0 col-md-10 offset-md-1  rigt-block-text text-left">
                                <img src="/images/susses-icon-event.png" class="animated icons success-icon">
                                <div>
                                    <h5 class="title-host-h5 event-title">
                                        Complimenti <?= Yii::$app->user->identity->getUsernameWithSurname(); ?>,<br>
                                        Hai appena pubblicato un Evento,<br>
                                        ora puoi visualizzare il tuo Evento ocondividerlo con i tuoi amici!
                                    </h5>
                                </div>
                                <div class="row no-padding">
                                    <div class="col-6">
                                        <?= Html::a('Visualizza Evento', Url::to([$model->getLink()]), ['class' => 'btn btn-info avanti-btn btn-vizzual-evento no-margin']) ?>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 style-share">
                                        <?= Share::widget([
                                            'networks' => [
                                                'facebook' => 'https://www.facebook.com/sharer/sharer.php?u={url}',
                                                'google-plus' => 'https://plus.google.com/share?url={url}',
                                            ],
                                            'type' => Share::TYPE_LARGE,
                                            'htmlOptions' => ['id'=>'socials-share']
                                        ]);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="event-block-right bacg-event-steo-5">
                        <div class="row no-padding">
                            <div class="col-12 offset-0 col-md-8 offset-md-2 text-center">
                                <img src="<?= $model->getDefaultImage(); ?>">
                                <p class="date-start-text">
                                    <?= $model->getDateStart(); ?>
                                </p>
                                <h4 class="name-event-user">
                                    <?= $model->getName(); ?>
                                </h4>
                                <p class="addres-style-text">
                                    <img class="img-ic-room" src="/images/ic_room_24px.png"><?= $model->getAddress(); ?>
                                </p>
                                <h2 class="payment-text-style">
                                    <?= $model->getPaymentPerPerson(); ?>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>