<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\profile\models\EventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .navbar-nav li.active a{
        color:#0080FF !important;
    } 
</style>
<nav class="navbar navbar-expand navbar-light left-menu">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ">
            <li class="nav-item <?= !array_key_exists('type', Yii::$app->request->get()) ? ' active ' : ''; ?>">
                <a class="nav-link"  href="<?= Url::to(['/profile/events']); ?>"><?= Yii::t('profile_e_left_menu', 'Eventi Futuri'); ?></a>
            </li>
            <li class="nav-item <?= array_key_exists('type', Yii::$app->request->get()) && ( Yii::$app->request->get('type') == 'pass') ? ' active ' : ''; ?>">
                <a class="nav-link" href="<?= Url::to(['/profile/events?type=pass']); ?>"><?= Yii::t('profile_e_left_menu', 'Eventi passati'); ?></a>
            </li>
            <li class="nav-item <?= array_key_exists('type', Yii::$app->request->get()) && ( Yii::$app->request->get('type') == 'canceled') ? ' active ' : ''; ?>">
                <a class="nav-link" href="<?= Url::to(['/profile/events?type=canceled']); ?>"><?= Yii::t('profile_e_left_menu', 'Eventi archeviati'); ?></a>
            </li>
        </ul>
    </div>
</nav>
<?php if (Yii::$app->user->identity->getHostCount()) { ?>
    <?= Html::a('<img src="/images/plus-icon.png" style="height:9px; margin-top:-1px; margin-rigth:2px;">' . Yii::t('app', 'Create Evento'), ['create'], ['class' => 'btn creo-evento']) ?>
<?php } ?>
