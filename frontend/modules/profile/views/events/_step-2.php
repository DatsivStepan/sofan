<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    kartik\widgets\Select2,
    common\models\Host,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDoSL8IN0GJJOwChniCpdNoPvWQa0c7ZnI&libraries=places&callback=initAutocomplete', [
    'async' => true,
    'defer' => true,
]);

$colSm7 = ['options' => ['class' => 'col-sm-7']];
$colSm5 = ['options' => ['class' => 'col-sm-5']];
$colSm12 = ['options' => ['class' => 'col-sm-12']];
$hosts = Host::getAllInArrayMap(Yii::$app->user->id);
$model->host_id = $model->host_id ? $model->host_id : key($hosts);
?>
<section class="hai-una2">
    <div class="container-fluid">
        <div id="host_form" class="events-form">
            <?php $form = ActiveForm::begin(); ?>
            <div class="block-row ">
                <div class="event-block-left adress-host-block">
                    <div class="row no-padding border-bot">
                        <div class="max-whith-block-even max-whith-block col-12 offset-0 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2 no-padding">
                            <h2 class="h2-style-adress"><?= $model->step; ?>. Location</h2>
                            <div class="row no-padding">
                                <div class="col-sm-12 no-padding">
                                    <div class="form-group">
                                        <?= $form->field($model, 'host_id', $colSm12)->widget(Select2::className(), [
                                            'data' => $hosts,
                                            'options' => [
                                                'placeholder' => Yii::t('app', 'sala, via mazzini Milano'),
                                            ],
                                            'pluginOptions' => [
                                                'dropdownCssClass' => 'options-drop',
                                                'allowClear' => true
                                            ]
                                        ])->label(false); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-padding">
                                <?= Html::hiddenInput('lat', $model->hostModel ? $model->hostModel->lat : '', ['id' => 'lat']) ?>
                                <?= Html::hiddenInput('lng', $model->hostModel ? $model->hostModel->lng : '', ['id' => 'lng']) ?>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Città</label>
                                        <?= Html::textInput('city', $model->getCityName(), [
                                            'class' => 'form-control col-ms-12 js-city-input',
                                            'id' => 'city',
                                            'readOnly' => true,
                                            'placeholder' => 'Milano',
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label">Provincia</label>
                                        <?= Html::textInput('province', null, [
                                            'class' => 'form-control col-ms-12 js-province-input',
                                            'id' => 'province',
                                            'readOnly' => true,
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label">Cap</label>
                                        <?= Html::textInput('zip_code', null, [
                                            'class' => 'form-control col-ms-12 js-zip_code-input',
                                            'id' => 'zip_code',
                                            'readOnly' => true,
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="form-group ">
                                        <label class="control-label">Via</label>
                                        <?= Html::textInput('street_name', $model->getStreetName(), [
                                            'class' => 'form-control col-ms-12 js-street_name-input',
                                            'id' => 'street_name',
                                            'readOnly' => true,
                                            'placeholder' => 'Brenta',
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group ">
                                        <label class="control-label">Numero</label>
                                        <?= Html::textInput('street_number', $model->getStreetNumber(), [
                                            'class' => 'form-control col-ms-12 js-street_number-input',
                                            'id' => 'street_number',
                                            'readOnly' => true,
                                            'placeholder' => '33',
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row w-100 no-padding no-margin">
                        <div class="max-whith-block-even col-12 offset-0 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2  no-padding">
                            <div class="form-group">
                                <div class="row w-100 no-gutters no-padding justify-content-between">
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <a href="<?= yii\helpers\Url::to(['/profile/events/create/' . $model->id, 'step' => 1]); ?>"
                                           class="btn btn-default avanti-btn-a">Indietro</a>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <?= Html::submitButton('Avanti', ['class' => 'btn btn-primary avanti-btn']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event-block-right img-block-upload-step-2">
                    <div id="map" class="map-block-mob"></div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>
</section>
<script>
    var formIDs = {
        street_number: "street_number",
        street_name: "street_name",
        locality: "city",
        administrative_area_level_1: "province",
        postal_code: "zip_code"
    };

    $(function () {

        function clearForm() {
            console.log(formIDs);
            $.each(formIDs, function (key, value) {
                $('#' + value).val('')
            })
        }

        $(document).on('change', '#events-host_id', function () {
            var hostId = $(this).val();
            clearForm();
            $.ajax({
                url: '<?= Url::to(['/profile/host/get-host-data']) ?>/' + hostId,
                method: 'post',
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        var host = response.data;
                        $.each(host, function (key, value) {
                            $('[name=' + key + ']').val(value);
                        });

                        var latlng = {lat: parseFloat(host.lat), lng: parseFloat(host.lng)};
                        geocodeLatLng(latlng);
                    }
                }
            })
        })
    })


    var autocomplete;
    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    var latInput = 'lat';
    var lngInput = 'lng';
    var map;
    var marker;
    var geocoder;

    function initAutocomplete() {

        var mapOptions = {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13,
            minZoom: 8,
            maxZoom: 12,
        };

        var lat = document.getElementById(latInput).value;
        var lng = document.getElementById(lngInput).value;
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
//
        if (lat && lng) {
            mapOptions.center.lat = parseFloat(lat);
            mapOptions.center.lng = parseFloat(lng);
        }

        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var image = {
            url: '/images/icons/map_icon_2.png',
            size: new google.maps.Size(50, 32)
        };

        marker = new google.maps.Marker({
            map: map,
            icon: image,
        });

        geocoder = new google.maps.Geocoder;
//        console.log(latlng)
        if (lat && lng) {
            geocodeLatLng(latlng);
        }
    }

    function geocodeLatLng(latlng) {
        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    map.setZoom(11);
                    map.setCenter(latlng);
                    marker.setPosition(latlng);

                    var place = results[0];
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            if (addressType == 'country') continue;
                            document.getElementById(formIDs[addressType]).value = val;
                        }
                    }
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

</script>