<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container">
    <?= $this->render('/default/menu_top'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class=" col-12 col-md-2 margin-bot-left-menu">
            <?= $this->render('_left_menu'); ?>
        </div>
        <div class="col-12 col-md-10 js-events-index"> 
            <?php if ($dataProvider->getModels()) { ?>
                <div class="row"> 
                    <?php foreach ($dataProvider->getModels() as $model) { ?>
                        <div class="event-for-two-half col-4" style="padding-top: 15px;">
                            <?= $this->render('_event_template',[
                                'model' => $model
                            ]); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <p class="p-no-message text-center"><?= Yii::t('profile_e_canceled', 'We dont Have .....'); ?> </p>
            <?php } ?>
        </div>
    </div>
</div>
