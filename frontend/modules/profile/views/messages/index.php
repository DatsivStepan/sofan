<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?= $this->render('/default/menu_top'); ?>
    <?php Pjax::begin(['id' => 'pjax-message-send', 'enablePushState' => false]); ?>
    <?php if ($modelD = $model) { ?>
        <?php $userD = $modelD->getUserData(); ?>
        <div class="messages-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <div class="row no-margin">
                <div class="col-md-12 col-lg-3 col-xl-3 massage-notific-block">
                    <img src="/images/notification (2).png">
                    <h2>Messaggi&nbsp; (<?= $modelI->getMessagesCount(); ?>)</h2>
                </div>
                <div class="col-md-4 col-lg-3 col-xl-3 block-message-left">
                    <?php foreach ($dialogs as $dialog) { ?>
                        <div class="<?= $modelD->id == $dialog->id ? ' active ' : ''; ?>">
                            <a href="<?= Url::to(['/profile/messages/' . $dialog->id]); ?>" data-pjax="0">
                                <?php if ($user = $dialog->getUserData()) { ?>
                                    <img class="img-avatar-usr" src="<?= $user->getAvatar(); ?>">
                                    <span class="user-whith-surname"><?= $user->getUsernameWithSurname(); ?></span>
                                    <?php if ($countD = $dialog->getNotReadCount(Yii::$app->user->id)) { ?>
                                        <div class="block-red-mass">
                                            <?= $countD; ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-8 col-lg-6 col-xl-6 block-message-right">
                    <div class="message-block" id="js-message-block">
                        <?php if ($dataProvider->getModels()) { ?>
                            <?php $count = count($dataProvider->getModels()); ?>
                            <?php $lastDate = null; ?>
                            <?php $lastHourMinute = null; ?>
                            <?php foreach ($dataProvider->getModels() as $key => $model) { ?>
                                <?php $nowDate = date('M j', strtotime($model->created_at)); ?>
                                <?php $nowHourMinute = date('H:i', strtotime($model->created_at)); ?>
                                <?php if ($lastDate != $nowDate && ($lastDate = $nowDate)) { ?>
                                    <?php $lastHourMinute = $nowHourMinute; ?>
                                    <div class="row no-margin">
                                        <h2 class="text-center">
                                            <?= $lastDate == date('M j') ? 'Today' : $lastDate; ?>
                                            <?= $lastHourMinute; ?>
                                        </h2>
                                    </div>
                                <?php } elseif ($lastHourMinute != $nowHourMinute) { ?>
                                    <?php $lastHourMinute = $nowHourMinute; ?>
                                    <div class="row no-margin">
                                        <h4 class="text-center"><?= $lastHourMinute; ?></h4>
                                    </div>
                                <?php } ?>
                                <div class="row no-margin">
                                    <?php if ($model->from_user_id == Yii::$app->user->id) { ?>
                                        <div class="col-12 padd-masseg-chat">
                                            <span class="not-my"><?= $model->message; ?></span>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-12 padding-sm-mob-no">
                                            <div class="row no-margin align-items-center mar-top-mas">
                                                <div class="col-2 col-sm-1 no-padding">
                                                    <img class="avat-img-message" src="<?= $userD->getAvatar(); ?>">
                                                </div>
                                                <div class="col-10 col-sm-11 my">
                                                    <span><?= $model->message; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                
                            <?php } ?>
                        <?php } ?>
                    </div>

                    <div>
                        <?php $form = ActiveForm::begin(['id' => 'events-booking-form-', 'options' => ['data-pjax' => 'pjax-message-send']]); ?>
                        <?= $form->field($modelNew, 'dialog_id')->hiddenInput(['value' => $modelD->id])->label(false); ?>
                        <?= $form->field($modelNew, 'from_user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false); ?>
                        <?= $form->field($modelNew, 'to_user_id')->hiddenInput(['value' => $userD->id])->label(false); ?>
                        <div class="row">
                            <div class="col-sm-12 no-padding massage-textarea">
                                <?= $form->field($modelNew, 'message')->textarea(['placeholder' => "Scrivi il testo e premi invio per mandare"]) ?>
                                <?= Html::submitButton('', ['class' => 'btn-send-botton']); ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

        </div>
    <?php } else { ?>
        <p class="p-no-message text-center">We dont Have messages</p>
    <?php } ?>
    <script>
        var objDiv = document.getElementById("js-message-block");
        objDiv.scrollTop = objDiv.scrollHeight;
    </script>
    <?php Pjax::end(); ?>
</div>
