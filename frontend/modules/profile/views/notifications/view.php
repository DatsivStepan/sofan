<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Notification */

$this->title = 'Notifica'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-view">
    <?= $this->render('/default/menu_top'); ?>
    <div class="container site-content">
        <div class="row">
            <div class="col-md-2 col-lg-3 col-xl-3">
                <div class="notification-title-block">
                    <img src="/images/notification.png">
                    <h2>Notifiche &nbsp;(<?= Yii::$app->user->identity->getNotificationsCount(); ?>)</h2>
                </div>
                <div class="notif-recevute"><a href="/profile/notifications"><- Notifiche recevute</a></div>
            </div>
            <div class="col-12 col-md-10 col-lg-9 col-xl-9 notification">
                <section class="right-block">
                    <div class="row">
                        <div class="col-12 notific-name" style="padding-left: 15px">
                            <h2> <?= Html::encode($this->title) ?></h2>
                        </div>
                        <div class="col-6"></div>
                    </div>
                    <div class="row dashboard-block">
                        <div class="col-9 col-sm-10 col-md-10 col-xl-10 dashboard-block__img-block">
                            <img class="img-block__images" src="/images/img-ava.jpg">
                        </div>
                        <div class="col-3 col-sm-2 col-md-2 col-xl-2 dashboard-block__date align-self-center">
                        </div>
                        <div class="row">
                            <div class="col-7 col-md-6 col-lg-6 col-xl-6 dashboard-block__text-block">
                                <?= $model->note; ?>
                            </div>
                        </div>
                        <div class="row w-100">
                            <div class="col-12 col-md-12 text-right block__botton">
                                <?= $this->render('_action_buttons', [
                                    'model' => $model
                                ]); ?>
                            </div>
                        </div>
                        <div class="row w-100">
                            <div class="col-12 col-md-12 dashboard-block__date">
                                <p>
                                    <?= $model->getDateCreate('M d/m/Y H:i'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
