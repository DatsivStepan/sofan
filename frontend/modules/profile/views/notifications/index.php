<?php

use yii\helpers\Html,
    yii\grid\GridView,
    yii\widgets\Pjax,
    common\models\Notification,
    kartik\widgets\Select2,
    common\models\Events,
    kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\NotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$array = [null => 'Select All'] + Events::getMyEventsInMapArray();
$this->title = Yii::t('app', 'Notifications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">
    <?= $this->render('/default/menu_top'); ?>
    <div class="container site-content">
        <div class="row">
            <div class="col-md-2 col-lg-2 col-xl-2 margin-bottom-to-footer">
                <div class="notification-title-block">
                    <img src="/images/notification.png">
                    <h2>Notifiche &nbsp;(<?= Yii::$app->user->identity->getNotificationsCount(); ?>)</h2>
                </div>
            </div>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="col-12 col-md-10 col-lg-9 col-xl-9 whigth-right-block">
              <div class="row">
                  <div class="col-12 col-md-4 offset-md-8 select-block"><?php $form = ActiveForm::begin([
                        'id' => 'form-notification-search',
                        'action' => ['index'],
                        'method' => 'get',
                    ]); ?>

                    <?= $form->field($searchModel, 'parent_object_id')->widget(Select2::className(), [
                        'data' => $array,
                        'value' => $searchModel->parent_object_id,
                        'pluginOptions' => [
                            'dropdownCssClass' => 'options-drop option-font',
                            'allowClear' => true
                        ]
                    ])->label(false) ?>

                <?php ActiveForm::end(); ?>
                    
            </div>
              </div>
                <section class="right-block-notification">
                    <?php if ($dataProvider->getModels()) { ?>
                        <?php foreach ($dataProvider->getModels() as $model) { ?>
                            <div class="row notifiche-block js-dashboadrd-block <?= $model->status == Notification::STATUS_READ ? 'read' : 'not-read'; ?>">
                                <div class="col-12 col-sm-12 col-md-1 col-xl-1 align-self-center">
                                    <img class="notifiche-block__avatar" src="/images/user.png">
                                </div>
                                <div class="col-12 col-sm-12 col-md-5 col-xl-5 align-self-center text-block-notifiche">
                                    <p><a href="<?= $model->getLink(); ?>"><?= $model->getShortNote(); ?></a></p>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-xl-6 block-date-notifiche">
                                    <div class="row">
                                        <div class="col-12 col-md-12 block__botton">
                                            <?= $this->render('_action_buttons', [
                                                'model' => $model
                                            ]); ?>
                                        </div>
                                        <div class="col-12 col-md-12 no-padding block__date-create">
                                            <?= $model->getDateCreate('M d/m/Y H:i'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </section>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(document).on('change', '#notificationsusersearch-parent_object_id', function(){
            $('#form-notification-search').submit()
        })
    })
</script>