<?php

use yii\helpers\Html,
    common\models\Notification,
    common\models\Events,
    common\models\EventsBookings,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\NotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?php 
$buttons = '';
if ($notification = $model->notificationModel) {
    switch ($notification->type) {
        case Notification::TYPE_EVENT_BOOKING:
            if ($modelEventsB = EventsBookings::findOne($notification->object_id)) {
                if ($modelEventsB->status == EventsBookings::STATUS_ON_CONFIRM) {
                    $icon = null;
                    $class = '';
                    $disabled = false;
                } elseif ($modelEventsB->status == EventsBookings::STATUS_CONFIRM) {
                    $icon = '/images/susses-icon.png';
                    $class = ' btn-disabled';
                    $disabled = true;
                } else {
                    $class = ' btn-disabled';
                    $icon = '/images/delete.png';
                    $disabled = true;
                }

                $buttons = 
                    '<div>' .
                        Html::button('Accetta', [
                            'class' => 'btn btn-success js-notification-button js-active-events-booking ' . $class,
                            'data-id' => $modelEventsB->id,
                            'data-not_id' => $model->id,
                            'disabled' => $disabled
                        ]) .
                        Html::a('Contatta', Url::to(['/profile/messages/check-dialog/' . $modelEventsB->user_id]), [
                            'class' => 'btn btn-primary js-notification-button ' . $class
                        ]) .
                        Html::button('Rifiuta', [
                            'class' => 'btn btn-danger js-notification-button js-deactive-events-booking ' . $class,
                            'data-id' => $modelEventsB->id,
                            'data-not_id' => $model->id,
                            'disabled' => $disabled
                        ]) .
                    ($icon ? '<img class="block-bottom-img" src="'.$icon.'">' : '').
                    '</div>';
            }
            break;
        case Notification::TYPE_EVENT_DELETE:
            //no Action Required
            break;

        default:
            break;
    }
} 
?>
<div>
    <?= $buttons; ?>
</div>
<script>
    $(function(){

        $(document).on('click', '.js-active-events-booking', function(){
            var id = $(this).data('id');
            var not_id = $(this).data('not_id');
            var thisEl = $(this)
            $.ajax({
                url: '<?= Url::to(['/profile/notifications/booking-change-status']) ?>/' + id,
                data:{type:'active', not_id:not_id},
                method: 'get',
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        thisEl.closest('.js-dashboadrd-block').find('.js-notification-button').addClass('btn-disabled')
                        thisEl.closest('.js-dashboadrd-block').find('.js-notification-button').attr('disabled', 'disabled')
                        thisEl.closest('.js-dashboadrd-block').addClass('read')
                    }
                }
            })
        })

        $(document).on('click', '.js-deactive-events-booking', function(){
            var id = $(this).data('id');
            var not_id = $(this).data('not_id');
            var thisEl = $(this)
            $.ajax({
                url: '<?= Url::to(['/profile/notifications/booking-change-status']) ?>/' + id,
                data:{type:'deactive', not_id:not_id},
                method: 'get',
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        thisEl.closest('.js-dashboadrd-block').find('.js-notification-button').addClass('btn-disabled')
                        thisEl.closest('.js-dashboadrd-block').find('.js-notification-button').attr('disabled', 'disabled')
                        thisEl.closest('.js-dashboadrd-block').addClass('read')
                    }
                }
            })
        })

    })
</script>