<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container password-page">
    <?= $this->render('/default/menu_top'); ?>
    
    <div class="row">
        <div class=" col-12  col-md-3 col-lg-2 left-block feedback-left-menu"> 
            <?= $this->render('/default/menu_left'); ?>
        </div>
        <div class="col-12 col-md-9 col-lg-10 js-events-index right-block"> 
            <div class="row name">
                <div class="col-12">
                    <h5><?= Yii::t('profile_password', 'Cambia Password'); ?></h5>
                </div>
            </div>
            <section class="right">
                <div class="row">
                    <div class="col-12 password-form-block">
                        <h6><?= Yii::t('profile_password', 'In caso di smarrimento, la tua password sara inviata a'); ?> </h6>
                        <text><?= $model->email; ?></text>
                        <?php $form = ActiveForm::begin(); ?>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-lg-2 col-form-label label-text"><?= Yii::t('profile_password', 'Password attuale'); ?></label>
                                <div class="col-sm-8 col-lg-4">
                                  <?= $form->field($model, 'old_password')->input('password')->label(false); ?>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-lg-2 col-form-label label-text"><?= Yii::t('profile_password', 'Creo una nuova password'); ?></label>
                                <div class="col-sm-8 col-lg-4">
                                 <?= $form->field($model, 'password')->input('password')->label(false); ?>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-lg-2 col-form-label label-text"><?= Yii::t('profile_password', 'Conferma la nuova password'); ?></label>
                                <div class="col-sm-8 col-lg-4">
                                 <?= $form->field($model, 'password_repeat')->input('password')->label(false); ?>
                                </div>
                              </div>
                            
                        <div class="col-sm-3 offset-sm-4 offset-lg-2 button-salva-block" style="padding-left: 5px;">
                            <div class="form-group" style="margin-bottom: 20px;">
                                <?= Html::submitButton(Yii::t('app', 'Salva'), ['class' => 'btn btn-primary btn-salva']) ?>
                                <?php if (Yii::$app->session->hasFlash('oldPasswordNotValid')) { ?>
                                    <span class="js-old-password-help-block"><?= Yii::t('profile_password', 'Old Password is not valid'); ?></span>
                                <?php } ?>
                                <?php if (Yii::$app->session->hasFlash('saveSuccess')) { ?>
                                    <span class="js-old-password-help-block"><?= Yii::t('profile_password', 'Password success save'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                         <?php ActiveForm::end(); ?>
                    </div>
                </div>
             </section>
        </div>
    </div>

</div>
<script>
    $(function(){
        $(document).on('change', 'input', function(){
            $('.js-old-password-help-block').hide()
        })
    })
</script>
