<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();

/* @var $this yii\web\View */
/* @var $model common\models\User */

$class = '';
?>
<section class="menu-left">
    <div class="menu-content">
        <div class="menu_profile_left">
            <h3><?= Yii::t('profile_left_menu', 'Profilo'); ?></h3>
            <ul class="profile-nav">
                <li class="<?= $modelMenuUrl->checkUrl('profile/'); ?> <?= $modelMenuUrl->checkUrl('profile/default/index'); ?>" >
                    <a href="<?= Url::to(['/profile/default/index']); ?>"><?= Yii::t('profile_left_menu', 'Informazioni Personali'); ?></a>
                </li>
                <li class="<?= $modelMenuUrl->checkUrl('profile/default/avatar'); ?>" >
                    <a href="<?= Url::to(['/profile/default/avatar']); ?>"><?= Yii::t('profile_left_menu', 'Foto'); ?></a>
                </li>
                <li class="<?= $modelMenuUrl->checkUrl('profile/default/verification'); ?>" >
                    <a href="<?= Url::to(['/profile/default/verification']); ?>"><?= Yii::t('profile_left_menu', 'Verifiche'); ?></a>
                </li>
                <li class="<?= $modelMenuUrl->checkUrl('profile/default/hosts'); ?>" >
                    <a href="<?= Url::to(['/profile/default/hosts']); ?>"><?= Yii::t('profile_left_menu', 'Location'); ?></a>
                </li>
            </ul>
            <h3><?= Yii::t('profile_left_menu', 'Feedback'); ?></h3>
            <ul class="profile-nav">
                <li class="<?= $modelMenuUrl->checkUrl('profile/feedback') && array_key_exists('type', Yii::$app->request->get()) && ( Yii::$app->request->get('type') == 'to') ? ' active ' : ''; ?>">
                    <a href="<?= Url::to(['/profile/feedback', 'type' => 'to']); ?>">
                       <?= Yii::t('profile_left_menu', 'Feedback recivuti'); ?> 
                    </a>
                </li>

                <li class="<?= $modelMenuUrl->checkUrl('profile/feedback') && array_key_exists('type', Yii::$app->request->get()) && ( Yii::$app->request->get('type') == 'from') ? ' active ' : ''; ?>">
                    <a href="<?= Url::to(['/profile/feedback', 'type' => 'from']); ?>">
                        <?= Yii::t('profile_left_menu', 'Feedback inviati'); ?>
                    </a>
                </li>

            </ul>
            <h3><?= Yii::t('profile_left_menu', 'Account'); ?></h3>
            <ul class="profile-nav">
                <li class="<?= $modelMenuUrl->checkUrl('profile/default/password'); ?>">
                    <a href="<?= Url::to(['/profile/default/password']); ?>">
                        <?= Yii::t('profile_left_menu', 'Password'); ?>
                    </a>
                </li>
                <li class="<?= $modelMenuUrl->checkUrl('profile/default/delete-account'); ?>">
                    <a href="<?= Url::to(['/profile/default/delete-account']); ?>">
                        <?= Yii::t('profile_left_menu', "Chiudi l'account"); ?>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</section>