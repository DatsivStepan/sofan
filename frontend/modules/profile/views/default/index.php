<?php

use yii\helpers\Html,
    yii\widgets\DetailView,
    kartik\widgets\ActiveForm,
    kartik\date\DatePicker,
    common\models\Country,
    kartik\select2\Select2,
    common\models\UserVerification;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Informazioni Personali';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container user-view">
    <?= $this->render('/default/menu_top'); ?>

    <div class="row no-margin">
        <div class=" col-12 col-md-3 col-lg-2 left-block feedback-left-menu">
            <?= $this->render('/default/menu_left'); ?>
        </div>
        <div class="col-12 col-md-9 col-lg-10 js-events-index right-block">
            <div class="title-menu-profile">
                <span><?= Html::encode($this->title) ?></span>
            </div>
            <div class="row no-margin profil-block-right">
                <div class="col-12 col-sm-12 offset-sm-0 col-md-12 offset-md-0 col-lg-12 offset-lg-0 col-xl-10 offset-xl-1">
                    <?php $form = ActiveForm::begin(); ?>
                    <div id="profilo-id-form" class="row no-margin">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'Name'); ?></label>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12 no-padding-right-mob no-padding-left">
                            <?= $form->field($modelInfo, 'name')->textInput()->label(false); ?>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'Surname'); ?></label>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12 no-padding-right-mob no-padding-left">
                            <?= $form->field($modelInfo, 'surname')->textInput()->label(false); ?>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'Gender'); ?></label>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12 no-padding-right-mob no-padding-left">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-11 col-sm-11 col-12  no-padding-rigth padding-right-mob">
                                    <?= $form->field($modelInfo, 'gender')->dropDownList(['men', 'woman'])->label(false); ?>
                                </div>
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'Email'); ?></label>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-10 col-12 no-padding-left mob-pad-rigth-no">
                            <?= $form->field($model, 'email')->input('email')->label(false); ?>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'Phone Pref'); ?></label>
                        </div>
                        <div class="col-6 col-sm-5 col-md-3 col-lg-3 col-xl-3 no-padding-left">
                            <?= $form->field($modelInfo, 'phone_pref')->widget(Select2::className(), [
                                'data' => Country::getPhonePreList(),
                                'pluginOptions' => [
                                    'dropdownCssClass' => 'options-drop option-font',
                                    'allowClear' => true
                                ]
                            ]) ?>
                        </div>
                        <div class="col-6 col-sm-5 col-md-3 col-lg-3 col-xl-3 no-padding-left mob-pad-rigth-no">
                            <?= $form->field($modelInfo, 'phone')->textInput()->label(false); ?>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'Birthday'); ?></label>
                        </div>
                        <div id="calendar-birthday"
                             class="col-12 col-sm-5 col-md-6 col-lg-6 col-xl-6 no-padding-left mob-pad-rigth-no sm-pad-right">
                            <?= $form->field($modelInfo, 'birthday')->label(false)
                                ->widget(DatePicker::className(), [
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                    ]
                                ]) ?>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'Lang'); ?></label>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12 no-padding-left no-padding-right-mob">
                            <?= $form->field($modelInfo, 'lang')->dropDownList(['Italiano', 'English'], ['prompt' => 'Select'])->label(false); ?>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 no-padding-left">
                            <label class="control-label"><?= Yii::t('profile_update', 'About'); ?></label>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 no-padding-left mob-pad-rigth-no">
                            <?= $form->field($modelInfo, 'about')->textarea()->label(false); ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-4 no-padding-left non-inser-block">
                            <p><?= Yii::t('profile_update', 'Non inserire:'); ?></p>
                            <div class="row no-margin align-items-center marg-bot-import">
                                <?php if ($model->getUserVerificationStatus(UserVerification::TYPE_PHONE)) { ?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2 no-padding align-items-center">
                                        <img class="sp-img-about" src="/images/check-mark.png">
                                    </div>
                                    <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10 no-padding align-items-center">
                                        <span> <?= Yii::t('profile_update', 'Cellulare verificato'); ?></span>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2 no-padding align-items-center">
                                        <img class="sp-img-about" src="/images/ic_error_outline_24px.png">
                                    </div>
                                    <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10 no-padding align-items-center">
                                        <span> <?= Yii::t('profile_update', 'Cellulare non verificato'); ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row no-margin align-items-center marg-bot-import">
                                <?php if ($model->getUserVerificationStatus(UserVerification::TYPE_EMAIL)) { ?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2 no-padding align-items-center">
                                        <img class="sp-img-about" src="/images/check-mark.png">
                                    </div>
                                    <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10 no-padding align-items-center">
                                        <span> <?= Yii::t('profile_update', 'Email Verificata'); ?></span>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2 no-padding align-items-center">
                                        <img class="sp-img-about" src="/images/ic_error_outline_24px.png">
                                    </div>
                                    <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10 no-padding align-items-center">
                                        <span><?= Yii::t('profile_update', 'Email non Verificata'); ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row no-margin align-items-center marg-bot-import">
                                <?php if ($model->getUserVerificationStatus(UserVerification::TYPE_FACEBOOK)) { ?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2 no-padding align-items-center">
                                        <img class="sp-img-about" src="/images/check-mark.png">
                                    </div>
                                    <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10 no-padding align-items-center">
                                        <span><?= Yii::t('profile_update', 'Facebook verificato'); ?></span>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-2 col-xl-2 no-padding align-items-center">
                                        <img class="sp-img-about" src="/images/ic_error_outline_24px.png">
                                    </div>
                                    <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10 no-padding align-items-center">
                                        <span><?= Yii::t('profile_update', 'Facebook non verificato'); ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-12 no-padding-left">
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Salva'), ['class' => $model->isNewRecord ? 'btn btn-success btn-creat' : 'btn btn-primary btn-updat']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
