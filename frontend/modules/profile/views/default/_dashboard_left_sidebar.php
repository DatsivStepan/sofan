<?php

use yii\helpers\Html,
    yii\helpers\Url,
    common\models\UserVerification;

/* @var $this yii\web\View */
/* @var $model common\models\User */

?>
<div class="col-12  col-md-4 col-lg-4 left-dashboadr-block">
<section class="left-content">
    <div class="avatar-info-block"">
        <div class="row" style="padding: 13px 12px 8px;">
            <div class="col-12 col-lg-4 avatar-block">
                <img src="<?= $model->getAvatar(); ?>">
            </div>
            <div class="col-12 col-lg-8 info-block">
                <h6><?= $model->getUsernameWithSurname(); ?></h6>
                <div style="display: inherit;">
                   <?= Html::a('Modifica il mio profilo', Url::to(['/profile/default/index']))?>
                </div> 
                <div style="display:inherit;line-height: 13px;" class="text-block-ic">
                    <?= Html::a('Visualizza il mio profilo', Url::to([$model->getLink()]))?>
                </div>
            </div>
        </div>
    </div>

<hr style="margin: 15px 30px 0px;">
    <section class="verifiche-block">
    <div class="row">
        <div class="col-12">
            <div class="text-icon">
                <?php if ($model->getUserVerificationStatus(UserVerification::TYPE_PHONE)) { ?>
                    <img src="/images/check-mark.png">
                    <text> <?= Yii::t('profile_dashboard', 'Cellulare verificato'); ?></text>
                <?php } else { ?>
                    <img src="/images/ic_error_outline_24px.png">
                    <text style="color:#f1c40f"><?= Yii::t('profile_dashboard', 'Cellulare non verificato'); ?> </text>
                <?php } ?>
            </div>
            <div class="text-icon">
                <?php if ($model->getUserVerificationStatus(UserVerification::TYPE_EMAIL)) { ?>
                    <img src="/images/check-mark.png">
                    <text style="color: #59c50e"> <?= Yii::t('profile_dashboard', 'Email Verificata'); ?></text>
                <?php } else { ?>
                    <img src="/images/ic_error_outline_24px.png">
                    <text style="color: #f1c40f"> <?= Yii::t('profile_dashboard', 'Email non Verificata'); ?></text>
                <?php } ?>
            </div>
            <div class="text-icon">
                <?php if ($model->getUserVerificationStatus(UserVerification::TYPE_FACEBOOK)) { ?>
                    <img src="/images/check-mark.png">
                    <text style="color: #59c50e"><?= Yii::t('profile_dashboard', 'Facebook verificato'); ?></text>
                <?php } else { ?>
                    <img src="/images/ic_error_outline_24px.png">
                    <text style="color: #f1c40f"><?= Yii::t('profile_dashboard', 'Facebook non verificato'); ?></text>
                <?php } ?>
            </div>
            <div style="padding-top: 45px; padding-bottom: 10px;">
                <a href="/en/profile/default/verification" class="text"><?= Yii::t('profile_dashboard', 'Completa la verifica del profilo'); ?> </a>
            </div>
        </div>
    </div>
    </section>
    <hr style="margin: 20px 30px 40px;">
    <section class="activa">
    <div class="row">
        <div class="col-sm-12 col-md-12" style="padding-bottom: 10px;">
            <h6><?= Yii::t('profile_dashboard', "Attività"); ?></h6>
            <span><?= Yii::t('profile_dashboard', "Data d'iscrizione"); ?>  <?= $model->getDateCreate(); ?></span>
        </div>
    </div>
    </section>
</section>
</div>
