<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\UserVerification;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Verifiche';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
$userVerEmail = $model->getUserVerificationStatus(UserVerification::TYPE_EMAIL);
$userVerPhone = $model->getUserVerificationStatus(UserVerification::TYPE_PHONE);
$userVerFacebook = $model->getUserVerificationStatus(UserVerification::TYPE_FACEBOOK);

?>
<div class="user-view">

    <?= $this->render('menu_top'); ?>

    <section class="container site-content">
        <div class="row no-gutters">
            <div class="col-12 col-md-3">
                <?= $this->render('menu_left'); ?>
            </div>
            <div class="col-md-9">
                <section class="verifiche-info">
                    <div class="title-menu-profile">
                        <span><?= Html::encode($this->title) ?></span>
                    </div>
                    <div class="row no-margin align-items-center verifiche-block-info">
                        <p class="verifield">
                            <?php if ($userVerEmail != UserVerification::STATUS_ACTIVE) { ?>
                                <img class="img-warning-varif" src="/images/ic_error_outline_24px.png">
                                <?= Yii::t('profile_verification', 'Email non verificata'); ?>
                            <?php } else { ?>
                               <?= Yii::t('profile_verification', 'Email verificata'); ?> 
                            <?php } ?>
                        </p>
                        <div class="w-100"></div>
                        <div class="col-12 col-md-8 no-padding-left">
                            <p class="p-text-mail-verifich"><?= Yii::t('profile_verification', 'Il tuo indirizzo email e:'); ?> <span><?= $model->email; ?></span>
                            </p>
                            <p class="p-text-mail-verifich"><?= Yii::t('profile_verification', 'Con il tuo indizzo email verificato, possiamo  facilmente contattarti se neccessario'); ?></p>
                        </div>
                        <div class="col-12 col-md-4 align-items-center js-confirm-email-block">
                            <?php if ($userVerEmail === UserVerification::STATUS_NOT) { ?>
                                <button class="btn btn-conferma js-confirm-email-button"><?= Yii::t('profile_verification', 'conferma'); ?></button>
                            <?php } elseif ($userVerEmail === UserVerification::STATUS_NOT_ACTIVE) { ?>
                                <p class="js-confirm-email-help email-help-verifich"><?= Yii::t('profile_verification', 'Il link di conferma è stato inviato
                                    via email'); ?></p>
                                <button class="btn btn-conferma js-confirm-email-button"><?= Yii::t('profile_verification', 'inviare di nuovo'); ?></button>
                            <?php } else { ?>
                                <img class="group-img-style" src="/images/group_192.png">
                            <?php } ?>
                        </div>
                    </div>


                    <div class="row no-margin align-items-center verifiche-block-info">
                        <p class="verifield">
                            <?php if (!$userVerPhone) { ?>
                                <img class="img-warning-varif" src="/images/ic_error_outline_24px.png">
                                <?= Yii::t('profile_verification', 'Telefono non verificato'); ?>
                            <?php } else { ?>
                                <?= Yii::t('profile_verification', 'Telefono verificato'); ?>
                            <?php } ?>
                        </p>
                        <div class="w-100"></div>
                        <div class="col-12 col-md-8 no-padding-left">
                            <p class="p-text-mail-verifich"><?= Yii::t('profile_verification', 'Il tuo numero di telefono e:'); ?>
                                <span><?= $model->getPhone(); ?></span>
                                <span class="modif-cs-text">
                                    <a href="<?= Url::to(['/profile/default/index']); ?>"><?= Yii::t('profile_verification', 'Modificare'); ?></a>
                                </span>
                            </p>
                            <p class="p-text-mail-verifich"><?= Yii::t('profile_verification', 'Con il tuo numero di telefono Verificato possiamo  facilmente contattarti se
                                neccessario'); ?> </p>
                        </div>
                        <div class="col-12 col-md-4 align-items-center">
                            <?php if (!$userVerPhone) { ?>
                                <div class="js-confirm-form" style="display:none;">
                                    <p class="js-confirm-phone-text p-phone-text"></p>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control js-confirm-phone-code"
                                                   placeholder="code">
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-conferma js-confirm-phone-save"><?= Yii::t('profile_verification', 'Save'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <button class="btn btn-conferma js-confirm-form-show" <?= !$model->getPhone() ? 'disabled="disabled"' : ''; ?>>
                                   <?= Yii::t('profile_verification', 'conferma'); ?> 
                                </button>

                            <?php } else { ?>
                                <img class="group-img-style" src="/images/group_192.png">
                            <?php } ?>
                        </div>
                    </div>


                    <div class="row no-margin align-items-center verifiche-block-info">
                        <p class="verifield">
                            <?php if (!UserVerification::getVerificationStatus(UserVerification::TYPE_EMAIL, $model->email, $model->id)) { ?>
                                <img class="img-warning-varif" src="/images/ic_error_outline_24px.png">
                            <?php } ?>
                            <?= Yii::t('profile_verification', 'Collega il tuo account Facebook'); ?></p>
                        <div class="w-100"></div>
                        <div class="col-12 col-md-8 no-padding-left">
                            <p class="p-text-mail-verifich"><?= Yii::t('profile_verification', 'Con il tuo account facebook collegato i Guest possono vedere quanti amici hai ed
                                aumenti la possibilitа di essere scelto da loro'); ?></p>
                        </div>
                        <div class="col-12 col-md-4 align-items-center style-share style-share-verifiche">

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $(document).on('click', '.js-confirm-email-button', function () {
            var thisEl = $(this)
            thisEl.attr('disabled', true)
            $('.js-confirm-email-help').text('')
            $.ajax({
                url: '<?= Url::to(['/profile/default/confirm-email']) ?>',
                method: 'get',
                dataType: 'json',
                success: function (response) {
                    thisEl.attr('disabled', false)
                    var text = ''
                    if (response.status) {
                        text = 'Il link di conferma è stato inviato via email';
                    } else {
                        text = 'щось пішло не так';
                    }

                    if ($('.js-confirm-email-help').length) {
                        $('.js-confirm-email-help').text(text)
                    } else {
                        $('.js-confirm-email-block').append('<p class="js-confirm-email-help">' + text + '</p>')
                    }
                }
            })
        })

        $(document).on('click', '.js-confirm-form-show', function () {
            $(this).hide();
            $('.js-confirm-form').show();

            $.ajax({
                url: '<?= Url::to(['/profile/default/confirm-phone']) ?>',
                method: 'get',
                data: {type: 'start'},
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        $('.js-confirm-phone-text').text('Inserisci il codice di verifica che hai ricevuto via SMS');
                    } else {
                        $('.js-confirm-phone-text').text('Qualcosa è andata storta, Riprova o contatta il supporto tecnico');
                    }
                }
            })
        })

        $(document).on('click', '.js-confirm-phone-save', function () {
            var code = $('.js-confirm-phone-code').val();
            console.log(code.length);
            if (code.length >= 4) {
                $.ajax({
                    url: '<?= Url::to(['/profile/default/confirm-phone']) ?>',
                    method: 'get',
                    data: {type: 'check', code: code},
                    dataType: 'json',
                    success: function (response) {
                        if (response.status) {
                            swal({
                                title: "",
                                className: "sweet-alert-css",
                                text: "Verifica avvenuta con sucesso",
                                button: false,
                                timer: 2000,
                            }).then((value) = > {
                                location.reload()
                        })
                            ;
                        } else {
                            $('.js-confirm-phone-text').text('Hai inserito un codice di verifica errato');
                        }
                    }
                })
            } else {
                $('.js-confirm-phone-text').text('Il codice di verifica non può essere meno di 4 cifre ');
            }
        })
    })
</script>