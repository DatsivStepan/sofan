<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();
/* @var $this yii\web\View */
/* @var $model common\models\User */

$class = '';
?>
<section class="container menu_content">
    <div class="menu_profile">
        <ul class="profile-nav">
            <li class="<?= $modelMenuUrl->checkUrl('profile/default/dashboard'); ?>" >
                <a href="<?= Url::to(['/profile/default/dashboard']); ?>"><?= Yii::t('profile_top_menu', 'Dashboard'); ?></a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('profile/events'); ?>" >
                <a href="<?= Url::to(['/profile/events/']); ?>"><?= Yii::t('profile_top_menu', 'Eventi organizzati'); ?></a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('profile/events-booking'); ?>" >
                <a href="<?= Url::to(['/profile/events-booking/']); ?>"><?= Yii::t('profile_top_menu', 'Prenotazioni'); ?></a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('profile/messages'); ?>" >
                <a href="<?= Url::to(['/profile/messages']); ?>"><?= Yii::t('profile_top_menu', 'Messagi'); ?></a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('profile/notifications'); ?>" >
                <a href="<?= Url::to(['/profile/notifications']); ?>"><?= Yii::t('profile_top_menu', 'Notifiche'); ?></a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('profile/'); ?> <?= $modelMenuUrl->checkUrl('profile/default/index'); ?>" >
                <a href="<?= Url::to(['/profile']); ?>/"><?= Yii::t('profile_top_menu', 'Profilo'); ?></a>
            </li>
        </ul>
    </div>
    
</section>