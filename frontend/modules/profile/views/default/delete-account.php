<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'delete account';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container delete-account">
    <?= $this->render('/default/menu_top'); ?>

    <div class="row">
        <div class=" col-12  col-md-3 col-lg-2 left-block feedback-left-menu"> 
            <?= $this->render('/default/menu_left'); ?>
        </div>
        <div class="col-12 col-md-9 col-lg-10 js-events-index right-block"> 
             <div class="row name">
                <div class="col-12">
                    <h5><?= Yii::t('profile_delete_account', 'Chiudi il tuo account'); ?></h5>
                     <h6><?= Yii::t('profile_delete_account', 'Se desideri veramente chiudere il tuo account, ti chiediamo gentilmente di indicarci il motivo della tua scelta per aiutarci a  migliorare il nostro servizio'); ?></h6>
                </div>
            </div>
                <section class="right">
                    <div class="row">
                        <div class="col-12 delete-form-block">
                            <?php $form = ActiveForm::begin(); ?>
                              <div class="form-group">
                                <label for="exampleFormControlInput1"><?= Yii::t('profile_delete_account', 'Motivo:'); ?></label>
                                <div class="col-12 col-sm-5 col-lg-3 select-block">
                                    <?= $form->field($modelNewD, 'why')->dropDownList(['Scegli', 'second'])->label(false); ?>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="exampleFormControlInput1"><?= Yii::t('profile_delete_account', 'Come possiamo migliorarci?'); ?></label>
                                <div class="col-sm-8 select-block">
                                   <?= $form->field($modelNewD, 'why_not')->textarea()->label(false); ?>
                                </div>
                              </div>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Chiudi il mio account'), ['class' => 'btn btn-primary btn-delete']) ?>
                            </div>

                             <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </section>
        </div>
    </div>

</div>
