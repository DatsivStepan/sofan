<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\EventsBookings;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container default-host-page">
    <?= $this->render('/default/menu_top'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class=" col-12  col-md-3 col-lg-2 left-block"> 
            <?= $this->render('menu_left'); ?>
        </div>
        <div class="col-12 col-md-9 col-lg-10 js-events-index right-block right-block-host"> 
            <div class="row">
                <?php if (Yii::$app->session->hasFlash('saveSuccess')) { ?>
                    <script>
                        const imageURL = '/images/group_192.png';
                        swal({
                            title: "",
                            className: "sweet-alert-css",
                            text: "Your Location Updated",
                            button: false,
                            icon: imageURL,
                            timer: 2000,
                        });
                    </script>
                <?php } ?>
            </div>

            <div class="row">
                <div class="col-12 name">
                    <h6><?= Yii::t('profile_hosts', 'Le mie Location'); ?></h6>
                </div>
            </div>
            
            <?php if ($dataProvider->getModels()) { ?>
                <?php foreach ($dataProvider->getModels() as $model) { ?>
                <section class="right">
                    <div class="row">
                        <div class="col-12  location-block">
                            <div class="row">
                                <div class="col-sm-6 col-lg-3 image-block">
                                    <img src="/images/default_avatar1.jpg">
                                </div>
                                <div class="col-sm-6 adress-block">
                                  <h6>  <?= $model->city; ?><br>
                                    <?= $model->street_name . ' ' . $model->street_number; ?>
                                </h6>
                                </div>
                                <div class="col-12 col-lg-3 button-block">
                                    <?= Html::a('Modifica Location', Url::to(['/profile/host/update', 'id' => $model->id]), ['class' => 'btn btn-modifica']); ?>

                                    <?= Html::button('Elimina Location', [
                                        'class' => 'btn btn-danger js-delete-host btn-location',
                                        'data-id' => $model->id
                                    ]); ?>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php } ?>
            <?php } ?>
            <section class="right-button">
            <div class="row">
                <div class="col-12 col-sm-5 col-lg-3 offset-sm-7 offset-lg-9 location-block-button" style="text-align: right;">
                    <?= Html::a('+ Aggiungi Location', Url::to(['/profile/host/create']), ['class' => 'btn btn-aggiungi'])?>
                </div>
            </div>
        </section>
            
        </div>
    </div>
</div>

<script>
    $(function(){
        
        $(document).on('click', '.js-delete-host', function () {
            var id = $(this).data('id')
            $.confirm({
                id: 'deleteLocModal',
                title: '',
                type: 'blue',
                content: "url:<?= Url::to(['/profile/host/delete']); ?>/" + id,
                containerFluid: true,
                columnClass: 'col-11 col-sm-11 col-md-11 col-lg-9 col-xl-7 delete-location',
                buttons: {
                    close: {
                        text: 'Anulla',
                        btnClass: 'btn-default',
                    },
                    ok: {
                        text: 'Delete location',
                        btnClass: 'btn-danger',
                    },
                }
            });
        })

        $(document).on('click', '.js-open-booking-modal', function () {
            var id = $(this).data('id')
        })
    })
</script>