<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('menu_top'); ?>

    <section class="container site-content">
        <div class="row no-gutters">
            <?= $this->render('_dashboard_left_sidebar', [
                'model' => $model
            ]); ?>
            
            <div class="col-12  col-md-8 col-lg-8">
                <section class="right-block">
                    <div class="row">
                        <div class="col-6 name">
                            <img src="/images/notification.png">
                            <h2>
                                <?= Yii::t('profile_dashboard', 'Notifiche'); ?>
                                 (<?= count($notifications); ?>)
                             </h2>
                        </div>
                        <div class="col-6" style="text-align: right;padding-right: 30px;">
                            <a href="/en/profile/notifications">
                                <?= Yii::t('profile_dashboard', 'Tutte le notifiche'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="right-scroll">
                    <?php if ($notifications) { ?>
                        <?php foreach ($notifications as $notification) { ?>
                            <div class="row dachboadrd-block">
                                <div class="col-3 col-lg-1 img-block">
                                    <img src="/images/user.png">
                                </div>
                                <div class="col-9 col-lg-5 text-block">
                                    <p><a href="<?= $notification->getLink(); ?>"><?= $notification->getShortNote(); ?></a></p>
                                </div>
                                <div class="col-11 col-lg-6 d-not-button-block">
                                    <?= $this->render('/notifications/_action_buttons', [
                                        'model' => $notification
                                    ])?>
                                </div>
                                <div class="col-4 offset-8 date">
                                    <p>
                                        <?= $notification->getDateCreate(' d/m/Y H:i'); ?>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                    <?php }else{ ?>
                        <div class="col-12 d-not-found">
                            <?= Yii::t('profile_dashboard', 'You do not have any new notifications'); ?>
                        </div>
                    <?php } ?>
                </div>
                </section>


                <section class="right-block right-block-2">
                    <div class="row">
                        <div class="col-8 name">
                            <img src="/images/notification (2).png">
                            <h2>
                                <?= Yii::t('profile_dashboard', 'Nuovi messaggi'); ?>
                                (<?= count($massagesD); ?>)
                            </h2>
                        </div>
                        <div class="col-4" style="text-align: right;padding-right: 30px;">
                            <a href="/en/profile/messages">
                                <?= Yii::t('profile_dashboard', 'Tutte I messaggi'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="right-scroll">
                    <?php if ($massagesD) { ?>
                        <?php foreach ($massagesD as $dialog) { ?>
                            <?php if ($message = $dialog->lastMessage(Yii::$app->user->id)) { ?>
                                <?php $user = $dialog->getUserData(); ?>
                                <div class="row dachboadrd-block">
                                    <div class="col-3 col-lg-1 img-block">
                                        <img src="<?= $user ? $user->getAvatar() : ''; ?>">
                                    </div>
                                    <div class="col-9 col-lg-5 text-block">
                                        <p><?= $message->message; ?></p>
                                    </div>
                                    <div class="col-11 col-lg-6 d-not-button-block">
                                        <a href="<?= Url::to(['/profile/messages/' . $dialog->id]); ?>" type="button" class="btn btn-primary">
                                            <?= Yii::t('profile_dashboard', 'Rispondi'); ?>
                                        </a>
                                    </div>
                                    <div class="col-4 offset-8 date">
                                        <p>
                                            <?= Yii::t('profile_dashboard', '22/10/2014 23.21'); ?>
                                        </p>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    <?php }

                    else{ ?>
                        <div class="col-12 d-not-found">
                            <?= Yii::t('profile_dashboard', 'You do not have any new messages'); ?>
                        </div>
                    <?php } ?>
                </div>
                </section>
            </div>

        </div>
    </section>
</div>
