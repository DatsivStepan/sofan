<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \bigpaulie\social\share\Share;
use budyaga\cropper\Widget;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'La mia Foto';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .hidden{
        display:none;
    }
    .new_photo_area .cropper_label.hidden {
        display:none;
    }
    .field-userinfo-avatar .cropper-buttons .delete-photo{
        position: absolute;
        top: 42px;
        right: 73px;
        padding: 0px;
        background-color: transparent;
        background-image: url(/images/delete.png);
        background-position: center;
        background-size: 30px;
        height: 30px;
        width: 30px;
        border:0px;
        background-repeat: no-repeat;
    }
    .field-userinfo-avatar .cropper-buttons .delete-photo:active{
        background-color: transparent;
        background-image: url(/images/delete.png);
        background-position: center;
    }
    .field-userinfo-avatar .cropper-buttons .crop-photo,
    .field-userinfo-avatar .cropper-buttons .upload-new-photo{
        background:transparent;
        color: #5C5E60;
        border:0px;
        font-size: 12px;
        font-weight: 400;
        font-family: "open-sans"
    }
    .field-userinfo-avatar .cropper-buttons .crop-photo:active,
    .field-userinfo-avatar .cropper-buttons .crop-photo:focus,
    .field-userinfo-avatar .cropper-buttons .crop-photo:hover,
    .field-userinfo-avatar .cropper-buttons .upload-new-photo:active,
    .field-userinfo-avatar .cropper-buttons .upload-new-photo:focus,
     .field-userinfo-avatar .cropper-buttons .upload-new-photo:hover{
        color: #5C5E60 !important;
        border:0px !important;
        box-shadow: none !important;
        background-color: transparent;
    }
    .field-userinfo-avatar .cropper-buttons .glyphicon{
        color: #5C5E60;
        font-size: 14px;
        padding-right: 6px;
        padding-top: 3px;
    }
    .field-userinfo-avatar .cropper-buttons .delete-photo:focus{
        border-radius: 100px;
    }
    .field-userinfo-avatar .cropper_widget img.thumbnail{
        min-height: 200px;
        min-width: 200px;
        border-radius:50%;
    }
    .new-photo-area .cropper-label.hidden {
        display:none;
    }
    .field-userinfo-avatar .cropper-widget img.thumbnail{
        min-height: 200px;
        min-width: 200px;
        border-radius:50%;
    }
    .cropper-widget{
        text-align: center;
    }
    @media (min-width: 1200px){
        .field-userinfo-avatar .cropper-buttons .delete-photo{
            right: 108px !important;
        }
        .new-photo-area{
            margin:20px auto !important;
        }

        .field-userinfo-avatar{
            margin-bottom: 40px;
        }
    }
</style>
<div class="user-view">

    <?= $this->render('menu_top'); ?>

    <section class="container site-content">
        <div class="row no-gutters">
            <div class="col-12 col-md-3">
                <?= $this->render('menu_left'); ?>
            </div>
            <div class="col-md-9">
                <?php $form = ActiveForm::begin(['id' => 'form-profile']); ?>
                    <section class="personal-information">
                        <div class="title-menu-profile">
                            <span><?= Html::encode($this->title) ?></span>
                        </div>
                        <div class="row no-margin la-mia-foto align-items-center" style="height:auto !important;">
                            <div class="col-12 col-md-6" style="padding-top: 40px;">
                                <?= $form->field($modelInfo, 'avatar')->widget(Widget::className(), [
                                    'uploadUrl' => Url::toRoute('/profile/default/uploadPhoto'),
                                    'width' => '200',
                                    'height' => '200',
                                    'noPhotoImage' => '/images/user.png'
                                ])->label(false) ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <p class="text-p-bot">
                                        <?= Yii::t('profile_avatar', 'Aggiungi una foto! Ispirerai più fiducia agli altri utenti e verrai contattato/a più
                                        spesso. Inoltre sarà più facile riconoscersi nel punto di ritrovo concordato.'); ?>
                                    </p>
                                    <?= Html::submitButton(Yii::t('profile_avatar', 'Salva'), ['class' => 'btn salva-btn']) ?>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>
</div>
<script>
    $(function(){
        $(document).on('click', '.field-userinfo-avatar .cropper_widget img.thumbnail, .field-userinfo-avatar .cropper-widget img.thumbnail', function(){
            if ($('.upload-new-photo').length){
                $('.upload-new-photo').trigger('click')
            }
            if ($('.upload_new_photo').length){
                $('.upload_new_photo').trigger('click')
            }
        })
    })
</script>