<?php

use yii\helpers\Html,
    common\models\EventsBookings,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('booking', 'Prenotazioni Future');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container">
    <?= $this->render('/default/menu_top'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class=" col-12 col-md-2 margin-bot-left-menu">
            <?= $this->render('_left_menu'); ?>
        </div>
        <div class="col-12 col-md-10 js-events-index">
            <?php if ($dataProvider->getModels()) { ?>
                <?php foreach ($dataProvider->getModels() as $model) { ?>
                    <div class="row">
                        <div class="event-for-two-half offset-2 offset-sm-0 col-8 col-sm-6 col-lg-4" style="padding-top: 15px;">
                            <?= $this->render('/events/_event_template',[
                                'model' => $model
                            ]); ?>
                        </div>
                        <div class="event-for-two-half two-half offset-2 offset-sm-0 col-8 col-sm-6 col-lg-8">
                            <div class="col-12 text-name-block">
                                <h6><?= Yii::t('profile_eb_index', 'Prenotazioni Richieste'); ?> </h6>
                            </div>
                            <?php if ($model->getMyEventsBooking()) { ?>
                                <?php foreach ($model->getMyEventsBooking() as $bookings) { ?>
                                    <div class="row info-block">
                                        <div class="col-12 col-lg-4 js-open-booking-modal first-block" data-status="<?= $bookings->status; ?>" data-id="<?= $bookings->id; ?>">
                                            <?php if ($bookings->status == EventsBookings::STATUS_ON_CONFIRM) { ?>
                                                <img src="/images/ic_error_outline_24px.png" class="icon-right">
                                            <?php } elseif ($bookings->status == EventsBookings::STATUS_CONFIRM) { ?>
                                                <img src="/images/susses-icon.png" class="icon-right">
                                            <?php } elseif ($bookings->status == EventsBookings::STATUS_REJECTED) { ?>
                                                <img src="/images/delete.png" class="icon-right">
                                            <?php } ?>
                                            <?= $bookings->getName(); ?>
                                        </div>
                                        <div class="col-12 col-lg-5 three-block">
                                            <div class="col-12 text-w">
                                                <?php if ($user = $bookings->userModel) { ?>
                                                    <img src="<?= $user->getAvatar(); ?>">
                                                    <p><?= $user->getUsernameWithSurname(); ?>  <?= $user->getAge() ? ', '.$user->getAge() : ''; ?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-2 four-block">
                                            <p><?= $bookings->getDateCreate(); ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <p class="p-no-message text-center"><?= Yii::t('profile_eb_index', 'We dont Have .....'); ?> </p>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".arrow-bokings").on('click', function () {
            $(this).toggleClass("arrow-bokings-top");
        });
    });
    $(function () {

        $(document).on('click', '.js-open-booking-modal', function () {
            var id = $(this).data('id')
            var status = $(this).data('status')
            if (status == '<?= EventsBookings::STATUS_ON_CONFIRM; ?>') {
                $.confirm({
                    id: 'bookingModal',
                    title: '',
                    type: 'blue',
                    content: "url:<?= Url::to(['/profile/events/booking-confirm']); ?>/" + id,
                    containerFluid: true,
                    columnClass: 'col-11 col-sm-11 col-md-11 col-lg-9 col-xl-7 prenoto-modal-class',
                    buttons: {
                        close: {
                            text: 'Anulla',
                            btnClass: 'btn-default'
                        },
                        delete: {
                            text: 'Not confirm',
                            btnClass: 'btn-danger',
                            action: function (button) {
                                deleteEventBooking(id);
                                return false;
                            }
                        },
                        ok: {
                            text: 'Confirm',
                            btnClass: 'btn-primary',
                        },
                    }
                });
            }
        })

        function deleteEventBooking(id) {
            $.ajax({
                url: '<?= Url::to(['/profile/events/booking-delete']) ?>/' + id,
                method: 'post',
                dataType: 'json',
                success: function (response) {
                    $modal.bookingModal.close();
                    location.reload();
                }
            })
        }
    })
</script>