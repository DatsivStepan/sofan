<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\profile\models\EventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .navbar-nav li.active a{
        color:#0080FF !important;
    } 
</style>
<nav class="navbar navbar-expand navbar-light left-menu">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ">
            <li class="nav-item <?= !array_key_exists('type', Yii::$app->request->get()) ? ' active ' : ''; ?>">
                <a class="nav-link"  href="<?= Url::to(['/profile/events-booking']); ?>">
                    <?= Yii::t('booking', 'Prenotazioni Future'); ?>
                </a>
            </li>
            <li class="nav-item <?= array_key_exists('type', Yii::$app->request->get()) && ( Yii::$app->request->get('type') == 'pass') ? ' active ' : ''; ?>">
                <a class="nav-link" href="<?= Url::to(['/profile/events-booking?type=pass']); ?>">
                    <?= Yii::t('booking', 'Prenotazioni passate'); ?>
                </a>
            </li>
            <li class="nav-item <?= array_key_exists('type', Yii::$app->request->get()) && ( Yii::$app->request->get('type') == 'canceled') ? ' active ' : ''; ?>">
                <a class="nav-link" href="<?= Url::to(['/profile/events-booking?type=canceled']); ?>">
                    <?= Yii::t('booking', 'Prenotaz. Archeviate'); ?>
                </a>
            </li>
        </ul>
    </div>
</nav>
  
