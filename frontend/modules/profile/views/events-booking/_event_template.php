<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Events;

/* @var $this yii\web\View */
/* @var $model frontend\modules\profile\models\EventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="js-search-block-event" style="position:relative;background-image: url('<?= $model->getDefaultImage(); ?>');overflow: hidden;">
    <a href="<?= $model->getLink();  ?>">
        <input type="hidden" class="js-lat" value="<?= $model->hostModel ? $model->hostModel->lat : 0; ?>">
        <input type="hidden" class="js-lng" value="<?= $model->hostModel ? $model->hostModel->lng : 0; ?>">
        <input type="hidden" class="js-price" value="<?= $model->payment_per_person; ?>">
        <div class="right-img-block">
            <div class="event-user-block block-user text-center">
                <img src="<?= $model->userModel ? $model->userModel->getAvatar() : ''; ?>" >
                <span><?= $model->userModel ? $model->userModel->getUsernameWithSurname() : ''; ?></span>
                <div style="display: inline-flex;">
                    <img src="/images/favorite.png" class="star">
                    <img src="/images/favorite.png" class="star">
                    <img src="/images/favorite.png" class="star">
                    <img src="/images/favorite off.png" class="star">
                    <img src="/images/favorite off.png" class="star">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="event-user-block-1">
                <span> <?= $model->getName(); ?> </span>
                <p class="data">
                    <?= $model->getDateStart(); ?>
                </p>
                <p>

                </p>
                <p class="place">
                    <img src="/images/ic_room_241px.png" style="height: 8px; width: 5px;margin-right: 2px;">
                    <?= $model->getAddress(); ?>
                </p>
                <p class="payment">
                    <?= $model->getPaymentPerPerson(); ?>
                </p>
                <?php if ($model->status = Events::STATUS_NOT_ACTIVE) { ?>
                        <div class="red-opacity-block" style="background-color: rgba(255, 0, 0, 0.62);margin-top:5px;line-height: 14px;height: 30px;color:white;">
                            <h4 class="h4-finito">Finito</h4>
                        </div>
                <?php } elseif ($model->status = Events::STATUS_DELETED) { ?>
                        <div class="red-opacity-block" style="background-color: rgba(255, 0, 0, 0.62);margin-top:5px;line-height: 14px;height: 30px;color:white;">
                            <h4 style="color:white;">CANCELLATO</h4>
                        </div>
                <?php } elseif ($model->status = Events::STATUS_ACTIVE) { ?>
                        <div class="red-opacity-block" style="background-color: rgba(255, 0, 0, 0.62);margin-top:5px;line-height: 14px;height: 30px;">
                                <p class="button-text">
                                Iscrezione Scade Fra:
                            </p>
                            <p class="time">
                               8 Ore
                            </p>
                        </div>
                <?php } ?>
            </div>
        </div>
    </a>
</div>
