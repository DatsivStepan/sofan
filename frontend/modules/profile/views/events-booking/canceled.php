<?php

use yii\helpers\Html,
    yii\grid\GridView,
    yii\widgets\Pjax,
    common\models\EventsBookings;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('booking', 'Prenotaz. Archeviate');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index container">
    <?= $this->render('/default/menu_top'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class=" col-12 col-md-2 margin-bot-left-menu">
            <?= $this->render('_left_menu'); ?>
        </div>
        <div class="col-12 col-md-10 js-events-index"> 
            <?php if ($dataProvider->getModels()) { ?>
                    <?php foreach ($dataProvider->getModels() as $model) { ?>
                        <div class="row">
                            <div class="event-for-two-half offset-2 offset-sm-0 col-8 col-sm-6 col-lg-4" style="padding-top: 15px;">
                                <?= $this->render('/events/_event_template',[
                                    'model' => $model
                                ]); ?>
                            </div>
                            <div class="event-for-two-half two-half offset-2 offset-sm-0 col-8 col-sm-6 col-lg-8">
                                <div class="col-12 text-name-block">
                                    <h6>  <?= Yii::t('profile_eb_canceled', 'Prenotazioni Richieste'); ?></h6>
                                </div>
                                <?php if ($model->getMyEventsBooking()) { ?>
                                    <?php foreach ($model->getMyEventsBooking() as $bookings) { ?>
                                        <?php if (($bookings->status == EventsBookings::STATUS_CONFIRM)) { ?>
                                            <?php continue; ?>
                                        <?php } ?>
                                        <div class="row info-block">
                                            <div class="col-12 col-lg-4 js-open-booking-modal first-block" data-status="<?= $bookings->status; ?>" data-id="<?= $bookings->id; ?>">
                                                <?php if ($bookings->status == EventsBookings::STATUS_ON_CONFIRM) { ?>
                                                    <img src="/images/ic_error_outline_24px.png" class="icon-right">
                                                <?php } elseif ($bookings->status == EventsBookings::STATUS_CONFIRM) { ?>
                                                    <img src="/images/susses-icon.png" class="icon-right">
                                                <?php } elseif ($bookings->status == EventsBookings::STATUS_REJECTED) { ?>
                                                    <img src="/images/delete.png" class="icon-right">
                                                <?php } ?>
                                                <?= $bookings->getName(); ?>
                                            </div>
                                            <div class="col-12 col-lg-5 three-block">
                                                <div class="col-12 text-w">
                                                    <?php if ($user = $bookings->userModel) { ?>
                                                        <img src="<?= $user->getAvatar(); ?>">
                                                        <p><?= $user->getUsernameWithSurname(); ?>  <?= $user->getAge() ? ', '.$user->getAge() : ''; ?></p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-2 four-block">
                                                <p><?= $bookings->getDateCreate(); ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
            <?php } else { ?>
                <p class="p-no-message text-center"><?= Yii::t('profile_eb_canceled', 'We dont Have .....'); ?></p>
            <?php } ?>
        </div>
    </div>
</div>
