<?php

use yii\helpers\Html,
    yii\helpers\ArrayHelper,
    common\models\Image;

$this->registerJsFile('/js/plugins/dropzone.js');

/* @var $this yii\web\View */
/* @var $model common\models\Events */

?>
<div class="events-create">
    <div class="row w-100 align-items-center no-padding no-margin">
        <div class="max-whith-block-even col-8 offset-2 text-center align-items-center heigth-img-block js-add-images">
            <div class="row w-100 no-gutters align-items-center no-padding div-click-3">
                <div class="col-12 div-click-2">
                    <img class="img-foto-style" src="/images/upload-foto.png">
                </div>
                <div class="col-12 control-label div-click-1">
                    Trascina qui le immagini o
                    clicca per selezionarle
                </div>
            </div>
        </div>
    </div>
    <div class="row w-100 no-padding no-margin">
        <div class="col-4 text-right ">
            <div class="js-images-block">
                <?php if ($images = $model->image) { ?>
                    <?php foreach ($images as $image) { ?>
                        <div class="js-block-image">
                            <?= $form->field($model, 'image[]')->hiddenInput(['value' => $image])->label(false) ?>
                            <img src="/<?= $image; ?>">
                            <span class="js-delete-image"></span>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        if ($('.js-add-images').length) {
            $(document).find('body').append('<div style="display:none;">' +
                '<div class="row-table-style">' +
                '<div class="table table-striped" class="files" id="previews">' +
                '<div id="template" class="file-row">' +

                '</div>' +
                '</div>' +
                '</div>' +
                '</div>')

            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzone = new Dropzone($('.js-add-images')[0], {
                url: "/site/save-image?path=hosts",
                previewTemplate: previewTemplate,
                previewsContainer: "#previews",
                acceptedFiles: 'image/*',
                clickable: ".js-add-images, .div-click-1, .div-click-2",
//                    uploadMultiple:true,
//                maxFiles: 2,
            });

//            myDropzone.on("maxfilesexceeded", function (file) {
//                    myDropzone.removeAllFiles();
//                    myDropzone.addFile(file);
//            });

//            myDropzone.on("sending", function (response) {
//                alert('dfdf')
//            })
            myDropzone.on("complete", function (response) {
                if (response.status == 'success') {
                    console.log($('.js-block-image').length);
                    if ($('.js-block-image').length < 4) {
                        $('.js-images-block').append('<div class="js-block-image">' +
                            '<input type="hidden" value="' + response.xhr.response + '" class="form-control" name="HostForm[image][]">' +
                            '<img src="/' + response.xhr.response + '">' +
                            '<span class="js-delete-image" style="cursor:pointer"></span>' +
                            '</div>');
                    } else {
                        const imageURL = '/images/group_590.png';
                        swal({
                            title: "",
                            className: "sweet-alert-css",
                            text: "max 4 img",
                            button: false,
                            icon: imageURL,
                            timer: 2000,
                        });
                    }
                }
            });

            myDropzone.on("removedfile", function (response) {
                if (response.xhr != null) {
                    //deleteFile(response.xhr.response);
                }
            });

            $(document).on('click', '.js-delete-image', function () {
                $(this).closest('.js-block-image').remove();
            })
        }
    })
</script>