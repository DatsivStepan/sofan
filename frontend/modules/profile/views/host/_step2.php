<?php

use yii\widgets\ActiveForm,
    common\models\Host;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model \frontend\models\HostForm */

?>

<section class="hai-una2">
    <div class="container-fluid">
        <div class="block-row">
            <?php $form = ActiveForm::begin([
                'id' => 'host_form',
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <div class="event-block-left adress-host-block">
                <div class="row w-100 no-padding border-bot">
                    <div class="col-12 offset-0 col-sm-12 offset-sm-0 col-md-10 offset-md-1 col-ld-10 offset-lg-1 col-xl-8 offset-xl-2 no-padding">
                        <h2 class="h2-style-adress">2. Location</h2>
                        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>

                        <?= $form->field($model, 'location_type')->widget(Select2::className(), [
                            'data' => [
                                1 => 'Appartamento',
                                2 => 'casa in cittа',
                                3 => 'casa in campagna',
                                4 => 'openspace',
                                5 => 'Altro'
                            ],
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'dropdownCssClass' => 'options-drop',
                                'allowClear' => true,
                            ]
                        ]) ?>

                        <?= $form->field($model, 'location_size')->widget(Select2::className(), [
                            'data' => Host::$locationSizeArray,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'dropdownCssClass' => 'options-drop',
                                'allowClear' => true,
                            ]
                        ]) ?>
                        <?= $form->field($model, 'location_seats')->widget(Select2::className(), [
                            'data' => [
                                1 => 'first',
                                2 => 'second',
                            ],
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'dropdownCssClass' => 'options-drop',
                                'allowClear' => true,
                            ]
                        ]) ?>
                        <div class="row w-100 no-padding no-margin">
                            <div class="col-4 no-padding-left">
                                <?= $form->field($model, 'location_pets_exist')->widget(Select2::className(), [
                                    'data' => [
                                        'no', 'si'
                                    ],
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'dropdownCssClass' => 'options-drop',
                                        'allowClear' => true,
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-8 no-padding-rigth">
                                <?= $form->field($model, 'location_pets')->widget(Select2::className(), [
                                    'data' => Host::$animalsArray,
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'dropdownCssClass' => 'options-drop',
                                        'allowClear' => true,
                                    ]
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row w-100 no-padding no-margin">
                    <div class="col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-ld-8 offset-lg-2 col-xl-8 offset-xl-2  no-padding">
                        <div class="form-group">
                            <div class="row w-100 no-gutters no-padding justify-content-between">
                                <div class="col-6 col-sm-4 col-md-4">
                                    <?php if ($model->step > 1): ?>
                                        <?= \yii\bootstrap\Html::a('Indietro', ['host/create', 'step' => $model->step - 1], ['class' => 'btn btn-default indiero-btn']) ?>
                                    <?php endif; ?>
                                </div>
                                <div class="col-6 col-sm-4 col-md-4">
                                    <?= \yii\bootstrap\Html::submitButton('Avanti', ['class' => 'btn btn-primary avanti-btn']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="event-block-right img-block-upload">
                <?= $this->render('_image-upload', [
                    'model' => $model,
                    'form' => $form
                ]); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>
<div class="clearfix"></div>