<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */

$this->title = 'Host';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (isset($show_success_screen)) : ?>
    <section class="hai-una3">
        <div class="container-fluid">
            <div class="row no-padding">
                <div class="col-12 rigt-block-text text-left no-padding">
                    <div class="row w-100 no-padding no-margin">
                        <div class="col-12 ofset-0 col-sm-6 col-md-6 offset-md-0">
                            <div class="row w-100 no-padding no-margin">
                                <div class="col-12 offset-0 col-md-10 offset-md-1 mob-style-w">
                                    <img src="/images/susses-icon-event.png" class="animated icons success-icon">
                                    <h5 class="wow animate fadeInRight title-host-h5" data-wow-duration="1s"
                                        data-wow-delay="0.1s">
                                        Complimenti "<?= Yii::$app->user->identity->getUsernameWithSurname(); ?>"<br>
                                        Hai completato il tuo profilo Host,<br>
                                        ora puoi iniziare a creare<br>
                                        i tuoi eventi!
                                    </h5>
                                    <button type="button" class="btn-button-host" data-wow-duration="2s"
                                            data-wow-delay="0.05s"><?= Html::a('+ Crea evento', ['/profile/events/create'], ['class' => 'btn btn-primary btn-sofan wow animate fadeInRight btn-crea-event compila-bottom']) ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 main-triangle-heigth-vh align-self-center">
                            <img src="/images/goal.png" class="animated icons flag-img">
                            <div class="text-center"><span class="my-home-text"><?= $model->title; ?></span></div>
                            <div class="text-center"><img class="img-ic-room" src="/images/ic_room_24px.png"><span class="adress-text"><?= $model->city . ', ' . $model->street_name . ' ' . $model->street_number; ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
<?php else : ?>
    <section class="hai-una">
        <div class="container-fluid">
            <div class="row pad-top-6">
                <div class="col-12 col-sm-5 col-md-5 rigt-block-text right-block-host-text">
                    <h5 class="wow animate fadeInRight title-host-h5 titl-host-h" data-wow-duration="1s"
                        data-wow-delay="0.1s">
                        Ciao "<?= Yii::$app->user->identity->getUsernameWithSurname(); ?>"<br>
                        Hai una TV e<br>
                        una location?</h5>
                    <p class="wow animate fadeInRight description-host" data-wow-duration="1s" data-wow-delay="0.1s">
                        Diventa Host e <br>
                        guadagna divertendoti</p>
                    <button type="button" class="btn-button-host" data-wow-duration="2s"
                            data-wow-delay="0.05s">
                        <?= Html::a('Compila profilo Host', Yii::$app->user->isGuest ?
                            null : Url::to(['/profile/host/create']),
                            ['class' => 'btn btn-primary btn-sofan wow animate btn-comp-host' . (Yii::$app->user->isGuest ? ' js-open-login-modal' : '') . ' fadeInRight']) ?>
                    </button>
                </div>
                <div class="col-12 col-sm-7 col-md-7 main-triangle align-self-center">
                    <img src="/images/behost_photo.png" class="animated icons img-host">
                </div>
            </div>
        </div>
    </section>
    <style type="text/css">
        .footer {
            display: none;
        }
    </style>
<?php endif; ?>