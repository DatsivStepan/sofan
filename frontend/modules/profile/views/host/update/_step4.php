<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model \frontend\models\HostForm */

?>
<style type="text/css">
    .footer{
        display: none;
    }
</style>
<section class="hai-una2">
    <div class="container-fluid">
        <div id="host_form" class="events-form">
            <?php $form = ActiveForm::begin(); ?>
            <div class="block-row">
                <div class="event-block-left adress-host-block">
                    <div class="row w-100 no-padding border-bot step-4-host-padding">
                        <div class="max-whith-block-even max-whith-block col-12 offset-0 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2 no-padding">
                            <h2 class="h2-style-adress">4. Comfort e Preferenze</h2>
                            <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
                            <div class="row w-100 no-padding no-margin">
                                <div class="col-12 offset-0 col-sm-10 offset-sm-1">
                                    <div class="form-group"></div>
                                    <?= $form->field($model, 'wifi', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'wifi'
                                        ]
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>

                                    <?= $form->field($model, 'conditioner', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'air-conditioner'
                                        ]
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>

                                    <?= $form->field($model, 'food', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'dinner'
                                        ]
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>

                                    <?= $form->field($model, 'balcony', [
                                        'template' => '{input} {label}',
                                        'labelOptions' => ['class' => 'control-label'],
                                        'options' => [
                                            'class' => 'cbx-style js-change-status',
                                            'data-img' => 'balcony'
                                        ],
                                    ])
                                        ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>
                                </div>
                            </div>
                            <div class="row w-100 no-padding no-margin">
                                <div class="col-12 no-padding margit-bottom-title">
                                    <?= $form->field($model, 'title')->textInput(['placeholder' => 'ES. casa mia']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row w-100 no-padding no-margin">
                        <div class="max-whith-block-even col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2  no-padding">
                            <div class="form-group">
                                <div class="row w-100 no-gutters no-padding justify-content-between">
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <a href="<?= yii\helpers\Url::to(['/profile/host/update/' . $model->id, 'step' => 3]); ?>"
                                           class="btn btn-default avanti-btn-a">Indietro</a>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <?= \yii\bootstrap\Html::submitButton('Salva', ['class' => 'btn btn-primary avanti-btn']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event-block-right" style="background-color: #E1E8EE;">
                    <div class="row w-100 height_block-w no-padding no-margin align-items-center">
                        <div class="col text-center js-wifi">
                            <img class="img-vacation"
                                 src="<?= $model->wifi ? '/images/wifi_active.png' : '/images/wifi.png' ?>">
                        </div>
                        <div class="col text-center js-air-conditioner">
                            <img class="img-vacation"
                                 src="<?= $model->conditioner ? '/images/air-conditioner_active.png' : '/images/air-conditioner.png' ?>">
                        </div>
                        <div class="col text-center js-dinner">
                            <img class="img-vacation"
                                 src="<?= $model->food ? '/images/dinner_active.png' : '/images/dinner.png' ?>">
                        </div>
                        <div class="col text-center js-balcony">
                            <img class="img-vacation"
                                 src="<?= $model->balcony ? '/images/balcony_active.png' : '/images/balcony.png' ?>">
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
</section>

<script>
    $(function () {
        $(document).on('change', '.js-change-status', function () {
            var val = $(this).find('input').val();
            var src = $(this).data('img')
            if (val == 1) {
                src = src + '_active';
            }
            console.log(src)
            $('.js-' + $(this).data('img')).find('img').attr('src', '/images/' + src + '.png')
        })
    })
</script>