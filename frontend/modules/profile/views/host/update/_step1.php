<?php

use kartik\widgets\Select2,
    yii\helpers\Html,
    common\models\Country,
    kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \frontend\models\HostForm */

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDoSL8IN0GJJOwChniCpdNoPvWQa0c7ZnI&libraries=places&callback=initAutocomplete', [
    'async' => true,
    'defer' => true,
]);

$model->country = $model->country == null ? Country::ITALY_CODE : $model->country;
?>
<style type="text/css">
    .footer{
        display: none;
    }
</style>
<section class="hai-una2">
    <div class="container-fluid">
        <div class="block-row ">
            <div class="event-block-left adress-host-block">
                <div class="row w-100 no-padding border-bot">
                    <div class="col-12 offset-0 col-sm-12 offset-sm-0 col-md-10 offset-md-1 col-ld-10 offset-lg-1 col-xl-8 offset-xl-2 no-padding">
                        <h2 class="h2-style-adress">1. Indrizzo</h2>

                        <?php $form = ActiveForm::begin([
                            'id' => 'host_form',
                        ]); ?>

                        <?= $form->field($model, 'lat')->hiddenInput()->label(false)->error(false) ?>
                        <?= $form->field($model, 'lng')->hiddenInput()->label(false)->error(false) ?>
                        <div class="select-country">
                            <?= $form->field($model, 'country', ['options' => ['class' => 'sdfghjk']])->widget(Select2::className(), [
                                'data' => Country::getList(),
                                'pluginOptions' => [
                                    'dropdownCssClass' => 'options-drop',
                                    'allowClear' => true
                                ]
                            ]) ?>
                        </div>
                        <div class="form-group ">
                            <label class="control-label" for="">Indrizzo</label>
                            <input type="text" id="address" class="form-control" placeholder="Es. Via cadorna 1, Milano"
                                   value="<?= $model->street_name . 
                                ($model->street_number ? ', ' . $model->street_number : '') . 
                                ($model->city ? ', ' . $model->city : '') .
                                ($model->province ? ', ' . $model->province : ''); ?>">
                        </div>

                        <div class="row w-100 no-padding no-margin">
                            <div class="col-6 no-padding-left">
                                <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readOnly' => true, 'placeholder' => 'Es. Milano']) ?>
                            </div>
                            <div class="col-3 no-padding-left">
                                <?= $form->field($model, 'zip_code')->textInput(['maxlength' => true, 'readOnly' => true, 'placeholder' => 'Es. 26010']) ?>
                            </div>
                            <div class="col-3 no-padding-rigth">
                                <?= $form->field($model, 'province')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
                            </div>
                            <div class="col-8 no-padding-left">
                                <?= $form->field($model, 'street_name')->textInput(['maxlength' => true, 'readOnly' => true, 'placeholder' => 'Es. via Mazzini']) ?>
                            </div>
                            <div class="col-4 no-padding-rigth">
                                <?= $form->field($model, 'street_number')->textInput(['maxlength' => true, 'readOnly' => true, 'placeholder' => 'Es. 3']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row w-100 no-padding no-margin">
                    <div class="col-10 offset-1 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-ld-8 offset-lg-2 col-xl-8 offset-xl-2  no-padding">
                        <div class="form-group">
                            <div class="row w-100 no-gutters no-padding justify-content-between">
                                <div class="col-6 col-sm-4 col-md-4">
                                    <?= Html::submitButton('Avanti', ['class' => 'btn btn-primary avanti-btn']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="event-block-right img-block-upload-step-2">
                <div id="map" class="map-block-mob"></div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>

<script>

    var autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    var formIDs = {
        street_number: "hostform-street_number",
        route: "hostform-street_name",
        locality: "hostform-city",
        administrative_area_level_1: "hostform-province",
        postal_code: "hostform-zip_code"
    };
    var latInput = 'hostform-lat';
    var lngInput = 'hostform-lng';

    function initAutocomplete() {

        var mapOptions = {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13,
            minZoom: 8,
            maxZoom: 12,
        };

        var lat = document.getElementById(latInput).value;
        var lng = document.getElementById(lngInput).value;
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

        if (lat && lng) {
            mapOptions.center.lat = parseFloat(lat);
            mapOptions.center.lng = parseFloat(lng);
        }
          
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var image = {
            url: '/images/icons/map_icon_2.png',
            size: new google.maps.Size(50, 32)
        };
        
        var marker = new google.maps.Marker({
            map: map,
            icon: image,
        });
        var countryInput = $('#hostform-country');

        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('address')),
            {types: ['geocode']});

        autocomplete.setComponentRestrictions(
            {'country': [countryInput.val()]});

        autocomplete.addListener('place_changed', fillInAddress);

        countryInput.on('change', function () {
            autocomplete.setComponentRestrictions({'country': [$(this).val()]})
        });

        setGeopos(latlng);

        function fillInAddress() {
            var place = autocomplete.getPlace();

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    if (addressType == 'country') continue;
                    document.getElementById(formIDs[addressType]).value = val;
                }
            }
            document.getElementById(latInput).value = place.geometry.location.lat();
            document.getElementById(lngInput).value = place.geometry.location.lng();

            setGeopos({
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            });
        }

        function setGeopos(latlng) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    if (results[1]) {
                        map.setZoom(11);
                        map.setCenter(latlng);
                        marker.setPosition(latlng);
                    } else {
                        window.alert('No results found');
                    }
                }
            });
        }
    }

</script>