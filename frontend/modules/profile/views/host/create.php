<?php

/* @var $this yii\web\View */
/* @var $model \frontend\models\HostForm */

$this->title = 'Create Host';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_step' . $model->step , [
    'model' => $model
]); ?>
