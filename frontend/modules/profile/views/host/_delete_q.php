<?php

use yii\helpers\Html,
    yii\bootstrap\ActiveForm,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    common\models\Host;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

?>

<?php Pjax::begin(['id' => 'pjax-events-booking', 'enablePushState' => false]); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <script>
        if ($modal.deleteLocModal) {
            $modal.deleteLocModal.close();
        }

        const imageURL = '/images/group_192.png';
        swal({
            title: "",
            className: "sweet-alert-css",
            text: "Your Location And selected Events Had been Deleteted",
            button: false,
            icon: imageURL,
            timer: 2000,
        }).then((value) = > {
            location.reload()
        });
    </script>
<?php } ?>
<?php $form = ActiveForm::begin(['id' => 'events-booking-form-', 'options' => ['data-pjax' => 'pjax-events-booking']]); ?>
<?= $form->field($model, 'status')->hiddenInput(['value' => Host::STATUS_DELETE])->label(false); ?>
<?= Html::input('hidden', 'type', 'question') ?>
    <img class="img-location-delete" src="/images/warning-icon-host-delet.png">
    <h3 class="delete-location-h3">Are you sure to delete your location!<br> you will be able to delete also all events
        collegate to<br> this location!</h3>
    <div class="row no-margin">
        <div class="col-12 col-md-10 offset-md-1">
            <?= $form->field($model, 'why')->widget(Select2::className(), [
                'data' => Host::$reasonDeletion,
                'pluginOptions' => [
                    'dropdownCssClass' => 'options-drop option-font',
                    'allowClear' => true
                ]
            ])->label(false) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>