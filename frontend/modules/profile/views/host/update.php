<?php

/* @var $this yii\web\View */
/* @var $model \frontend\models\HostForm */

$this->title = 'Update Host';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('update/_step' . $step , [
    'model' => $model
]); ?>
