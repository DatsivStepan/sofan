
<?php

use yii\helpers\Html,
    yii\bootstrap\ActiveForm,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    common\models\Host;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

?>

<?php Pjax::begin(['id' => 'pjax-events-booking', 'enablePushState' => false]); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <script>
        if ($modal.deleteLocModal) {
            $modal.deleteLocModal.close();
        }

        const imageURL = '/images/group_192.png';
        swal({
            title: "",
            className: "sweet-alert-css",
            text: "Your Location And selected Events Had been Deleteted",
            button: false,
            icon: imageURL,
            timer: 2000,
        }).then((value) => {
            location.reload()
        });
    </script>
<?php } ?>
<?php $form = ActiveForm::begin(['id' => 'events-booking-form-', 'options' => ['data-pjax' => 'pjax-events-booking']]); ?>
    <?= Html::input('hidden', 'type', 'event')?>
    <?= $form->field($model, 'status')->hiddenInput(['value' => Host::STATUS_DELETE])->label(false); ?>
    <img class="img-location-delete" src="/images/warning-icon-host-delet.png">
    <h3 class="delete-location-h3">you have these events connected do you want to delete !</h3>
    <?php if ($model->eventsModel) { ?>
        <div class="row no-margin justify-content-center">
            <?php foreach($model->eventsModel as $event) { ?>
                <div class="col-4 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-center no-padding">
                    <a target="_blank" href="<?= $event->getLink(); ?>">
                        <img class="img-location-delet-step-2" src="<?= $event->getDefaultImage(); ?>">
                    </a>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>