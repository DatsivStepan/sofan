<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\EventsBookings;
use yii\helpers\Url;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedback');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .rating-container .caption{
        display:none !important;
    }
</style>
<div class="events-index container">
    <?= $this->render('/default/menu_top'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class=" col-12  col-md-3 col-lg-2 left-block feedback-left-menu"> 
            <?= $this->render('/default/menu_left'); ?>
        </div>
        <div class="col-12 col-md-9 col-lg-10 js-events-index right-block"> 
            <?php if ($dataProvider->getModels()) { ?>
                <?php foreach ($dataProvider->getModels() as $model) { ?>
                    <div class="row rating--maine-container align-items-center">
                        <div class="col-12 col-sm-2 col-lg-2 col-xl-2 text-center">
                          <img class="rating-user-img" src="<?= $model->userModel ? $model->userModel->getAvatar() : ''; ?>">
                            <p class="feedback-user-name"><?= $model->userModel ? $model->userModel->getUsernameWithSurname() : ''; ?></p>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
                            <p class="feedback-date-create text-center"><?= $model->getDateCreate(); ?></p>
                            <p class="feedback-text-content"><?= $model->note; ?></p>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-4 col-xl-4 rating-stars-container">
                            <?= StarRating::widget([
                                'name' => 'locationR',
                                'value' => $model->getStarRatingCount(),
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'disabled'=>true,
                                    'showClear'=>false
                                ]
                            ]); ?>
                            <p><?= number_format($model->getStarRatingCount(), 1, '.', ''); ?> / 5</p>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        </div>
    </div>
</div>