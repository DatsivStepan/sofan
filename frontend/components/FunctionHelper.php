<?php

namespace frontend\components;

use Yii,
    yii\helpers\Html,
    yii\helpers\ArrayHelper;

/**
 * Class FunctionHelper
 * @package app\components\helpers
 */

class FunctionHelper
{
    public static function getDays($from = 1, $to = 31){
        $array = [];
        for ($i = $from; $i <= $to; $i++) {
            $array[$i] = $i;
        }
        return $array;
    }

    public static function getYears($from = 1970, $to = 2017){
        $array = [];
        for ($i = $from; $i <= $to; $i++) {
            $array[$i] = $i;
        }
        return $array;
    }

    public static function getMonths(){
        return [
            1 => 'gennaio',
            2 => 'febbraio',
            3 => 'marzo',
            4 => 'aprile',
            5 => 'maggio',
            6 => 'giugno',
            7 => 'luglio',
            8 => 'agosto',
            9 => 'settembre',
            10 => 'ottobre',
            11 => 'novembre',
            12 => 'dicembre',
        ];
    }
}