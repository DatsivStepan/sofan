<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/slick/slick.css',
        'scss/theme.scss',
        'css/jquery-confirm.min.css',
        'css/sweetalert.css',
        'css/media/main.css',
        'css/glyphicon.css',
        'css/fonts.css',
        'css/animate.css',
        'css/profile-css.css',
        'css/site.css',
        'css/prettyPhoto.css',
    ];
    public $js = [
        'plugins/slick/slick.min.js',
        'js/jquery-confirm.min.js',
        'js/jquery-confirm.settings.js',
        "https://unpkg.com/sweetalert/dist/sweetalert.min.js",
        'js/jquery.form.js',
        'js/main.js?ver=0.0.1',
        'js/wow.js',
        'js/jquery.prettyPhoto.js',
         "https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
