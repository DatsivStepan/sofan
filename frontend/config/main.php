<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'profile' => [
            'class' => 'frontend\modules\profile\Module',
        ],
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'language'=>'en-EN',
        'i18n' => [
            'class'=> Zelenin\yii\modules\I18n\components\I18N::className(),
            'languages' => ['it-IT', 'en-EN'],
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::className()
                ],
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ]
        ],
        'Yii2Twilio' => [
            'class' => 'filipajdacic\yiitwilio\YiiTwilio',
            'account_sid' => 'ACc3c44dfef1e9b95116fffd28ea6a9063',
            'auth_key' => 'add0e2b6b85ae7ce4fd368e7b62dcaa4', 
        ],
        'eauth' => [
                'class' => 'nodge\eauth\EAuth',
                'popup' => true, // Use the popup window instead of redirecting.
                'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
                'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                'httpClient' => [
                        // uncomment this to use streams in safe_mode
                        //'useStreamsFallback' => true,
                ],
                'services' => [ // You can change the providers and their classes.
                        'facebook' => [
                            // register your app here: https://developers.facebook.com/apps/
                            'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                            'clientId' => '168692620515915',
                            'clientSecret' => 'a397914c6d79833de8b7b44592e768f3',
                        ],
                        'google' => [
                            // register your app here: https://code.google.com/apis/console/
                            'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                            'clientId' => '...',
                            'clientSecret' => '...',
                            'title' => 'Google',
                        ],
                ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
            'class' => 'frontend\components\LangRequest',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'bundles' => require(__DIR__ . '/assets.php'),
            'converter'=> [
                 'class'=>'nizsheanez\assetConverter\Converter',
                 'destinationDir' => 'build', //at which folder of @webroot put compiled files
                 'force'=> false,
                 'parsers' => [
                     'scss' => [ // file extension to parse
                         'class' => 'nizsheanez\assetConverter\Sass',
                         'output' => 'css', // parsed output file type
                         'options' => [] // optional options
                    ]
                ]
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class'=>'frontend\components\LangUrlManager',
            'rules' => [
                '' => 'site/index',
                'page/<id:\w+>' => 'pages/view',
                
                'public-profile/<id:\d+>' => 'site/public-profile',
                'profile/messages/<id:\d+>' => 'profile/messages/index',
                'profile/messages/check-dialog/<id:\d+>' => 'profile/messages/check-dialog',
                
                'users/<user_id:\d+>/posts' => 'user-posts/index',
                'users/<user_id:\d+>/posts/<id:\d+>' => 'user-posts/view',
                'users/<user_id:\d+>/posts/<id:\d+>/<_a:[\w-]+>' => 'user-posts/<_a>',
                'users/<user_id:\d+>/posts/<_a:[\w-]+>' => 'user-posts/<_a>',

                '<_c:[\w-]+>' => '<_c>/index',
                '<_c:[\w-]+>/<id:\d+>' => '<_c>/view',
                '<_c:[\w-]+>/<id:\d+>/<_a:[\w-]+>' => '<_c>/<_a>',
                '<_c:[\w-]+>/<_a:[\w-]+>/<id:\d+>/' => '<_c>/<_a>',
                '<_m:[\w-]+>/<_c:[\w-]+>/<_a:[\w-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
                'login/<service:google|facebook|etc>' => 'site/login',
            ],
        ],
    ],
    'params' => $params,
];
