<?php

return [
	'app' => [
		'basePath' => '@webroot',
		'baseUrl' => '@web',
        'css' => [
            'scss/site.scss'
        ],
        'js' => [

        ],
        'depends' => [
            'yii',
        ],
	],
];
?>
