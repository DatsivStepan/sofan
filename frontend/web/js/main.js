$(function(){
//event slider code degin
  if ($('.event-slider')) {
    $('.event-slider').slick({
      dots: true,
      centerMode: false,
      focusOnSelect: true,
      arrows: false,
      autoplay: false,
      autoplaySpeed: 4000,
      fade: true,
      customPaging: function (slider, i) {
          return '<div class="thumbnail">' + $(slider.$slides[i]).find('img').prop('outerHTML') + '</div>';
      }
    });
  }
  
  $('.event-slider .slide').css({
      height: ($('.event-slider').width() / 1.4)
  });
  $( window ).resize(function() {
    var slideHeight = ($('.event-slider').width() / 1.4);
    $('.event-slider .slide').css({
      height: slideHeight
    });
  });  
  $("a[rel^='prettyPhoto']").prettyPhoto(); 
//event slider code end
  
new WOW().init();

//    $(document).on('click', '.js-open-signup-modal', function(e) {
//        e.preventDefault();
//        $.dialog({
//            id: 'signupModal',
//            title: '',
//            type:false,
//            content: "url:/site/signup",
//            containerFluid: true,
//            columnClass: 'col-md-5 col-md-offset-3  signup-modal-class',
//        });
//    })
//
//    $(document).on('click', '.js-open-login-modal', function(e) {
//        e.preventDefault();
//        $.dialog({
//            id: 'loginModal',
//            title: '',
//            type:false,
//            content: "url:/site/login",
//            containerFluid: true,
//            columnClass: 'col-md-5 col-md-offset-3 login-modal-class',
//        });
//    })
//
//    $(document).on('click', '.js-go-to-signup', function(e){
//        e.preventDefault();
//        if ($modal.loginModal) {
//            modalReload($modal.loginModal, '/site/signup');
//        }
//        if ($modal.signupModal) {
//            modalReload($modal.signupModal, '/site/signup');
//        }
//    })

    $(document).on('click', '.js-go-to-login', function(e){
        e.preventDefault();
        if ($modal.loginModal) {
            modalReload($modal.loginModal, '/site/login');
        }
        if ($modal.signupModal) {
            modalReload($modal.signupModal, '/site/login');
        }
    })

    $("#host_form").on("change", "#host-img", function(){
        var $form = $(this).closest("form");
        var $el = $(this);
        var imageBox = $form.find(".image-box");
        var imageInput = $form.find("#hostform-image");
        var url = $form.find(".field-hostform-image").data('url');
        $form.ajaxSubmit({
            url: url,
            success: function (data) {
                var rez = $.parseJSON(data);
                if (rez.success) {

                    imageBox.html("");
                    imageInput.val("");
                    rez.result.forEach(function(item){

                        imageBox.append("<div class='image'><img src='"+item.src+"'/></div>");

                        var names = imageInput.val();
                        console.log(names);
                        if (names.length) {
                            names = names.split(",");
                            names.push(item.img);
                            names = names.join(",");
                            imageInput.val(names);
                        } else {
                            imageInput.val(item.img);
                        }
                    });
                } else {
                    alert(rez.error);
                }
            },
            error: function () {
                alert("Connection error");
            }
        });
        setTimeout(function(){
            $el.parent().html($el.parent().html());
        },1000);
        return false;
    });


})