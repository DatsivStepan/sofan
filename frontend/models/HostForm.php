<?php
/**
 * Created by PhpStorm.
 * User: dimatall
 * Date: 4/3/2018
 * Time: 1:29 AM
 */

namespace frontend\models;


use common\models\Host;
use common\models\Image;
use yii\base\Model;

class HostForm extends  Model
{
    const SCENARIO_ADDRESS = 'address';
    const SCENARIO_LOCATION = 'location';
    const SCENARIO_DEVICE = 'device';
    const SCENARIO_COMFORT = 'comfort';

    public $title;

    public $id;
//    public $user_id;
//    public $place_id;
//    public $created_at;
//    public $updated_at;
//    public $status;
//    public $why;
    // Address
    public $country;
    public $city;
    public $lat;
    public $lng;

    public $province;
    public $zip_code;
    public $street_name;
    public $street_number;

    // Location
    public $location_type;
    public $location_seats;
    public $location_size;
    public $location_pets;
    public $location_pets_exist;
    public $image;

    // Device
    public $device_type;
    public $device_size;
    public $device_resolution;

    // Comfort
    public $wifi;
    public $food;
    public $balcony;
    public $conditioner;

    public $step;


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title' => \Yii::t('app', 'Nome Location'),
            'city' => \Yii::t('app', 'Città'),
            'step' => \Yii::t('app', 'Tipo'),
            'country' => \Yii::t('app', 'Nazione'),
            'province' => \Yii::t('app', 'Provincia'),
            'zip_code' => \Yii::t('app', 'Cap'),
            'lat' => \Yii::t('app', 'Latitude'),
            'lng' => \Yii::t('app', 'Longitude'),
            'street_name' => \Yii::t('app', 'Via'),
            'street_number' => \Yii::t('app', 'Numero'),
            'location_seats' => \Yii::t('app', 'Seats'),
            'location_size' => \Yii::t('app', 'Grandezza'),
            'location_pets' => \Yii::t('app', 'Pets'),
            'device_type' => \Yii::t('app', 'Tipo'),
            'location_type' => \Yii::t('app', 'Tipo'),
            'device_size' => \Yii::t('app', 'Grandezza'),
            'device_resolution' => \Yii::t('app', 'Risoluzione'),
            'wifi' => \Yii::t('app', 'WI-FI'),
            'balcony' => \Yii::t('app', 'Si può fumare fuori'),
            'food' => \Yii::t('app', 'Si può ordinare cibo a casa'),
            'conditioner' => \Yii::t('app', 'Aria condizionata')
        ];
    }

    public function scenarios()
    {
        return array_merge([
            self::SCENARIO_ADDRESS => ['country', 'lat', 'lng', 'city', 'street_name', 'street_number', 'zip_code', 'province'],
            self::SCENARIO_LOCATION => ['location_type', 'location_seats', 'location_pets_exist', 'location_pets', 'location_size', 'image'],
            self::SCENARIO_DEVICE => ['device_type', 'device_resolution', 'device_size', 'image'],
            self::SCENARIO_COMFORT => ['wifi', 'conditioner', 'balcony', 'food', 'title'],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'country'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['step', 'id', 'location_pets_exist'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['image'], 'safe'],
            [['title', 'city', 'province'], 'string', 'max' => 255],
            [['street_name', 'street_number', 'zip_code'], 'string'],
            [['country'], 'string', 'max' => 2],
            [['location_type', 'location_seats', 'location_size', 'location_pets', 'device_type', 'device_size', 'device_resolution', 'wifi', 'balcony', 'food', 'conditioner'], 'integer'],

            [['country', 'city', 'lat', 'lng', 'street_name', 'street_number', 'zip_code'], 'required', 'on' => self::SCENARIO_ADDRESS],
            [['location_type', 'location_seats', 'location_pets', 'location_size'], 'required', 'on' => self::SCENARIO_LOCATION],
            [['device_type', 'device_resolution', 'device_size'], 'required', 'on' => self::SCENARIO_DEVICE],
            [['title'], 'required', 'on' => self::SCENARIO_COMFORT],
        ];
    }

    public function saveImages($images)
    {
        Image::deleteAll(['relation_id' => $this->id]);
        if ($images) {
            foreach ($images as $image) {
                $image = json_decode($image);
                $name = $image->name;
                $path = $image->path;
                if ($name && $path) {
                    $imageModel = new Image([
                        'path' => $path,
                        'name' => $name,
                        'relation_id' => $this->id
                    ]);
                    if ($imageModel->validate()) {
                        $imageModel->save();
                    }
                }
            }
        }
    }

    public function saveStepData()
    {
        if ($model = Host::findOne($this->id)) {
            $model->setAttributes($this->getAttributes());
            return $model->save();
        } else {
            return false;
        }
    }

    public function save()
    {
        $session = \Yii::$app->getSession();
        $data = $session['host_form_data'];

        $images = $data['image'];
        unset($session['host_form_data']);
        unset($data['image']);

        $host = new Host($data);

        if ($host->save(false)) {

            if ($images) {
                foreach ($images as $image) {
                    $imageModel = new Image([
                        'path' => $image,
                        'name' => $this->title . ' images.png',
                        'relation_id' => $host->id
                    ]);
                    if ($imageModel->validate()) {
                        $imageModel->save();
                    }
                }
            }
            return true;
        }

        return false;
    }
}