<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $password;
    public $birthday;
    public $terms1;
    public $terms2;
    public $day;
    public $month;
    public $year;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'terms1', 'terms2', 'day', 'month', 'year'], 'required', 'message'=> ''],
            [['name', 'surname'], 'string', 'min' => 2, 'max' => 55],
            ['birthday', 'safe'],

            ['email', 'trim'],
            ['email', 'required', 'message' => ''],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required', 'message' => ''],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->status = User::STATUS_ACTIVE;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $res = null;
        if ($user->save()) {
            $res = $user;
            $modelUserInfo = new \common\models\UserInfo();
            $modelUserInfo->user_id = $user->id;
            $modelUserInfo->name = $this->name;
            $modelUserInfo->surname = $this->surname;
            $modelUserInfo->birthday = date('Y-m-d', strtotime($this->year . '-' . $this->month. '-' . $this->day));
            $modelUserInfo->save();
        }
        return $res;
    }
}
