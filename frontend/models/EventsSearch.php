<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Events;

/**
 * EventsSearch represents the model behind the search form about `common\models\Events`.
 */
class EventsSearch extends Events
{
    public $cityName;
    public $tv_show_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'cityName', 'tv_show_id', 'date_start'
                ],
                'required'
                ],
            [
                [
                    'id', 'user_id', 'tv_show_id', 'host_id', 'max_places_count',
                    'min_places_count', 'registration_end', 'payment_per_person',
                    'step', 'status'
                ],
                'integer'
            ],
            [
                [
                    'cityName', 'tv_show_name'
                ],
                'string'
            ],
            [
                [
                    'live', 'name', 'date_start', 'time_start', 'description', 'wifi',
                    'smoke', 'food', 'conditioner', 'created_at', 'updated_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
                ->alias('event')
                ->joinWith(['tvShowModel tvS', 'hostModel host'])
                ->where(['event.status' => self::STATUS_ACTIVE])
                ->distinct();
 
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'event.date_start' => $this->date_start,
            'event.tv_show_id' => $this->tv_show_id,
//            'max_places_count' => $this->max_places_count,
//            'min_places_count' => $this->min_places_count,
//            'registration_end' => $this->registration_end,
//            'payment_per_person' => $this->payment_per_person,
//            'step' => $this->step,
//            'status' => $this->status,
        ]);
//        var_dump($this->cityName);exit;
//        $query->andFilterWhere(['like', 'host.city', $this->cityName]);
//            ->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'wifi', $this->wifi])
//            ->andFilterWhere(['like', 'smoke', $this->smoke])
//            ->andFilterWhere(['like', 'food', $this->food])
//            ->andFilterWhere(['like', 'conditioner', $this->conditioner]);

        return $dataProvider;
    }
}
