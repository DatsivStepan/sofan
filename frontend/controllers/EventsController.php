<?php

namespace frontend\controllers;

use Yii;
use common\models\Events;
use frontend\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\EventsBookings;
use common\models\EventsBookingItem;
use common\models\Feedback;
use common\models\Notification;
use common\models\EventsQuestions;
use yii\web\Response;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionMarker()
    {

//        header('Content-type: image/jpeg');
//        $img = @imagecreatefrompng(\Yii::$app->urlManager->createAbsoluteUrl('../images/icons/map_icon_1.png'));
//        // определяем цвет, в RGB
//        $color = imagecolorallocate($img, 255, 0, 0);
//
//        // указываем путь к шрифту
//        $font = \Yii::$app->urlManager->createAbsoluteUrl('fonts/OpenSans.ttf');
//        $text = urldecode($_GET['name']);
//        imagettftext($img, 24, 0, 365, 159, $color, $font, $text);
//        // 24 - размер шрифта
//        // 0 - угол поворота
//        // 365 - смещение по горизонтали
//        // 159 - смещение по вертикали
//
////        var_dump($img);exit;
//        
//        imagejpeg($img, NULL, 100);
        
        header("Content-type: image/png");
        $string = $_GET['text'];
        $im     = imagecreatefrompng(\Yii::$app->urlManager->createAbsoluteUrl('../images/icons/map_icon_4.png'));
        $color = imagecolorallocate($im, 255, 255, 255);
        imagestring($im, 5, 19, 4, $string, $color);
        imagepng($im);
        imagedestroy($im);
        
//        header("Content-type: image/png");
//        $string = '3 €';
//        $im     = imagecreatefrompng(\Yii::$app->urlManager->createAbsoluteUrl('../images/icons/map_icon_1.png'));
//        $orange = imagecolorallocate($im, 255, 255, 255);
//        $px     = (imagesx($im) - 7.5 * strlen($string)) / 2;
//        imagestring($im, 3, $px, 9, $string, $orange);
//        imagepng($im);
//        imagepng($im);
//        ob_start();
//        $imagedata = ob_get_contents();
//        ob_end_clean();
//        echo '<img src="data:image/png;base64,'.base64_encode($imagedata).'"/>';
//        imagedestroy($im);
        
//        header("Content-type: image/png");
//        $img = @imagecreatefrompng(\Yii::$app->urlManager->createAbsoluteUrl('../images/icons/map_icon_2.png'));
////        $img = imagecreate(320, 240);
//        //$background_color = imagecolorallocate($img, 155, 255, 255);
//        $text_color = imagecolorallocate($img, 233, 14, 91);
//        imagestring($img, 2, 5, 5,  "This is example", $text_color);
////        imagepng($img);
//        ob_start();
//        imagepng($img);
//        $imagedata = ob_get_contents();
//        ob_end_clean();
//        echo '<img src="data:image/png;base64,'.base64_encode($imagedata).'"/>';
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $searchModel->load(Yii::$app->request->queryParams);
//        var_dump(Yii::$app->request->get());exit;
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionEventDelete($id)
    {
        $this->layout = '/modal';
        $model = $this->findModel($id);
        $model->scenario = 'delete';
        if ($model->load(Yii::$app->request->post())) {
            $model->status = Events::STATUS_DELETED;
            if ($model->save()) {
                if ($model->eventsBookingModel) {
                    $userIds = [];
                    foreach ($model->eventsBookingModel as $eventsBooking) {
                        if ($eventsBooking->userModel) {
                            $userIds[$eventsBooking->userModel->id] = $eventsBooking->userModel->email;
                        }
                    }
                    Notification::saveNotification(
                        Notification::TYPE_EVENT_DELETE,
                        $userIds,
                        $model->id,
                        $model->id
                    );
                }
                
                Yii::$app->session->setFlash('success');
            } else {
                Yii::$app->session->setFlash('error');
            }
        }

        return $this->render('_booking-delete', [
            'model' => $model,
            'reasons' => ['first', 'second', 'other'],
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionBookingBookDelete($id)
    {
        $this->layout = '/modal';
        $model = $this->findModel($id);

        $modelBooking = $this->findBookingModel(Yii::$app->request->get('book_id'));

        if ($modelBooking->load(Yii::$app->request->post())) {
            $modelBooking->status = EventsBookings::STATUS_NOT_ACTIVE;
            if ($modelBooking->saveWithLog(EventsBookings::STATUS_NOT_ACTIVE)) {
                Yii::$app->session->setFlash('success');
            }
        }

        return $this->render('_booking-book-delete', [
            'model' => $model,
            'modelBooking' => $modelBooking,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionBooking($id)
    {
        $this->layout = '/modal';

        $model = $this->findModel($id);
        $modelNew = new EventsBookings();
        $modelNew->event_id = $id;
        $modelNew->user_id = Yii::$app->user->id;
        $modelNewBI = new EventsBookingItem();

        if ($modelNewBI->load(Yii::$app->request->post())) {
            if ($modelNewBI['name'] && $modelNew->saveWithItem($modelNewBI, $model->confirm_status)) {
                Yii::$app->session->setFlash('success');
            } else {
                Yii::$app->session->setFlash('error');
            }
        }

        return $this->render('_booking', [
            'model' => $model,
            'modelNewBI' => $modelNewBI,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionRefreshEvents()
    {
        $events = Events::find()
                ->where(['status' => Events::STATUS_ACTIVE])
                ->andWhere(['<', 'date_start', date('Y-m-d')])
                ->andWhere(['<', 'time_start', date('h:i', strtotime('+1 hour'))])
                ->all();
        var_dump($events);
        
        $ids = \yii\helpers\ArrayHelper::getColumn($events, 'id');
        Events::updateAll(['status' => Events::STATUS_NOT_ACTIVE], ['id' => $ids]);
    }

    public function actionUpdate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->scenario = 'update';
        $post = Yii::$app->request->post();
        $status = false;
        if ($model->isOwner()) {
            if ($post['comfort']) {
                foreach ($post['comfort'] as $comfort) {
                    switch ($comfort['name']) {
                        case 'wifi':
                            $model->wifi = $comfort['active'];
                            break;
                        case 'dinner':
                            $model->food = $comfort['active'];
                            break;
                        case 'air-conditioner':
                            $model->conditioner = $comfort['active'];
                            break;
                        case 'balcony':
                            $model->balcony = $comfort['active'];
                            break;
                    }
                }
            }
            $model->description = $post['description'];
            $status = $model->save();
        }
        return ['status' => $status];
    }
    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Yii::$app->params['with_out_footer'] = true;
        $model = $this->findModel($id);
        if (($model->status == Events::STATUS_DELETED) && !$model->isOwner()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $modelNewQ = new EventsQuestions();
        $modelNewQ->user_id = Yii::$app->user->id;
        $modelNewQ->event_id = $model->id;
        if ($modelNewQ->load(Yii::$app->request->post())) {
            $modelNewQ->saveWithNotification();
        }
        
        $modelNewF = new Feedback();
        $modelNewF->user_id = Yii::$app->user->id;
        $modelNewF->event_id = $model->id;
        if ($modelNewF->load(Yii::$app->request->post())) {
            $modelNewQ->saveWithNotification();
        }
        return $this->render('view', [
            'model' => $model,
            'modelNewF' => $modelNewF,
            'modelNewQ' => $modelNewQ,
        ]);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findBookingModel($id)
    {
        if (($model = EventsBookings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
