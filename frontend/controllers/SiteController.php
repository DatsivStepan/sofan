<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\web\Response;
use common\models\EmailSender;
use common\models\User;
use common\models\Host;
use yii\helpers\ArrayHelper;
use common\models\Events;
use common\models\Feedback;
use common\models\TvShow;
use Authy\AuthyApi;
use yii\widgets\ActiveForm;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'save-image', 'profile-show', 'get-tv-show'],
                'rules' => [
                    [
                        'actions' => ['signup', 'profile-show', 'get-tv-show'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'save-image', 'profile-show', 'get-tv-show'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return mixed
     */
    public function actionGetTvShow()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $status = false;
        $data = ['Sport' => null, 'Film' => null, 'Show' => null];
        if ($value = Yii::$app->request->get('value')) {
            $sport = TvShow::find()->where(['like', 'name', $value])->andWhere(['category_id' => 1])->limit(4)->all();
            $film = TvShow::find()->where(['like', 'name', $value])->andWhere(['category_id' => 2])->limit(4)->all();
            $show = TvShow::find()->where(['like', 'name', $value])->andWhere(['category_id' => 3])->limit(4)->all();
            $status = true;
            if ($sport) {
                $data['Sport'] = ArrayHelper::map($sport, 'id', 'name');
            }
            if ($film) {
                $data['Film'] = $film ? ArrayHelper::map($film, 'id', 'name') : null;
            }
            if ($show) {
                $data['Show'] = $show ? ArrayHelper::map($show, 'id', 'name') : null;
            }
        }

        return ['status' => $status, 'data' => $data];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionPublicProfile($id)
    {
        $user = User::findOne($id);
        $location = Host::find()->where(['user_id' => $id]);

        if (Yii::$app->request->get('location_id')) {
            $location->andWhere([
                'id' => Yii::$app->request->get('location_id')
            ]);
        }

        $feedback = $user->getUserFeedback();

        return $this->render('profile-show', [
            'user' => $user,
            'feedbacks' => $feedback,
            'location' => $location->orderBy(['created_at' => SORT_DESC])->one()
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLoginAjax() {
        $model = new LoginForm();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return ActiveForm::validate($model);
        }
    }

    public function actionSignupAjax() {
        $model = new SignupForm();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && ($user = $model->signup())) {
            if (Yii::$app->user->login($user)) {
                $modelU = User::findOne($user->id);
                EmailSender::sendEmail($modelU->email, 'Registration', 'signup', ['model' => $modelU]);
                return $this->goBack();
            }
        } else {
            return ActiveForm::validate($model);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        $this->layout = 'modal';

        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {

                    $identity = User::findByEAuth($eauth);
                    $profile = $eauth->getAttributes();
                    //var_dump($profile);exit;
                    if(!User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->exists()){
                        $model = new User();
                        $model->username = (isset($profile['name']))?$profile['name']:$profile['id'];
                        $model->password = \Yii::$app->security->generateRandomString(8);
                        $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
                        $model->auth_key = \Yii::$app->security->generateRandomString(8);
                        $model->username = $profile['name'];
                        $model->email = (isset($profile['email']))?$profile['email']:$profile['id'];
                        $model->signup_type = $eauth->getServiceName();
                        $model->social_id = (isset($profile['id']))?$profile['id']:'';
                        if ($model->save()) {
                          if (Yii::$app->getUser()->login($model)) {
                            $eauth->redirect();
                          }
                        }
                    }else{
                        $model = User::find()->where(['social_id' => (isset($profile['id']))?$profile['id']:$profile['id'],'username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->one();
                        if (Yii::$app->getUser()->login($model)) {
                          $eauth->redirect();
                        }
                    }
                    //Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
//                    $eauth->redirect();
                }else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
        
        if (!Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('successDataLogin');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->session->setFlash('successDataLogin');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
        
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = 'modal';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->user->login($user)) {
                    $modelU = User::findOne($user->id);
                    EmailSender::sendEmail($modelU->email, 'Registration', 'signup', ['model' => $modelU]);
                    Yii::$app->session->setFlash('successDataSignup');
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionSaveImage($path = 'other')
    {
        $uId = Yii::$app->user->id;
        $ds = DIRECTORY_SEPARATOR;
        $uFolder = 'images/' . $path . '/' . $uId;
        $storeFolder = \Yii::getAlias('@webroot') . '/' . $uFolder;

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder)){
                mkdir($storeFolder, 0777, true);
            }

            $tempFile = $_FILES['file']['tmp_name'];

            $newFileName = time() . '_' . rand(10,99).rand(10,99) . '.png';
            $targetFile =  $storeFolder . $ds . $newFileName;

            move_uploaded_file($tempFile, $targetFile);

            return $uFolder . '/' . $newFileName; //5
        }

        return false;
    }
}
