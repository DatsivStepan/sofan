<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Host');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="hai-una">
    <div class="container-fluid">
        <div class="row pad-top-6">
            <div class="col-12 col-sm-5 col-md-5 rigt-block-text right-block-host-text">
                <h5 class="wow animate fadeInRight title-host-h5 titl-host-h" data-wow-duration="1s"
                    data-wow-delay="0.1s">
                    Ciao "<?php if (!Yii::$app->user->isGuest) { ?><?= Yii::$app->user->identity->username ?>"!<br>
                    <?php } else { ?>Guest"!<br>
                    <?php } ?>

                    Hai una TV e<br>
                    una location?</h5>
                <p class="wow animate fadeInRight description-host" data-wow-duration="1s" data-wow-delay="0.1s">
                    Diventa Host e <br>
                    guadagna divertendoti</p>
                <button type="button" class="btn-button-host" data-wow-duration="2s"
                        data-wow-delay="0.05s">
                    <?= Html::a('Compila profilo Host', Yii::$app->user->isGuest ?
                        null : ['profile/host/create'],
                        ['class' => 'btn btn-primary btn-sofan wow animate btn-comp-host' . (Yii::$app->user->isGuest ? ' js-open-login-modal' : '') . ' fadeInRight']) ?>
                </button>
            </div>
            <div class="col-12 col-sm-7 col-md-7 main-triangle align-self-center">
                <img src="/images/behost_photo.png" class="animated icons img-host">
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .footer {
        display: none;
    }
</style>