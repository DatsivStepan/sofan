<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$model->why_delete = key($reasons);
?>

<?php Pjax::begin(['id' => 'pjax-events-booking', 'enablePushState' => false]); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <script>
        if ($modal.bookingDeleteModal) {
            $modal.bookingDeleteModal.close();
        }

        const imageURL = '/images/group_192.png';
        swal({
            title: "",
            className: "sweet-alert-css-2",
            text: "La tua prenotazione è stata Annullata\n" +
            "come da tua richiesta",
            button: false,
            icon: imageURL,
            timer: 2000,
        }).then((value) => {
            location.reload()
        });
    </script>
<?php } ?>

<?php $form = ActiveForm::begin(['id' => 'events-booking-form-', 'options' => ['data-pjax' => 'pjax-events-booking']]); ?>
    <div class="js-events-booking-container col-sm-12 no-padding-lg">
        <div id="host_form" class="col-lg-12" style="margin-top:5px;">
            <div class="warning-img"><img src="/images/warning-icon.png"></div>
            <h2 class="sweet-he-text">vuoi davvero annullare la tua prenotazione si ricorda
                che alcune annulamenti non sono rimborsabili</h2>
            <div class="row no-margin">
                <div id="events-booking-form" class="col-md-10 offset-md-1">
                    <?= $form->field($model, 'why_delete')->widget(Select2::className(), [
                        'data' => $reasons,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Perché vuoi annulare la tua Prenotazione ?'),
                        ],
                        'pluginOptions' => [
                            'dropdownCssClass' => 'options-drop bok-delete-option',
                            'allowClear' => true
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>