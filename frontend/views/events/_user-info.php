<?php

use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<style>
    .rating-container .caption{
        display:none !important;
    }
</style>
<div class="event-user-block-bot text-center">
    <div class="row no-padding no-margin">
        <div class="col-12 col-md-12 offset-md-0 col-lg-12 offset-lg-0 col-xl-10 offset-xl-1">
            <div class="row no-padding no-margin borb-bot">
                <div class="col-md-4 no-padding">
                    <a href="<?= $model->userModel ? $model->userModel->getLink() : ''; ?>">
                        <img src="<?= $model->userModel ? $model->userModel->getAvatar() : ''; ?>">
                    </a>
                </div>
                <div class="col-md-8 pad-left-no-lg">
                    <p class="user-name-event">
                        <a href="<?= $model->userModel ? $model->userModel->getLink() : ''; ?>">
                            <?= $model->userModel ? $model->userModel->getUsernameWithSurname() : ''; ?>
                            <?= $model->userModel ? $model->userModel->getAge() : ''; ?>
                        </a>
                    </p>
                    <p class="stars-p">
                        <?php if ($user = $model->userModel) { ?>
                            <?php $rateStars = $user->getStarRatingCount(); ?>
                            <?= StarRating::widget([
                                'name' => 'userR',
                                'value' => $rateStars['sum'],
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'disabled'=>true,
                                    'showClear'=>false
                                ]
                            ]); ?>
                        <?php } ?>
                    </p>
                    <?php if (!Yii::$app->user->isGuest && ($user = $model->userModel) && ($user->id != Yii::$app->user->id)) { ?>
                        <a href="/profile/messages/check-dialog/<?= $user->id; ?>" class="btn btn-contatta"> Contatta</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="event-user-coment">
    <div class="row no-padding no-margin">
        <div class="col-12 col-md-12 offset-md-0 col-lg-12 offset-lg-0 col-xl-10 offset-xl-1 block-rigth-style">
            <?php if ($model->userModel && ($feedbacks = $model->userModel->getUserFeedback(4))) { ?>
                <?php foreach ($feedbacks as $feedback) { ?>
                    <?php  if (($user = $feedback->userModel) && $feedback->note) { ?>
                            <div class="row no-gutters back-comment-block">
                                <div class="col-3 col-md-2 text-center">
                                    <a href="<?= $user->getLink(); ?>">
                                        <img class="event-one-ico-lap" src="<?= $user->getAvatar(); ?>">
                                    </a>
                                </div>
                                <div class="col-9 col-md-10 align-items-center">
                                    <p class="text-icon-lap"><?= $feedback->note; ?></p>
                                </div>
                            </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>