<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\rating\StarRating;
use common\models\Events;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
    <style>
        .rate-block .rating-container .caption {
            display: none !important;
        }
    </style>

<?php Pjax::begin(['id' => 'pjax-events-question', 'enablePushState' => false]); ?>
    <div class="tab-2-block">
        <div class="block-height-tab">
            <?php foreach ($model->eventsQuestionsModel as $question) { ?>
                <div class="row no-margin" style="padding: 0 30px;">
                    <div class="col-12 no-padding">
                        <div class="row no-gutters back-tab-block align-items-center">
                            <div class="col-3 col-sm-1 col-md-1 col-lg-1 col-xl-1 text-center">
                                <img src="<?= $question->userModel ? $question->userModel->getAvatar() : ''; ?>">
                            </div>
                            <div class="col-9 col-sm-11 col-md-11 col-lg-11 col-xl-11 align-items-center">
                                <p class="quest-tab"><?= $question->question; ?>
                                    <span class="ques-data"><?= $question->getDateCreate(); ?></span></p>
                            </div>
                        </div>
                        <div class="col-sm-10 offset-1 no-padding">
                            <?php if ($question->answer) { ?>
                                <div class="row no-gutters tab-block-user align-items-center">
                                    <div class="col-3 col-sm-1 col-md-1 col-lg-1 col-xl-1 text-center">
                                        <img src="<?= $model->userModel ? $model->userModel->getAvatar() : ''; ?>">
                                    </div>
                                    <div class="col-9 col-sm-11 col-md-11 col-lg-11 col-xl-11 align-items-center">
                                        <p class="quest-tab"><?= $question->answer; ?>
                                            <span class="ques-data"><?= $question->getDateUpdate(); ?></span></p>
                                    </div>
                                </div>
                            <?php } else if ($model->isOwner()) { ?>
                                <div class="text-right">
                                    <a class="js-open-answer-form add-adwer-btn" onclick="Go(this); return false;">Add answer</a>
                                    <div class="row js-answer-block-form no-margin" style="display:none;">
                                        <div class="col-sm-12 no-padding">
                                            <textarea id="add-textarea-question" class="form-control"></textarea>
                                            <a class="btn-send-answer js-send-answer"></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php if (!Yii::$app->user->isGuest) { ?>
            <?php $form = ActiveForm::begin(['id' => 'events-question-form', 'options' => ['data-pjax' => 'pjax-events-question']]); ?>
            <div id="tab-2-textarea" class="row  no-margin">
                <div class="col-sm-12">
                    <?= $form->field($modelNewQ, 'question')->textarea(['placeholder' => 'Fai una domanda Pubblica'])->label(false); ?>
                    <?= Html::submitButton(Yii::t('app', ''), [
                        'class' => 'btn-send-botton2',
                    ]) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        <?php } ?>

    </div>
    <script>
        function Go(Obj) {
            Obj.innerHTML=(Obj.innerHTML=='Add answer')? 'Close answer': 'Add answer';
        }

        $(function () {
            $(document).on('click', '.js-open-answer-form', function () {
                $(this).parent().find('.js-answer-block-form').toggle();
            })

            $(document).on('click', '.js-send-answer', function () {

            })
        })
    </script>
<?php Pjax::end(); ?>