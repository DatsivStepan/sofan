<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\rating\StarRating;
use common\models\Events;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
    <style>
        .rate-block .rating-container .caption {
            display: none !important;
        }
    </style>
<?php Pjax::begin(['id' => 'pjax-events-feedback', 'enablePushState' => false]); ?>
    <div class="rate-block">
        <?php if ($model->checkPermitToFeedback()) { ?>
            <?php $form = ActiveForm::begin(['id' => 'events-feedback-form', 'options' => ['data-pjax' => 'pjax-events-feedback']]); ?>
            <div id="star-tab-3" class="row">
                <div class="col-sm-6">
                    <?= $form->field($modelNewF, 'location')->widget(StarRating::classname(), [
                        'pluginOptions' => ['size' => 'xs', 'step' => 1, 'showClear' => false]
                    ]); ?>

                    <?= $form->field($modelNewF, 'hoster')->widget(StarRating::classname(), [
                        'pluginOptions' => ['size' => 'xs', 'step' => 1, 'showClear' => false]
                    ]); ?>
                </div>
                <div class="col-sm-6">

                    <?= $form->field($modelNewF, 'organization')->widget(StarRating::classname(), [
                        'pluginOptions' => ['size' => 'xs', 'step' => 1, 'showClear' => false]
                    ]); ?>

                    <?= $form->field($modelNewF, 'hospitality')->widget(StarRating::classname(), [
                        'pluginOptions' => ['size' => 'xs', 'step' => 1, 'showClear' => false]
                    ]); ?>

                    <?= $form->field($modelNewF, 'cleaning')->widget(StarRating::classname(), [
                        'pluginOptions' => ['size' => 'xs', 'step' => 1, 'showClear' => false]
                    ]); ?>

                    <?= $form->field($modelNewF, 'fun')->widget(StarRating::classname(), [
                        'pluginOptions' => ['size' => 'xs', 'step' => 1, 'showClear' => false]
                    ]); ?>
                </div>
                <div class="w-100"></div>
                <div class="col-sm-9">
                    <?= $form->field($modelNewF, 'note')->textarea() ?>
                </div>

                <div class="col-sm-3 no-padding">
                    <?= Html::submitButton(Yii::t('app', 'Vota'), [
                        'class' => 'js-submit-feetback btn-vota-botton',
                        'disabled' => true
                    ]) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        <?php // } elseif (($model->status == Events::STATUS_NOT_ACTIVE) && $model->feedbackModel) { ?>
        <?php } else { ?>
            <?php $result = $model->getFeedbackResult(); ?>

            <div id="star-tab-3" class="row">
                <div class="col-sm-6 class-reting">
                    <div class="dis-web-box">
                        <label class="control-label">Location</label>
                        <?= StarRating::widget(['name' => 'location', 'value' => $result['location'], 'pluginOptions' => ['size' => 'xs', 'disabled' => true, 'showClear' => false]]); ?>
                    </div>
                    <div class="dis-web-box">
                        <label class="control-label">Hoster</label>
                        <?= StarRating::widget(['name' => 'hoster', 'value' => $result['hoster'], 'pluginOptions' => ['size' => 'xs', 'disabled' => true, 'showClear' => false]]); ?>
                    </div>
                    <?php $sum = ($result['location'] + $result['hoster'] + $result['organization'] +
                            $result['hospitality'] + $result['cleaning'] + $result['fun']) / 6; ?>
                    <h1 class="h-total">Total <p><?= number_format($sum, 1, '.', ''); ?>/5</p></h1>
                </div>
                <div class="col-sm-6 class-reting">
                    <div class="dis-web-box">
                        <label class="control-label">Organization</label>
                        <?= StarRating::widget(['name' => 'organization', 'value' => $result['organization'], 'pluginOptions' => ['size' => 'xs', 'disabled' => true, 'showClear' => false]]); ?>
                    </div>
                    <div class="dis-web-box">
                        <label class="control-label">Hospitality</label>
                        <?= StarRating::widget(['name' => 'hospitality', 'value' => $result['hospitality'], 'pluginOptions' => ['size' => 'xs', 'disabled' => true, 'showClear' => false]]); ?>
                    </div>
                    <div class="dis-web-box">
                        <label class="control-label">Cleaning</label>
                        <?= StarRating::widget(['name' => 'cleaning', 'value' => $result['cleaning'], 'pluginOptions' => ['size' => 'xs', 'disabled' => true, 'showClear' => false]]); ?>
                    </div>
                    <div class="dis-web-box">
                        <label class="control-label">Fun</label>
                        <?= StarRating::widget(['name' => 'fun', 'value' => $result['fun'], 'pluginOptions' => ['size' => 'xs', 'disabled' => true, 'showClear' => false]]); ?>
                    </div>
                </div>
                <div class="col-sm-3 no-padding">
                    <?= Html::submitButton(Yii::t('app', 'Vota'), [
                        'class' => 'js-submit-feetback btn-vota-botton',
                        'disabled' => true
                    ]) ?>
                </div>
            </div>
        <?php } ?>


    </div>
    <script>
        $(function () {
            $(document).on('change', '[id ^= feedback-]', function () {
                $('.js-submit-feetback').attr('disabled', false)
            })
        })
    </script>
<?php Pjax::end(); ?>