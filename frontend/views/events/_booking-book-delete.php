<?php

use yii\helpers\Html,
    yii\bootstrap\ActiveForm,
    yii\widgets\Pjax,
    common\models\EventsBookings,
    kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

?>

<?php Pjax::begin(['id' => 'pjax-events-booking', 'enablePushState' => false]); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <script>
        if ($modal.bookingDeleteBookModal) {
            $modal.bookingDeleteBookModal.close();
        }
        const imageURL = '/images/group_192.png';
        swal({
            title: "",
            className: "sweet-alert-css",
            text: "La tua prenotazione è stata Annullata come da tua richiesta",
            button: false,
            icon: imageURL,
            timer: 2000,
        }).then((value) => {
            location.reload()
        });
    </script>
    <?php exit; ?>
<?php } ?>

<?php $form = ActiveForm::begin(['id' => 'events-booking-form-', 'options' => ['data-pjax' => 'pjax-events-booking']]); ?>
    <div class="js-events-booking-container col-sm-12 no-padding-lg">
        <div class="col-lg-12" style="margin-top:5px;">
            <?php if ($modelBooking->bookingItemsModel) { ?>
                <table class="table">
                    <tr class="top-table">
                        <td class="top-left"><b>Name</b></td>
                        <td><b>Gender</b></td>
                        <td class="top-right"><b>Age</b></td>
                    </tr>
                        <?php foreach ($modelBooking->bookingItemsModel as $bookingItems) { ?>
                            <tr class="top-table-1">
                                <td><?= $bookingItems->name; ?></td>
                                <td><?= $bookingItems->gender ? 'M' : 'F'; ?></td>
                                <td><?= $bookingItems->age ? $bookingItems->age : 0; ?></td>
                            </tr>
                        <?php } ?>
                </table>
                <?= $form->field($modelBooking, 'why')->widget(Select2::className(), [
                    'data' => EventsBookings::$reasonDeletion,
                    'pluginOptions' => [
                        'dropdownCssClass' => 'options-drop option-font',
                        'allowClear' => true
                    ]
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
    <script>
        $(function () {
            var content = $('#events-booking-form').html();
            $('.js-events-booking-container').on('change', '[name=js-max-item-count]', function () {

                var count = parseInt($(this).val()) + 1;
                $('#events-booking-form').html('');
//                var ageDrop = $('.js-age-drop').html();
                for (var i = 1; i <= parseInt(count); i++) {
                    $('#events-booking-form').append(content)
                }
            })
        })
    </script>

<?php Pjax::end(); ?>