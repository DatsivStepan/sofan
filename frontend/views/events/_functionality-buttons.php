<?php

use yii\helpers\Html,
    common\models\EventsBookings,
    common\models\Events;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$availableCount = $model->getAvailablePlacesCount();
$myBookings = $model->getMyEventsBooking();
?>
<?php if (!Yii::$app->user->isGuest) { ?>
    <?php if (!$model->isOwner()) { ?>
        <?php if ($model->status != Events::STATUS_NOT_ACTIVE) { ?>
            <?php if (EventsBookings::getUserBookedCount($model->id)) { ?>
                <div class="row <?= $availableCount < 1 ? ' justify-content-center ' : ''; ?>">

                            <?php if ($availableCount >= 1) { ?>
                                <div class="col-sm-6 no-padding" id="anulla-pren-btn">
                                    <?= Html::button('+ Aggiugi', ['class' => ['btn js-open-booking-modal aggiugi-btn']]) ?>
                                </div>
                            <?php } ?>
                            <div class="col-sm-6 no-padding" id="anulla-pren-btn2">
                                <?php if (count($myBookings) > 1) { ?>
                                    <div role="group">
                                        <?= Html::button('Annulla prenotazione', [
                                            'class' => 'btn cencel-btn dropdown-toggle',
                                            'data-toggle' => "dropdown",
                                            'aria-haspopup' => "true",
                                            'aria-expanded' => "false"
                                        ]) ?>
                                        <ul class="dropdown-menu drop-menu-style">
                                            <?php foreach ($myBookings as $bookings) { ?>
                                            <?php if ($bookings->status == EventsBookings::STATUS_NOT_ACTIVE) { ?>
                                                <?php continue; ?>
                                            <?php } ?>
                                                <li class="js-open-delete-booking-modal ofice-chair-icon cursor-pointer "
                                                    data-id="<?= $bookings->id; ?>">
                                                     <?php if ($bookings->status == EventsBookings::STATUS_ON_CONFIRM) { ?>
                                                        <img src="/images/ic_error_outline_24px.png" class="icon-right">
                                                    <?php } elseif ($bookings->status == EventsBookings::STATUS_CONFIRM) { ?>
                                                        <img src="/images/susses-icon.png" class="icon-right">
                                                    <?php } elseif ($bookings->status == EventsBookings::STATUS_REJECTED) { ?>
                                                        <img src="/images/delete.png" class="icon-right">
                                                    <?php } ?>
                                                    <?= $bookings->getName() ?>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } else { ?>
                                    <?= Html::button('Annulla prenotazione', [
                                        'class' => 'btn js-open-delete-booking-modal cencel-btn cencel-anull-btn',
                                        'data-id' => $myBookings[0]->id
                                    ]) ?>
                                <?php } ?>
                            </div>
                </div>
            <?php } else { ?>
                <?php if ($availableCount) { ?>
                    <?= Html::button('Prenota', ['class' => ['btn js-open-booking-modal prenota-btn']]) ?>
                <?php } ?>
            <?php } ?>
        <?php } else { ?>
            <?= Html::button('Vota', ['class' => ['btn voto-btn js-feedback-show']]) ?>
        <?php } ?>
    <?php } else { ?>
        <?php if ($model->status == Events::STATUS_DELETED) { ?>
            Evento is Canceled
        <?php } else { ?>
            <button class="btn btn-cancella-event js-delete-event">Cancella Evento</button>
        <?php } ?>
    <?php } ?>
    <div class="ofice-chair-icon">
        <?php foreach (range(1, $model->max_places_count) as $k => $i) { ?>
            <?php if ($k < $bookedCount) { ?>
                <img src="/images/office-chair.png">
            <?php } else { ?>
                <img src="/images/office-chair2.png">
            <?php } ?>
        <?php } ?>
    </div>
    <p class="post-p-text"><?= $bookedCount . ' / ' . $model->max_places_count; ?>
        Posti Disponibili
    </p>
    <div>
        <?php if ($model->isOwner()) { ?>
            <?php if ($myBookings) { ?>
                <?php foreach ($myBookings as $bookings) { ?>
                    <?php if ($bookings->status == EventsBookings::STATUS_NOT_ACTIVE) { ?>
                        <?php continue; ?>
                    <?php } ?>
                    <div class="ofice-chair-icon">
                         <?php if ($bookings->status == EventsBookings::STATUS_ON_CONFIRM) { ?>
                            <img src="/images/ic_error_outline_24px.png" class="icon-right">
                        <?php } elseif ($bookings->status == EventsBookings::STATUS_CONFIRM) { ?>
                            <img src="/images/susses-icon.png" class="icon-right">
                        <?php } elseif ($bookings->status == EventsBookings::STATUS_REJECTED) { ?>
                            <img src="/images/delete.png" class="icon-right">
                        <?php } ?>
                        <?= $bookings->getName() ?>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } else { ?>
            <?php if ($allBookings = $model->eventsBookingModel) { ?>
                <?php foreach ($allBookings as $bookings) { ?>
                    <?php if ($bookings->status == EventsBookings::STATUS_NOT_ACTIVE) { ?>
                        <?php continue; ?>
                    <?php } ?>
                    <div class="ofice-chair-icon">
                         <?php if ($bookings->status == EventsBookings::STATUS_ON_CONFIRM) { ?>
                            <img src="/images/ic_error_outline_24px.png" class="icon-right">
                        <?php } elseif ($bookings->status == EventsBookings::STATUS_CONFIRM) { ?>
                            <img src="/images/susses-icon.png" class="icon-right">
                        <?php } elseif ($bookings->status == EventsBookings::STATUS_REJECTED) { ?>
                            <img src="/images/delete.png" class="icon-right">
                        <?php } ?>
                        <?= $bookings->getName() ?>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>