<?php

use yii\helpers\Html,
    yii\widgets\DetailView,
    common\models\Events,
    common\models\EventsBookings,
    common\models\Host,
    yii\helpers\Url,
    yii\widgets\Pjax,
    kartik\rating\StarRating,
    kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDoSL8IN0GJJOwChniCpdNoPvWQa0c7ZnI&libraries=places&callback=initAutocomplete', [
    'async' => true,
    'defer' => true,
]);

$this->title = 'Event: ' . $model->getName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$bookedCount = $model->getBookedCount();
?>
<style>
    .js-show-update{
        display:none;
    }
</style>
<input type="hidden" class="js-lat" value="<?= $model->hostModel ? $model->hostModel->lat : 0; ?>">
<input type="hidden" class="js-lng" value="<?= $model->hostModel ? $model->hostModel->lng : 0; ?>">
<input type="hidden" class="js-price" value="<?= $model->payment_per_person; ?>">
<div class="events-view">
    <div class="row no-padding no-margin">
        <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 no-padding bacg-on-ewent">
            <div class="max-whith-one-event row no-padding no-margin">
                <?php if (Yii::$app->request->referrer) { ?>
                    <div class="col-0 col-sm-0 col-md-1 no-padding" style="background: white;">
                        <a href="<?= Url::to(Yii::$app->request->referrer); ?>"><img class="arrow-left-style" src="/images/ar-lef2.png"></a>
                    </div>
                <?php } ?>
                <div class="col-12 col-sm-12 col-md-6" style="padding-top: 30px;">
                    <div class="row">
                        <div class="col-sm-12" style="padding-right:0;padding-left:20px;">
                            <?php if ($model->getEventsImages()) { ?>
                                <div class="event-slider">
                                    <?php foreach ($model->getEventsImages() as $image) { ?>
                                        <div class="slide slide-width">
                                            <a href="/<?= $image; ?>" rel="prettyPhoto[gallery1]">
                                                <img src="/<?= $image; ?>" style="width:100%;">
                                            </a>
                                            <?php if ($model->status == Events::STATUS_NOT_ACTIVE) { ?>
                                                <div class="registration-time-container">
                                                    <span class="registration-time">Finito</span>
                                                </div>
                                            <?php } else { ?>
                                                <div class="registration-time-container">
                                                    <span class="registration-title">Iscrezione Scade tra:</span>
                                                    <span class="registration-time"><?= $model->getTimeToStart(); ?></span>
                                                </div>
                                            <?php } ?>
                                            <div class="registration-status-container d-flex justify-content-end">
                                                <?php if ($bookedCount >= $model->min_places_count) { ?>
                                                    <div class="registration-icon-green"></div>
                                                    <span class="registration-status-title">Min. Raggiunto</span>
                                                <?php } else { ?>
                                                    <div class="registration-icon-red"></div>
                                                    <span class="registration-status-title">Min. non Raggiunto</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php }  ?>
                        </div>
                        <div class="col-sm-12 no-padding mt-3 mob-pad-no">
                            <div class="whigth-tab-left nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                 aria-orientation="vertical">
                                <a class="active" id="v-pills-home-tab" data-toggle="pill"
                                   href="#v-pills-home" role="tab" aria-controls="v-pills-home"
                                   aria-selected="true"><img src="/images/information.png"></a>
                                <a class="" id="v-pills-profile-tab" data-toggle="pill"
                                   href="#v-pills-profile" role="tab" aria-controls="v-pills-profile"
                                   aria-selected="false"><img src="/images/FAQ.png"></a>
                                <a class="" id="v-pills-messages-tab" data-toggle="pill"
                                   href="#v-pills-messages" role="tab" aria-controls="v-pills-messages"
                                   aria-selected="false"><img src="/images/favorite (1).png"></a>
                            </div>
                            <div class="whigth-tab-right tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                                     aria-labelledby="v-pills-home-tab">
                                    <?= $this->render('_tab-1', [
                                        'model' => $model,
                                    ]); ?>
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                                     aria-labelledby="v-pills-profile-tab">
                                    <?= $this->render('_tab-2', [
                                        'model' => $model,
                                        'modelNewQ' => $modelNewQ
                                    ]); ?>
                                </div>
                                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                                     aria-labelledby="v-pills-messages-tab">
                                    <div class="row no-margin">
                                        <div class="col-md-10 offset-md-1 no-padding">
                                            <?= $this->render('_tab-3', [
                                                'model' => $model,
                                                'modelNewF' => $modelNewF
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 lg-block-user">
                    <div class="event-user-block bacg-event-one-nam">
                        <div class="row no-padding">
                            <div class="col-12 offset-0 col-md-8 offset-md-2 text-center pad-no-md">
                                <p class="date-start-text">
                                    <?= $model->getDateStart(); ?>
                                    <?php if ($model->isOwner(Yii::$app->user->id) && ($model->status != Events::STATUS_DELETED)) { ?>
                                        <i style="cursor:pointer;" class="js-event-update">Update</i>
                                    <?php } ?>
                                </p>
                                <h4 class="name-event-user">
                                    <?= $model->getName(); ?>
                                </h4>
                                <p class="addres-style-text">
                                    <img class="img-ic-room"
                                         src="/images/ic_room_24px.png"><?= $model->getAddress(); ?>
                                </p>
                                <h2 class="payment-text-style">
                                    <?= $model->getPaymentPerPerson(); ?>
                                </h2>
                                <?= $this->render('_functionality-buttons', [
                                    'model' => $model,
                                    'bookedCount' => $bookedCount
                                ]); ?>
                            </div>
                        </div>
                        <div class="event-param-block text-center">
                            <div class="row no-padding no-margin marg-and-whit justify-content-center">
                                <?php if ($model->hostModel && $model->hostModel->getSize()) { ?>
                                    <div>
                                        <img class="event-one-ico-lap" src="/images/select.png">
                                        <span class="text-icon-lap">
                                                    <?= $model->hostModel->getSize(); ?>
                                                </span>
                                    </div>
                                <?php } ?>
                                <?php if ($model->hostModel && $model->hostModel->getDeviceSize()) { ?>
                                    <div>
                                        <img class="event-one-ico-lap" src="/images/flatscreen-tv.png"
                                             style="margin-left: 10px;">
                                        <span class="text-icon-lap"><?= $model->hostModel->getDeviceSize() ?></span>
                                    </div>
                                <?php } ?>
                                <div>
                                    <img class="event-one-ico-lap" src="/images/smoking.png" style="margin-left: 10px;">
                                    <span class="text-icon-lap"><?= $model->smoke == Events::SMOKE_YES ? 'Fumatori' : 'No Fumatori'; ?></span>
                                </div>
                            </div>
                        </div>

                        <?= $this->render('_user-info', [
                            'model' => $model,
                        ]); ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 no-padding">
            <div class="map-heigth">
                <div id="map" class="mob-height-map map-xl"></div>
            </div>
        </div>
    </div>
</div>

<script>
    var map;

    function initAutocomplete() {

        var mapOptions = {
            center: {lat: 41.9012969, lng: 12.4957764},
            zoom: 10,
            minZoom: 8,
            maxZoom: 12,
        };
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
    }

    $(function () {



        $(document).on('click', '.js-change-confort-icon', function () {
            var thisElement = $(this)
            var active = thisElement.data('active')
            console.log(active)

            var name = thisElement.data('name');
            if (active == 0) {
                name = name + '_active';
                thisElement.data('active', '1')
            } else {
                thisElement.data('active', '0')
            }
            thisElement.find('img').attr('src', '/images/' + name + '.png');
        })




        $(document).on('click', '.js-event-save', function () {
            $(this).addClass('js-event-update')
            $(this).removeClass('js-event-save')
            $(this).text('Update')

            var description = $('[name=update_description]').val()
            var comfort = [];
            $(".js-confort-icon").each(function() {
                var name = $(this).data('name')
                var active = $(this).data('active');
                comfort.push({name:name, active:active});
            });

            $.ajax({
                url: '<?= Url::to(['/events/update']) ?>/<?= $model->id; ?>',
                method: 'post',
                dataType: 'json',
                data: {description:description, comfort:comfort},
                success: function (response) {
                    if (response.status) {
                        $('.js-description-block').text(description)
                        $('.js-show-save').show();
                        $('.js-show-update').hide();
                        $('.js-confort-icon').removeClass('js-change-confort-icon');
                    }
                }
            })
        })

        $(document).on('click', '.js-event-update', function () {
            $(this).removeClass('js-event-update')
            $(this).addClass('js-event-save')
            $(this).text('Save')
            $('.js-show-save').hide();
            $('.js-show-update').show();
            $('.js-confort-icon').addClass('js-change-confort-icon');
        })

        $(document).on('click', '.js-feedback-show', function () {

        })

        $(document).on('click', '.js-delete-event', function () {
            $.confirm({
                id: 'bookingDeleteModal',
                title: '',
                type: 'blue',
                content: "url:<?= Url::to(['/events/event-delete/', 'id' => $model->id]); ?>",
                containerFluid: true,
                columnClass: 'col-11 col-sm-11 col-md-11 col-lg-9 col-xl-7 prenoto-modal-class modal-warning-event',
                buttons: {
                    close: {
                        text: 'Indiero',
                        btnClass: 'btn-info'
                    },
                    ok: {
                        text: 'Cancella Evento',
                        btnClass: 'btn-danger',
                    },
                }
            });
        })

        $(document).on('click', '.js-open-delete-booking-modal', function () {
            var id = $(this).data('id')
            console.log(id)
            $.confirm({
                id: 'bookingDeleteBookModal',
                title: '',
                type: 'blue',
                content: "url:<?= Url::to(['/events/booking-book-delete', 'id' => $model->id]); ?>?book_id=" + id,
                containerFluid: true,
                columnClass: 'col-11 col-sm-11 col-md-11 col-lg-9 col-xl-7 prenoto-modal-class modal-warning-event',
                buttons: {
                    close: {
                        text: 'Indiero',
                        btnClass: 'btn-info'
                    },
                    ok: {
                        text: 'Close booking',
                        btnClass: 'btn-danger',
                    },
                }
            });
        })

        $(document).on('click', '.js-open-booking-modal', function () {
            $.confirm({
                id: 'bookingModal',
                title: '',
                type: 'blue',
                content: "url:<?= Url::to(['/events/booking/', 'id' => $model->id]); ?>",
                containerFluid: true,
                columnClass: 'col-11 col-sm-11 col-md-11 col-lg-9 col-xl-7 prenoto-modal-class modal-warning-event',
                buttons: {
                    close: {
                        text: 'Anulla',
                        btnClass: 'btn-danger'
                    },
                    ok: {
                        text: 'Prenoto',
                        btnClass: 'btn-primary',
                    },
                }
            });
        })

        loadUserMarjker();
    })

    function loadUserMarjker(latlng) {
        var lat = $('.js-lat').val()
        var lng = $('.js-lng').val()
        var price = $('.js-price').val()
        if (lat && lng) {
            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
            var image = {
                url: '/images/icons/map_icon_2.png',
                size: new google.maps.Size(50, 32)
            };

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image,
            });
            var infowindow = new google.maps.InfoWindow({
                content: '<h5 style="margin-bottom:0px;">Price - €' + price + '</h5>'
            });
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });

        }
        map.setZoom(12);
        if (latlng) {
            map.setCenter(latlng);
        }
    }
</script>
