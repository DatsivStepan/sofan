<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\rating\StarRating;
use common\models\Events;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDoSL8IN0GJJOwChniCpdNoPvWQa0c7ZnI&libraries=places&callback=initAutocomplete', [
    'async' => true,
    'defer' => true,
]);

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .rating-container .caption {
        display: none !important;
    }
</style>
<div class="js-events-index container-fluid event-search-md">

    <div class="block-row block-r-search">
        <div class="search-block-left adress-host-block">
            <div class="row no-margin">
                <div class="col-sm-12">
                    <div class="search-event">
                        <?= $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                </div>
            </div>
            <div class="row no-row no-margin">
                <div class="col-1 left-menu" style="background-color: #fafcfd; margin-top: 15px;">
                    <img id="icon" class="img-click" src="/images/ic_menu_24px.png" onclick="imgsrc(this)"
                         style="width:20px;margin: 10px auto 2px auto; display: inherit;">
                    <p class="filtri-style">Filtri</p>
                    <div class="filter">
                        <div class="row">
                            <div class="col-4">
                                <p class="filter__title">Filtri</p>
                            </div>
                            <div class="col-5 no-padding">
                                <div class="row">
                                    <div class="filter__circle-green"></div>
                                    <div class="filter__circle-red"></div>
                                    <div class="filter__circle-yellow"></div>
                                </div>
                            </div>
                            <div class="col-3">
<!--                                <img class="filter__images" src="/images/x2-2.png" alt="">-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="block-select">
                                    <label class="filter__label">Posti disponibili</label>
                                    <select class="filter__select">
                                        <option value="volvo">5</option>
                                        <option value="saab">8</option>
                                    </select>
                                    <label class="filter__label">Grandezza schermo</label>
                                    <select class="filter__select">
                                        <option value="volvo">20"</option>
                                        <option value="saab">40"</option>
                                    </select>
                                    <label class="filter__label">Grandezza location</label>
                                    <select class="filter__select">
                                        <option value="volvo">Da 0 a 20 MQ</option>
                                        <option value="saab">Da 0 a 40 MQ</option>
                                    </select>
                                    <label class="filter__label">Posti a sedere</label>
                                    <select class="filter__select">
                                        <option value="volvo">Poltrone</option>
                                        <option value="saab">Poltrone</option>
                                    </select>
                                    <label class="filter__label">Amici Pelosi</label>
                                    <select class="filter__select">
                                        <option value="volvo">SI</option>
                                        <option value="saab">SS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <img class="filter__images-botom" src="/images/wifi.png" alt="">
                                <img class="filter__images-botom" src="/images/air-conditioner.png" alt="">
                                <img class="filter__images-botom" src="/images/dinner.png" alt="">
                                <img class="filter__images-botom" src="/images/balcony.png" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                            <div class="filtra-marg-bot">
                                <a class="filter__a-filtra">Filtra</a><img src="/images/group-402.png" alt=""
                                                                           class="a-filtra__img">
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-11 right-block-search">
                    <img src="<?= Url::to(['/events/marker', 'text' => '3']); ?>">
                    <?php if ($dataProvider->getModels()) { ?>
                        <div class="row event-for-two-blok">
                            <?php foreach ($dataProvider->getModels() as $model) { ?>
                                <div class="event-for-two-half col-6 col-lg-4" style="padding-top: 15px;">
                                    <div class="js-search-block-event"
                                         style="position:relative;background-image: url('<?= $model->getDefaultImage(); ?>');">
                                        <a href="<?= $model->getLink(); ?>">
                                            <input type="hidden" class="js-lat"
                                                   value="<?= $model->hostModel ? $model->hostModel->lat : 0; ?>">
                                            <input type="hidden" class="js-lng"
                                                   value="<?= $model->hostModel ? $model->hostModel->lng : 0; ?>">
                                            <input type="hidden" class="js-price"
                                                   value="<?= $model->payment_per_person; ?>">
                                            <div class="right-img-block">
                                                <div class="event-user-block block-user text-center">
                                                    <img src="<?= $model->userModel ? $model->userModel->getAvatar() : ''; ?>">
                                                    <span><?= $model->userModel ? $model->userModel->getUsernameWithSurname() : ''; ?></span>
                                                    <div id="star-ss">
                                                        <?php if ($user = $model->userModel) { ?>
                                                            <?php $rateStars = $user->getStarRatingCount(); ?>
                                                            <?= StarRating::widget([
                                                                'name' => 'userR',
                                                                'value' => $rateStars['sum'],
                                                                'pluginOptions' => [
                                                                    'size' => 'xs',
                                                                    'disabled' => true,
                                                                    'showClear' => false
                                                                ]
                                                            ]); ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="event-user-block-1">
                                                    <span> <?= $model->getName(); ?> </span>
                                                    <p class="data">
                                                        <?= $model->getDateStart(); ?>
                                                    </p>
                                                    <p>

                                                    </p>
                                                    <p class="place">
                                                        <img src="/images/ic_room_24px.png"
                                                             style="height: 8px; width: 5px;">
                                                        <?= $model->getAddress(); ?>
                                                    </p>
                                                    <p class="payment">
                                                        <?= $model->getPaymentPerPerson(); ?>
                                                    </p>
                                                    <?php if ($model->status == Events::STATUS_NOT_ACTIVE) { ?>
                                                        <p class="time">
                                                            Finito
                                                        </p>
                                                    <?php } else { ?>
                                                        <p class="button-text">
                                                            Iscrezione Scade Fra:
                                                        </p>
                                                        <p class="time">
                                                            <?= $model->getTimeToStart(); ?>
                                                        </p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="">
                            <h3 class="text-center">No result</h3>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="search-block-right" style="padding-right: 0px;">
            <div class="map-heigth">
                <div id="map" class="map-block-mob" style="width: 35%;"></div>
            </div>
        </div>
    </div>

</div>
<div class="clearfix"></div>
<script>
    $(document).ready(function () {
        $(".img-click").click(function () {
            $(".right-block-search").toggleClass("search-for-two-event");
            $(".left-menu").toggleClass("left-menu-filter");
            $(".no-row").toggleClass("row-d-none");
            $(".filter").toggleClass("d-block");
            $(".img-click").toggleClass("img-x");
            $(".filtri-style").toggleClass("d-none");
        });
    });

    function imgsrc(img) {
        if ($(img).attr("src") == "/images/x2-2.png")
            $(img).attr("src", "/images/ic_menu_24px.png");
        else
            $(img).attr("src", "/images/x2-2.png");
    }
</script>
<script>
    var map;

    function initAutocomplete() {

        var mapOptions = {
            center: {lat: 41.9012969, lng: 12.4957764},
            zoom: 10,
            minZoom: 8,
            maxZoom: 12,
        };
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
    }

    $(function () {
        loadUserMarjker();

    function loadUserMarjker(latlng) {
        $(".js-search-block-event").each(function (index) {
            var lat = $(this).find('.js-lat').val()
            var lng = $(this).find('.js-lng').val()
            var price = $(this).find('.js-price').val()
            if (!price) {
                price = 0;
            }
            if (lat && lng) {
                var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
                var image = {
                    url: '/images/icons/map_icon.png',
                    size: new google.maps.Size(40, 30),
                    labelOrigin: new google.maps.Point(20, 12)
                };

                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    labelClass: "marker-labels",
                    label: {
                        text: '€' + price,
                        color: 'white',
                        fontSize: '16px',
                        fontWeight: "bold"
                    },
                    icon: image,
                    zIndex: 1
                });
                marker.addListener('click', function () {
                    console.log('df' + price)
                });
            }
            map.setZoom(12);
            if (latlng) {
                map.setCenter(latlng);
            }
        })
    }
    })

</script>
<style>
    @media (min-width: 320px) {
        .footer {
            display: none;
        }
    }
</style>