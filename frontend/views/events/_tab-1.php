<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\rating\StarRating;
use common\models\Events;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<p>Discrizione:</p>
<div class="js-show-save">
    <span class="descrip-on-view js-description-block"><?= $model->description; ?></span>
</div>
<div class="js-show-update">
    <?= Html::textarea('update_description', $model->description, [
        'class' => 'form-control'
    ]); ?>
</div>

<div class="row no-padding no-margin no-gutters">
    <div class="col-8 col-md-8">
        <p>Comodità In questa location:</p>
        <?php if ($model->wifi) { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="wifi" data-active="1">
                <img class="event-one-ico" src="/images/wifi_active.png">
                <span class="text-icon">Wifi</span>
            </span>
        <?php } else { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="wifi" data-active="0">
                <img class="event-one-ico" src="/images/wifi.png">
                <span class="text-icon">Wifi</span>
            </span>
        <?php } ?>
        

        <?php if ($model->food) { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="dinner" data-active="1">
                <img class="event-one-ico" src="/images/dinner_active.png">
                <span class="text-icon">Ordinare</span>
            </span>
        <?php } else { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="dinner" data-active="0">
                <img class="event-one-ico" src="/images/dinner.png">
                <span class="text-icon">Ordinare</span>
            </span>
        <?php } ?>
        

        <?php if ($model->conditioner) { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="air-conditioner" data-active="1">
                <img class="event-one-ico" src="/images/air-conditioner_active.png">
                <span class="text-icon">Aria cond.</span>
            </span>
        <?php } else { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="air-conditioner" data-active="0">
                <img class="event-one-ico" src="/images/air-conditioner.png">
                <span class="text-icon">Aria cond.</span>
            </span>
        <?php } ?>


        <?php if ($model->balcony) { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="balcony" data-active="1">
                <img class="event-one-ico" src="/images/balcony_active.png">
                <span class="text-icon">Balcone</span>
            </span>
        <?php } else { ?>
            <span class="clas-text-centr-icons js-confort-icon" data-name="balcony" data-active="0">
                <img class="event-one-ico" src="/images/balcony.png">
                <span class="text-icon">Balcone</span>
            </span>
        <?php } ?>
    </div>
    
    <div class="col-4 col-md-4 text-center ">
        <?php if ($model->hostModel && $model->hostModel->location_seats) { ?>
            <p class="sono-p">Sono presenti:</p>
            <div class="text-center">
                <img class="event-one-ico-lap" src="/images/animal-prints.png">
                <span class="text-icon-lap">
                    <?= $model->hostModel->getAnimals(); ?>
                </span>
            </div>
        <?php } ?>
    </div>
</div>