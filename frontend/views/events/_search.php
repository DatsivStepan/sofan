<?php
    use kartik\widgets\ActiveForm,
        kartik\widgets\Select2,
        kartik\date\DatePicker,
        yii\helpers\Html,
        frontend\models\EventsSearch,
        common\models\TvShowCategories,
        yii\helpers\Url;
 
//$tvArray = TvShowCategories::getAllWithCategory();
?>

 <?php $form = ActiveForm::begin(['id' => 'events-search-form', 'method' => 'get', 'action' => ['index'], 'options' => ['data-pjax' => 'pjax-events-search']]); ?>

    <div class="row block-search" style="padding-top: 25px;">
        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 search-block">
            <?= $form->field($model, 'tv_show_id')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'tv_show_name')->textInput(['placeholder' => 'Search...'])->label(false); ?>
            <div style="position:relative;" class="js-search-tv-block">
                <ul class="js-search-tv-ul show-dropdown">
                </ul>
            </div>
        </div>


        <div class="col-12 col-md-7 col-lg-7 col-xl-7 input-block">
            <div class="row input-block-sof">
                <div class="col-12 col-sm-6 col-lg-4 col-xl-4  dove-block">
                    <?= $form->field($model, 'cityName')->textInput(['class' => 'form-control input-sofan form-dove'])->label(false); ?>
                </div>

                <div class="col-12 col-sm-6 col-lg-4  col-xl-4 data-block block-date-search">
                <?= $form->field($model, 'date_start')
                    ->widget(DatePicker::className(), [
                        'removeButton' => false,
                        'value' => date('Y-m-d'),
                        'readonly' => true,
                        'options' => ['class' => 'input-sofan form-data'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'startDate'=> '0d',
                        ]
                    ])->label(false); ?>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 cedra-block md-carsa-btn">
                    <?= Html::submitButton('Cerca<img src="/images/up-button.png" style="width: 10px;height: 15px;box-sizing: initial;padding-left:12px;margin-top:-1px;">', ['class' => 'cersa btn-sofan']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script>
    $(function(){
        $(document).click(function(){
            $('.js-search-tv-ul').hide();
        });

        $(document).on('click', '.js-search-tv-ul .js-tv-show', function(event){
            $('.js-search-tv-ul').hide()
            $('.js-search-tv-ul').html('');
            var id = $(this).data('id');
            var text = $(this).text();
            $('#eventssearch-tv_show_id').val(id);
            $('#eventssearch-tv_show_name').val(text);
            event.stopPropagation();
        });

        $(document).on('click', '.js-search-tv-ul .js-tv-show-category, #eventssearch-tv_show_name', function(e){
            e.stopPropagation();
        });

        $(document).on('keyup', '#eventssearch-tv_show_name', function(){
            $('#eventssearch-tv_show_id').val('')
            var search_val = $(this).val();
            if (search_val.length > 2) {
                $.ajax({
                    url: '<?= Url::to(['/site/get-tv-show']) ?>/',
                    method: 'get',
                    dataType: 'json',
                    data: {value:search_val},
                    success: function (response) {
                        if (response.status && response.data) {
                            $('.js-search-tv-ul').show();
                            $('.js-search-tv-ul').html('')
                            if (response.data.length != 0) {
                                var group = response.data;
                                $.each(group, function( index, value ) {
                                    console.log('index ', index);
                                    if(value){
                                        $('.js-search-tv-ul').append('<li class="js-tv-show-category category-name ' + index.toLowerCase() + '">' + index + '</li>')
                                        console.log(value)
                                        $.each(value, function( i, v ) {
                                            $('.js-search-tv-ul').append('<li class="js-tv-show show-item" data-id="'+i+'">' + v + '</li>')
                                        })
                                    }
                                    //$('.js-search-tv-ul').append('<li class="js-tv-show" data-id="'+index+'">' + value + '</li>')
                                });
                            } else {
                                $('.js-search-tv-ul').append('<li class="text-center">Not found</li>')
                            }
                        } else {
                            $('.js-search-tv-ul').hide()
                            $('.js-search-tv-ul').html('')
                        }
                    }
                });
            } else {
                $('.js-search-tv-ul').hide()
                $('.js-search-tv-ul').html('')
            }
        })
    })
</script>