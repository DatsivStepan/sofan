<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

?>

<?php Pjax::begin(['id' => 'pjax-events-booking', 'enablePushState' => false]); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <script>
        if ($modal.bookingModal) {
            $modal.bookingModal.close();
        }
        const imageURL = '/images/group_192.png';
        swal({
            title: "",
            className: "sweet-alert-css",
            text: "La tua Richiesta di participazione\n" +
            "all'event Juve x Inter, è stata inultrata con\n" +
            "sucesso, riceverai notifica non appena\n" +
            "l'hoster conferma la tua\n" +
            "prenotazione",
            button: false,
            icon: imageURL,
            timer: 2000,
        }).then((value) => {
            location.reload()
        });
    </script>
<?php } ?>

<?php $form = ActiveForm::begin(['id' => 'events-booking-form-', 'options' => ['data-pjax' => 'pjax-events-booking']]); ?>
    <div class="js-events-booking-container col-sm-12 no-padding-lg">
        <div class="col-lg-12" style="margin-top:5px;">
            <div class="row" style="margin-bottom: 30px;">
                <div class="col-sm-2 select-option-col-2 ">
                    <?= Html::dropDownList('js-max-item-count', null, range(1, $model->getAvailablePlacesCount()), ['class' => 'form-control']); ?>
                </div>
                <div class="col-sm-10 text-right no-padding-right-lg">
                    <h2 class="h2-price">
                        <?= $model->getPaymentPerPerson(); ?> <span class="a-persona-text">A persona</span>
                    </h2>
                </div>
            </div>
            <div id="events-booking-form">
                <div class="row">
                    <div class="col-sm-8">
                        <?= $form->field($modelNewBI, 'name[]')->textInput(["placeholder" => 'Name'])->label(false); ?>
                    </div>
                    <div class="col-sm-2 select-option-col-2">
                        <?= $form->field($modelNewBI, 'gender[]')->dropDownList([0 => 'M', 1 => 'G'])->label(false); ?>
                    </div>
                    <div class="col-sm-2 select-option-col-2">
                        <?= $form->field($modelNewBI, 'age[]')->dropDownList(range(1, 100), ['prompt' => 'Età'])->label(false); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
    <script>
        $(function () {
            var content = $('#events-booking-form').html();
            $('.js-events-booking-container').on('change', '[name=js-max-item-count]', function () {

                var count = parseInt($(this).val()) + 1;
                $('#events-booking-form').html('');
//                var ageDrop = $('.js-age-drop').html();
                for (var i = 1; i <= parseInt(count); i++) {
                    $('#events-booking-form').append(content)
                }
            })
        })
    </script>

<?php Pjax::end(); ?>