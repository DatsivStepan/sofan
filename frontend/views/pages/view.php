<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container custom-pages-container">
    <div class="row">
        <div class="col-sm-3 pages-colum-left">
            <?= $this->render('_left_menu', [
                'slug' => $model->slug
            ]); ?>
        </div>
        <div class="col-sm-9">
            <section class="how-works-content">
                <div class="container">
                    <div class="page-container">
                        <h1 class=""><?= $model->name; ?></h1>
                        <div class="fr-view">
                            <?= html_entity_decode($model->content); ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>
        </div>
    </div>
</div>
