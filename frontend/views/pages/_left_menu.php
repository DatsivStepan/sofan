<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<h5><b>Sofan</b></h5>
<ul class="nav flex-column">
    <li class="nav-item <?= $slug == 'how_it_work' ? 'active' : ''; ?>" >
        <a class="nav-link"  href="<?= Url::to('/page/how_it_work') ?>">Come Funziona</a>
    </li>
    <li class="nav-item <?= $slug == 'who_we_are' ? 'active' : ''; ?>" >
      <a class="nav-link"  href="<?= Url::to('/page/who_we_are') ?>">Chi Siamo</a>
    </li>
    <li class="nav-item <?= $slug == 'faq' ? 'active' : ''; ?>" >
      <a class="nav-link"  href="<?= Url::to('/page/faq') ?>">FAQ</a>
    </li>
    <li class="nav-item <?= $slug == 'contact' ? 'active' : ''; ?>" >
      <a class="nav-link"  href="<?= Url::to('/page/contact') ?>">Contattaci</a>
    </li>
</ul>

<h5><b>Termini e condizioni</b></h5>
<ul class="nav flex-column">
    <li class="nav-item <?= $slug == 'term_1' ? 'active' : ''; ?>" >
      <a class="nav-link active"  href="<?= Url::to('/page/term_1') ?>">Termini e Condizioni</a>
    </li>
    <li class="nav-item <?= $slug == 'term_2' ? 'active' : ''; ?>" >
      <a class="nav-link"  href="<?= Url::to('/page/term_2') ?>">Privacy e Cookie</a>
    </li>
</ul>