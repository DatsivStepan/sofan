<?php
/* @var $this yii\web\View */
use frontend\widgets\WLang;

$this->title = 'Sofan Home';
?>
<section class="footer">
	<div class="container">
		<div class="row footer-b-t">
			<div class="col-12 col-md-4 select-block">
				  <div class="form-group">
				    <?= WLang::widget(); ?>
				  </div>
			</div>
			<div class="col-12 col-md-8">
				<div class="row">
					<div class="col-4 sofan">
						<h6> <?= Yii::t('footer', ' Sofan'); ?></h6>
						<a href="/page/how_it_work"><?= Yii::t('footer', 'Come Funziona'); ?></a>
						<a href="/page/who_we_are"><?= Yii::t('footer', 'Chi Siamo'); ?></a>
						<a href="/page/faq"> <?= Yii::t('footer', 'FAQ'); ?> </a>
						<a href="/page/contact"> <?= Yii::t('footer', ' Contattaci'); ?></a>
					</div>
					<div class="col-4 termini">
						<h6>  <?= Yii::t('footer', 'Termini e condizioni'); ?></h6>
						<a href="/page/term_1"> <?= Yii::t('footer', 'Termini e condizioni'); ?></a>
						<a href="/page/term_2"> <?= Yii::t('footer', 'Privacy e coockie'); ?></a>
					</div>
					<div class="col-4 diventa">
						<a href="/en/host"><h6><?= Yii::t('footer', 'Diventa Host'); ?></h6>
						</a>
					</div>
				</div>
			</div>
			<div class="col-12" style="border-bottom: 1px solid #95989A; padding-top:  45.9px"></div>
		</div>
		<div class="row button-footer">
			<div class="col-12 col-sm-6 footer-text-button">
		        <p>&copy; Eternity SRLS <?php //= date('Y') ?></p>
			</div>
			<div class="col-12 col-sm-6 icon-block" style="text-align: right;">
				<a href="#"><img src="/images/facebook-icon-footer.PNG" style="width: 15px;height: 20px"></a>
				<a href="#"><img src="/images/twiter-icon-footer.PNG" style="width: 17px;height: 17px">
				</a>
				<a href="#"><img src="/images/instagram-icon-footer.PNG" style="width: 20px;height: 20px"></a>
			</div>
		</div>
	</div>
</section>