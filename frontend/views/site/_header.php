<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use common\models\LoginForm;
use kartik\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use frontend\models\SignupForm;
use kartik\select2\Select2;
use frontend\components\FunctionHelper;

/* @var $this yii\web\View */
$eventStep = array_key_exists('eventStep', Yii::$app->params) ? Yii::$app->params['eventStep'] : null; 
$hostStep = array_key_exists('hostStep', Yii::$app->params) ? Yii::$app->params['hostStep'] : null; 
?>

<div class="fixed-top" style="background-color:white;">
<section class="header " style="background-color: white;width: 100%;max-width: 1920px;margin:0 auto;">
    
    <div class="container">
        <nav class="navbar navbar-expand-sm navbar-light bg-faded"  style="padding-right:48.2px;padding-left: 48.2px;">
             <a class="navbar-brand logo-left" href="<?= Url::to(['/']); ?>">
                 <img src="/images/logo.png">
             </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
                <img src="/images/align-justify-256.png" style="width: 22px; height: 15px; margin-top: -5px">
            </button>
            <div class="collapse navbar-collapse " id="nav-content">   
                <ul class="navbar-nav ml-auto button-right">
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <li class="nav-item nav-diventa">
                            <?= Html::a(Yii::t('header', 'Diventa Host'), Url::to(['/host']), [
                                'class' => 'btn btn-primary btn-diventa '
                             ])?>
                        </li>
                        <li class="nav-item nav-button">
                            <?= Html::a(Yii::t('header', 'registrati'), null, [
                                'class' => 'btn btn-primary btn-register-login-1 btn-register-login',
                                'data-toggle' => "modal",
                                'data-target' => "#signupModal"
                            ])?>
                        </li>
                        <li class="nav-item nav-button">
                            <?= Html::a(Yii::t('header', 'Accedi'), null, [
                                'class' => 'btn btn-primary btn-register-login',
                                'data-toggle' => "modal",
                                'data-target' => "#loginModal"
                            ])?>
                        </li>
                    <?php } else { ?>
                        <?php $modelU = \common\models\User::findOne(Yii::$app->user->id); ?>
                        <?php if ($eventStep) { ?>
                            
                                <li class="nav-item" style="display: inherit;;padding-right: 15px">
                                    <div class="step-text" style="padding-top:4px;"> Step <?= $eventStep; ?></div>
                                    <div style="padding-top:5px;" class="step-<?= $eventStep; ?>">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </li>
                        
                        <?php } elseif($hostStep) { ?>
                                <li class="nav-item" style="display: inherit;;padding-right: 15px">
                                    <div class="step-text" style="padding-top:4px;"> 
                                        <?= Yii::t('header', 'Step'); ?> 
                                        <?= $hostStep; ?>
                                    </div>
                                    <div style="padding-top:5px;" class="step-<?= $hostStep; ?>">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </li>
                        <?php } else { ?>
                            <?php if ($modelU->getHostCount()) { ?>
                                <li class="nav-item dropdown event-drop" style="padding-top: 2px;">
                                    <a class="nav-item nav-link mr-md-2 event-drop-menu"
                                    href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                         <img src="/images/plus-icon-1.png" style="height: 16px;margin-top: -3px;"
                                          class="icon-plus">
                                          <?= Yii::t('header', 'Crea Evento'); ?>
                                    </a>
                                    <div class="dropdown-menu dropdown-right" aria-labelledby="bd-versions">
                                        <a href="<?= Url::to(['/profile/events/create']); ?>" class="">  
                                            <div style="float: left; overflow: hidden; width: 80%;padding-top: 6px;">
                                                <?= Yii::t('header', 'Nuovo evento'); ?>
                                            </div>
                                            <div style="float: right;">
                                                <img src="/images/nuovo-evento.png">
                                            </div>
                                        </a>
                                        <?php if ($events = $modelU->getEventNotFinished()) { ?>
                                            <?php foreach ($events as $event) { ?>
                                                <a href="<?= $event->getNotFinishedLink(); ?>" class=""> 
                                                    <div style="float: left; overflow: hidden;width: 80%">
                                                         <?= $event->getName(); ?>
                                                    </div>
                                                    <div style="float: right;">
                                                        <img src="/images/nuovo-evento-red.png" class="button-red js-delete-event-header" data-id="<?= $event->id; ?>" title="delete" >
                                                    </div>
                                                </a>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </li>
                            <?php } else { ?>
                                <li class="nav-item nav-diventa">
                                    <?= Html::a(Yii::t('header', 'Diventa Host'), Url::to(['/profile/host']), [
                                        'class' => 'btn btn-primary btn-diventa '
                                     ])?>
                                </li>
                            <?php } ?>

                        <?php } ?>
                        <li class="nav-item img-nav">
                            <div>
                                <img src="<?= $modelU->getAvatar(); ?>">
                            </div>
                            <?php if ($countAll = $modelU->getNotReadCount()) { ?>
                                <div class="red-box"><?= $countAll; ?></div>
                            <?php } ?>
                        </li>
                        <li class="nav-item dropdown drop-menu">
                            <a class="nav-item nav-link mr-md-2"
                            href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                 <img src="/images/b1.PNG">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                                <a href="<?= Url::to(['/profile/default/dashboard']); ?>">
                                    <div style="display: inline-block;">
                                        <?= Yii::t('header', 'Dashboard'); ?>
                                    </div>
                                </a>
                                <a href="<?= Url::to(['/profile/events']); ?>">
                                    <?= Yii::t('header', 'Eventi organizzati'); ?>
                                </a>
                                <a href="<?= Url::to(['/profile/events-booking']); ?>">
                                 <?= Yii::t('header', 'Prenotazioni'); ?> 
                                </a>
                                <a href="<?= Url::to(['/profile/messages']); ?>">
                                    <?= Yii::t('header', 'Messagi'); ?> 
                                    <?php if ($countM = $modelU->getMessagesCount()) { ?>
                                        <div class="menu-block-right"><?= $countM; ?></div>
                                    <?php } ?>
                                </a>
                                <a href="<?= Url::to(['/profile/notifications']); ?>">
                                   <?= Yii::t('header', 'Notifiche'); ?>  
                                    <?php if ($countN = $modelU->getNotificationsCount()) { ?>
                                        <div class="menu-block-right"><?= $countN; ?></div>
                                    <?php } ?>
                               </a>
                                <?= Html::a(Yii::t('header', 'Profile'), Url::to(['/profile/default/index']), ['class' => ''])?>
                                <?= Html::beginForm(['/site/logout'], 'post'); ?>
                                <?= Html::submitButton('Logout', ['class' => 'logout']) ?>
                                <?= Html::endForm(); ?>
                            </div>
                         </li>
                     <?php } ?>
                </ul>
            </div>
        </nav>
    </div>

</section>
</div>

<?php if (Yii::$app->user->isGuest) { ?>
    <?php $modelLogin = new LoginForm(); ?>
    <!-- Modal -->
    <div id="loginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-header" style="border: none;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:0px ">
                <div class="modal-body">
                    <div class="js-site-login">
                        <?= \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
                        <div class="col-lg-12 form-control-login">
                            <div class="signup-or-separator">
                                <span class="h6 signup-or-separator--text">
                                    <?= Yii::t('login', 'O'); ?></span>
                                <hr>
                            </div>
                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'enableAjaxValidation' => true,
                                'action' => \yii\helpers\Url::to('/site/login-ajax', true)
                            ]); ?>

                                <?= $form->field($modelLogin, 'email')->textInput(['autofocus' => true, 
                                "placeholder" => 'Indirizzo Email', 'class' => 'form-control input-sofan'])->label(false) ?>

                                <?= $form->field($modelLogin, 'password')->passwordInput([
                                    "placeholder" => "Password",'class' => 'form-control input-sofan' ])->label(false)
                                 ?>
                                 <div style="display: inline-block;">
                                <?= $form->field($modelLogin, 'rememberMe')->widget(CheckboxX::classname(), [
                                    'pluginOptions'=>['threeState'=>false]
                                ])->label(false); ?>

                                </div>
                                <div style="display: inline-block;padding-left: 5px;">
                                <label style="color: #999;">
                                    <?= Yii::t('login', 'Remember me'); ?>
                                </label>
                                </div>
                                <div class="text-login" style="color:#999;">
                                    <?= Yii::t('login', ''); ?>    <?= Html::a('Password dimenticata? ', ['site/request-password-reset']) ?>
                                </div>

                                <div class="form-group" style="margin-top: 10px;margin-bottom: 0px;">
                                    <?= Html::submitButton('Accesso', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                </div>

                            <?php ActiveForm::end(); ?>
                            <hr>
                            <div class="row">
                                <div class="col-6 col-md-7 text-bottom"><?= Yii::t('login', 'Non hai un account?'); ?></div>
                                <div class="col-6 col-md-5 button-sign-im-login-form">
                                    <?= Html::a(Yii::t('login', 'Registrati'), null, ['class' => 'btn btn-primary js-go-to-signup']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
  
    <!-- Modal -->
    <div id="signupModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-header" style="border: none;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <?php $modelSignUp = new SignupForm(); ?>
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:0px ">
                <div class="modal-body">
                    <div class="site-signup-social js-sigup-block-social">
                        <?= \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
                        <div class="signup-or-separator">
                            <span class="h6 signup-or-separator--text">
                                <?= Yii::t('signup', 'O'); ?>
                            </span>
                            <hr>
                        </div>
                        <div class="open-button js-open-signup">
                        <?= Html::a('Registrati con la tua Email', null, ['class' => ''])?>
                        </div>
                        <div class="text-open">
                            <?= Yii::t('signup', 'Facendo clic su Registrati o Continua con, dischiaro di accettare i '); ?>
                            <a href="/page/term_1">
                                <?= Yii::t('signup', 'Termini del Servizio,'); ?></a> <?= Yii::t('signup', 'i'); ?> 
                             <a href="/page/term_1"><?= Yii::t('signup', 'Termini de servizio sui pagamenti,'); ?> </a> 
                             la 
                             <a href="/page/term_1"><?= Yii::t('signup', ' Informative sulla Privacy'); ?></a> <?= Yii::t('signup', 'e la'); ?> 
                             <a href="/page/term_2"> <?= Yii::t('signup', 'Politiche di non discriminazione'); ?>
                                 
                             </a><?= Yii::t('signup', 'di'); ?>
                             <text style="font-weight: bold;"> <?= Yii::t('signup', 'TVmates'); ?></text>
                        </div>
                        <hr>
                    </div>

                    <div class="site-signup d-none js-sigup-block-form">
                        <span class="js-signup-back"></span>
                        <p><?= Yii::t('signup', 'Registrati con'); ?>
                            <?= Html::a('Facebook', null, []); ?> o 
                            <?= Html::a('Google', null, []); ?>
                        </p>

                        <div class="signup-or-separator-1">
                            <span class="h6 signup-or-separator--text"><?= Yii::t('signup', 'O'); ?></span>
                            <hr>
                        </div>
                        <div class="col-lg-12" style="padding: 0px;">
                            <?php $form = ActiveForm::begin([
                                'id' => 'signup-form',
                                'enableAjaxValidation' => true,
                                'action' => \yii\helpers\Url::to('/site/signup-ajax', true)
                            ]); ?>
                                <?= $form->field($modelSignUp, 'email') ->textInput(["placeholder" => 'Indirizzo Email', 'class' => 'form-control input-sofan'])->label(false) ?>
                                <?= $form->field($modelSignUp, 'name')->textInput(["placeholder" => 'Nome', 'class' => 'form-control input-sofan'])->label(false) ?>
                                <?= $form->field($modelSignUp, 'surname')->textInput([ "placeholder" => "Cognome" , 'class' => 'form-control input-sofan'])->label(false)  ?>
                                <?= $form->field($modelSignUp, 'password')->passwordInput(["placeholder" => "Password" , 'class' => 'form-control input-sofan'])->label(false) ?>
                                <div class="row">
                                    <div class="col-12 sign-data-name">
                                        <h6><?= Yii::t('signup', 'Data di nascita'); ?></h6>
                                        <p>
                                           <?= Yii::t('signup', 'Per registrati devi aver compiuto almeno 18 anni. La data del tuo compleanno non sara visibile per altre persone.'); ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3" style="text-align:left; padding-right: 0px;">
                                        <?= $form->field($modelSignUp, 'year')->widget(Select2::className(), [
                                            'data' => FunctionHelper::getDays(),
                                            'hideSearch' => true,
                                            'pluginOptions' => [
                                                'dropdownCssClass' => 'options-drop option-font',
                                                'allowClear' => true
                                            ]
                                        ])->label(false) ?>
                                    </div>
                                    <div class="col-5" style="text-align: left; padding-right: 0px;">
                                        <?= $form->field($modelSignUp, 'month')->widget(Select2::className(), [
                                            'data' => FunctionHelper::getMonths(),
                                            'hideSearch' => true,
                                            'pluginOptions' => [
                                                'dropdownCssClass' => 'options-drop option-font',
                                                'allowClear' => true
                                            ]
                                        ])->label(false) ?>
                                    </div>
                                    <div class="col-4" style="text-align: left;">
                                        <?= $form->field($modelSignUp, 'day')->widget(Select2::className(), [
                                            'data' => FunctionHelper::getYears(),
                                            'hideSearch' => true,
                                            'pluginOptions' => [
                                                'dropdownCssClass' => 'options-drop option-font',
                                                'allowClear' => true
                                            ]
                                        ])->label(false) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-2" style="text-align: right; padding-right: 0px;">
                                        <?php $modelSignUp->terms1 = 1;?>
                                        <?= $form->field($modelSignUp, 'terms1')->widget(CheckboxX::classname(), [
                                            'pluginOptions'=>['threeState'=>false]
                                        ])->label(false); ?>
                                        <?php //= $form->field($model, 'terms1')->checkbox()->label(false) ?>
                                    </div>
                                    <div class="col-10" style="font-size: 12px;">
                                        <?= Yii::t('signup', 'Vorrei ricevere coupon, promozioni, sondaggi e aggiornamenti via email su Tvmates e i suoi partner.'); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-2" style="text-align: right; padding-right: 0px;">
                                        <?php $modelSignUp->terms2 = 1;?>
                                        <?= $form->field($modelSignUp, 'terms2')->widget(CheckboxX::classname(), [
                                            'pluginOptions'=>['threeState'=>false]
                                        ])->label(false); ?>
                                    </div>
                                    <div class="col-10 sign-href" style="font-size: 12px;">
                                        <?= Yii::t('signup', 'Facendo clic su Registrati o Continua con, dischiaro di accettare i '); ?>
                                        <a href="/page/term_1"><?= Yii::t('signup', 'Termini del Servizio'); ?> </a>
                                        , i 
                                        <a href="/page/term_1"><?= Yii::t('signup', 'Termini de servizio sui pagamenti'); ?></a>
                                            <?= Yii::t('signup', ', la '); ?>
                                        <a href="/page/term_1"><?= Yii::t('signup', 'Informative sulla Privacy'); ?></a>
                                         <?= Yii::t('signup', 'e la '); ?>
                                         <a href="/page/term_2"><?= Yii::t('signup', 'Politiche di non discriminazione'); ?></a>
                                          <?= Yii::t('signup', 'di'); ?> 
                                        <text style="font-weight: bold;"><?= Yii::t('signup', 'TVmates'); ?>  </text>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= Html::submitButton('Registrati', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-8 sign-text-bottom">
                                <?= Yii::t('signup', 'Hai gia un account Su TVmates?'); ?>    
                        </div>
                        <div class="col-4 button-login-sign">
                            <?= Html::a(Yii::t('login', 'Accedi'), null, ['class' => 'btn btn-primary js-go-to-login']) ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
 <?php } ?>


<script>
    $(function(){

        $(document).on('click', '.js-go-to-login', function(){
            $('#loginModal').modal('show')
            $('#signupModal').modal('hide')
        })

        $(document).on('click', '.js-go-to-signup', function(){
            $('#loginModal').modal('hide')
            $('#signupModal').modal('show')
        })
        
        $(document).on('click', '.js-open-signup', function(){
            $('.site-signup').removeClass('d-none')
            $('.site-signup-social').addClass('d-none')
        })

        $(document).on('click', '.js-signup-back', function(){
            $('.site-signup').addClass('d-none')
            $('.site-signup-social').removeClass('d-none')
        })

        $(document).on('click', '.js-delete-event-header', function(e){
            e.preventDefault()
            var eventId = $(this).data('id')
            var thisElem = $(this)
            $.ajax({
                url: '<?= Url::to(['/profile/events/delete']) ?>/' + eventId,
                method: 'post',
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        thisElem.closest('a').remove()
                    }
                }
            });
        })
    })
</script>