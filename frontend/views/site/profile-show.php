<?php

use yii\helpers\Html,
    yii\helpers\Url,
    kartik\rating\StarRating,
    common\models\UserVerification;

/* @var $this yii\web\View */

$this->title = 'Sofan Home';
$rateStars = $user->getStarRatingCount();

?>
<style>
    .rating-container .caption {
        display: none !important;
    }
</style>
<div class="container">
    <div class="row" style="margin-top:75px;">
        <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">
            <div class="left-block-profile-show">
                <div class="bord-bott">
                    <h4 class="h4-verifiche"> Verifiche </h4>
                    <p class="p-susses-text">
                        <?php if ($user->getUserVerificationStatus(UserVerification::TYPE_PHONE)) { ?>
                            <img src="/images/susses-icon.png">
                            <text> Cellulare verificato</text>
                        <?php } ?>
                    </p>
                    <p class="p-susses-text">
                        <?php if ($user->getUserVerificationStatus(UserVerification::TYPE_EMAIL)) { ?>
                            <img src="/images/susses-icon.png">
                            <text>Email Verificata</text>
                        <?php } ?>
                    </p>
                    <p class="p-susses-text">
                        <?php if ($user->getUserVerificationStatus(UserVerification::TYPE_FACEBOOK)) { ?>
                            <img src="/images/susses-icon.png">
                            <text>Facebook verificato</text>
                        <?php } ?>
                    </p>
                </div>
                <div class="bord-bott-attivit">
                    <h4 class="h4-verifiche"> Attività </h4>
                    <p class="p-attivit"> <?= $user->getEventsCount(); ?> Eventi Pubblicati </p>
                    <p class="p-attivit"> Data d'iscrizione : <?= $user->getDateCreate('M Y'); ?> </p>
                    <p class="p-attivit"> Ultimo Accesso : Oggi alle <?= $user->getDateCreate('H:i'); ?> </p>
                </div>
                <?php if ($location) { ?>
                    <h4 class="h4-verifiche-local"> Location </h4>
                    <div class="text-center">
                        <img class="img-location-defoult" src="<?= $location->getDefaultImage(); ?>">
                        <p class="p-city-location"><?= $location->city; ?></p>
                        <div class="stars-localR">
                            <?= StarRating::widget([
                                'name' => 'locationR',
                                'value' => $location->getStarRatingCount()['sum'],
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'disabled' => true,
                                    'showClear' => false
                                ]
                            ]); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">
            <div class="right-block-profile-show">
                <div class="row no-margin">
                    <div class="col-3 col-sm-2 no-padding ">
                        <img class="img-avatar" src="<?= $user->getAvatar(); ?>">
                    </div>
                    <div class="col-9 col-sm-10 padd-lef-user-block">
                        <h2 class="h-user-name-surname"><?= $user->getUsernameWithSurname() ?><a class="h-user-name-surname___a"></a></h2>
                        <h2 class="h-name-anni"><?= $user->getAge(); ?></h2>
                        <div class="stars-user">
                            <?= StarRating::widget([
                                'name' => 'userR',
                                'value' => $rateStars['sum'],
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'disabled' => true,
                                    'showClear' => false
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="row no-margin">
                    <div class="col-4 col-sm-3 no-padding ">
                        <span class="span-text-val">Valutazione media:</span>
                    </div>
                    <span class="col-8 col-sm-9">
                        <img class="img-favorit-star" src="/images/favorite2.png"><span
                                class="sp-sum"><?= number_format($rateStars['sum'], 1, '.', ''); ?>/ 5</span><span
                                class="sp-feedback"> - <?= $rateStars['count']; ?> feedback</span>
                </div>
                <div class="row no-margin">
                    <div class="col-4 col-sm-3 no-padding">
                        <span class="span-text-pref">Preferenze:</span>
                    </div>
                    <div class="col-8 col-sm-9 icon-prefereze">
                        <?php if ($location) { ?>
                            <?php if ($location->wifi) { ?>
                                <span class="clas-text-centr-icons">
                                <img class="event-one-ico" src="/images/wifi.png">
                            </span>
                            <?php } ?>
                            <?php if ($location->food) { ?>
                                <span class="clas-text-centr-icons">
                                <img class="event-one-ico" src="/images/dinner.png">
                            </span>
                            <?php } ?>
                            <?php if ($location->conditioner) { ?>
                                <span class="clas-text-centr-icons">
                                <img class="event-one-ico" src="/images/air-conditioner.png">
                            </span>
                            <?php } ?>
                            <?php if ($location->balcony) { ?>
                                <span class="clas-text-centr-icons">
                                <img class="event-one-ico" src="/images/balcony.png">
                            </span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="row no-margin block-qualcosa">
                    <p class="p-qual-text">Qualcosa su di me...</p>
                    <p class="p-serio-text">"<?= $user->userInfoModel && $user->userInfoModel->about ? $user->userInfoModel->about : 'Non'; ?>"</p>
                </div>
                <div class="row no-margin block-ricevuti">
                    <div class="col-12 col-sm-6 col-md-5 col-lg-4 col-xl-4 no-padding">
                        <p class="p-text-ricevuti">Feedback ricevuti</p><img class="img-ricev-star"
                                                                             src="/images/favorite2.png"><span
                                class="sp-ricev-sum"><?= number_format($rateStars['sum'], 1, '.', ''); ?>/ 5</span><span
                                class="sp-ricev-feedback"> - <?= $rateStars['count']; ?> feedback</span>
<!--                        <p class="p-text-buon">Buon conducente - 3 / 3 </p>-->
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-8 col-xl-8 no-padding">
                        <?php foreach ($user->getFeedbackStarts() as $name => $count) { ?>
                            <div class="row no-margin" style="padding-right: 10px;">
                                <div class="col-8 col-sm-6 col-md-7 col-xl-10 no-padding text-right">
                                    <span class="name-sp-ricev"><?= $name; ?></span>
                                </div>
                                <div class="col-4 col-sm-6 col-md-5 col-xl-2 no-padding stars-block">
                                    <?= StarRating::widget([
                                        'name' => $name,
                                        'value' => $count,
                                        'pluginOptions' => [
                                            'size' => 'xs',
                                            'disabled' => true,
                                            'showClear' => false
                                        ]
                                    ]); ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="row no-margin block-lascia">
                    <div class="col-12 no-padding">
                        <?php if ($feedbacks) { ?>
                            <?php foreach ($feedbacks as $feedback) { ?>
                                <?php if (($user = $feedback->userModel) && $feedback->note) { ?>
                                    <div class="row no-margin border-bot-lascia">
                                        <div class="col-2 col-sm-2 col-xl-1 align-items-center img-block-avat">
                                            <img class="img-avatar-lascia" src="<?= $user->getAvatar(); ?>">
                                        </div>
                                        <div class="col-10 col-sm-10 col-xl-11">
                                            <a class="green-circle-online"></a><h5 class="h-user-nam"><?= $user->getUsernameWithSurname(); ?></h5>
                                            <p class="p-user-text"><span class="sp-nam-second-user">Carla M:</span> <?= $feedback->note; ?></p>
                                            <p class="p-dat-text">mag 2018</p>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>