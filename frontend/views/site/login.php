<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\checkbox\CheckboxX;

$this->title = 'Login';
?>

<?php Pjax::begin(['id' => 'pjax-signup' . $uid, 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successDataLogin')) { ?>
        <script>
            if ($modal.signupModal) {
                $modal.signupModal.close();
            }
            if ($modal.loginModal) {
                $modal.loginModal.close();
            }
            parent.location.reload();
        </script>
    <?php } ?>

    <div class="js-site-login">
        <?= \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
        <div class="col-lg-12 form-control-login">
            <div class="signup-or-separator">
                <span class="h6 signup-or-separator--text">
                    <?= Yii::t('login', 'O'); ?></span>
                <hr>
            </div>
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['data-pjax' => 'pjax-login' . $uid]]); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true, 
                "placeholder" => 'Indirizzo Email', 'class' => 'form-control input-sofan'])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput([
                    "placeholder" => "Password",'class' => 'form-control input-sofan' ])->label(false)
                 ?>
                 <div style="display: inline-block;">
                <?= $form->field($model, 'rememberMe')->widget(CheckboxX::classname(), [
                    'pluginOptions'=>['threeState'=>false]
                ])->label(false); ?>
                    
                </div>
                <div style="display: inline-block;padding-left: 5px;">
                <label style="color: #999;">
                    <?= Yii::t('login', 'Remember me'); ?>
                </label>
                </div>
                <div class="text-login" style="color:#999;">
                    <?= Yii::t('login', ''); ?>    <?= Html::a('Password dimenticata? ', ['site/request-password-reset']) ?>
                </div>

                <div class="form-group" style="margin-top: 20px;">
                    <?= Html::submitButton('Accesso', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
            <hr>
            <div class="row">
            <div class="col-6 col-md-7 text-bottom"><?= Yii::t('login', 'Non hai un account?'); ?></div>
            <div class="col-6 col-md-5 button-sign-im-login-form">
                <?= Html::a(Yii::t('login', 'Registrati'), null, ['class' => 'btn btn-primary js-go-to-signup']) ?>
            </div>
        </div>
        </div>
    </div>
        
<?php Pjax::end();  ?>