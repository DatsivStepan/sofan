<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">
    <div class="row no-margin site-error">
        <div class="col-12 offset-0 col-md-8 offset-md-2 text-center">
            <h1 class="h1-title-error">#404</h1>
            <div class="denger-alert">
                <?= nl2br(Html::encode($message)) ?>
            </div>
            <p class="p-error">
                The above error occurred while the Web server was processing your request.
            </p>
            <p class="p-error">
                Please contact us if you think this is a server error. Thank you.
            </p>
        </div>
    </div>
</div>
