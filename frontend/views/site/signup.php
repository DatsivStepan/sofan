<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\checkbox\CheckboxX;

$uid = uniqid();

?>

<?php Pjax::begin(['id' => 'pjax-signup' . $uid, 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successDataSignup')) { ?>
        <script>
            if ($modal.signupModal) {
                $modal.signupModal.close();
            }
            if ($modal.loginModal) {
                $modal.loginModal.close();
            }

            parent.location.reload();
        </script>
    <?php } ?>

        <div class="site-signup-social">
            <?= \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
            <div class="signup-or-separator">
                <span class="h6 signup-or-separator--text">
                    <?= Yii::t('signup', 'O'); ?>
                </span>
                <hr>
            </div>
            <div class="open-button js-open-signup">
            <?= Html::a('Registrati con la tua Email', null, ['class' => ''])?>
            </div>
            <div class="text-open">
                <?= Yii::t('signup', 'Facendo clic su Registrati o Continua con, dischiaro di accettare i '); ?>
                <a href="/page/term_1">
                    <?= Yii::t('signup', 'Termini del Servizio,'); ?></a> <?= Yii::t('signup', 'i'); ?> 
                 <a href="/page/term_1"><?= Yii::t('signup', 'Termini de servizio sui pagamenti,'); ?> </a> 
                 la 
                 <a href="/page/term_1"><?= Yii::t('signup', ' Informative sulla Privacy'); ?></a> <?= Yii::t('signup', 'e la'); ?> 
                 <a href="/page/term_2"> <?= Yii::t('signup', 'Politiche di non discriminazione'); ?></a><?= Yii::t('signup', 'di'); ?>
                 <text style="font-weight: bold;"> <?= Yii::t('signup', 'TVmates'); ?></text>
            </div>
            <hr>
        </div>

        <div class="site-signup d-none">
            <h1><?= Html::encode($this->title) ?></h1>

            <p><?= Yii::t('signup', 'Registrati con'); ?>
                <?= Html::a('Facebook', null, []); ?> o 
                <?= Html::a('Google', null, []); ?>
            </p>

            <div class="signup-or-separator-1">
                <span class="h6 signup-or-separator--text"><?= Yii::t('signup', 'O'); ?></span>
                <hr>
            </div>
                <div class="col-lg-12">
                    <?php $form = ActiveForm::begin(['id' => 'signup-form', 'options' => ['data-pjax' => 'pjax-signup' . $uid]]); ?>
                        <?= $form->field($model, 'email') ->textInput([ 
                "placeholder" => 'Indirizzo Email', 'class' => 'form-control input-sofan'])->label(false) ?>
                        <?= $form->field($model, 'name')->textInput([ 
                "placeholder" => 'Nome', 'class' => 'form-control input-sofan'])->label(false) ?>
                        <?= $form->field($model, 'surname')->textInput([ "placeholder" => "Cognome" , 'class' => 'form-control input-sofan'])->label(false)  ?>
                        <?= $form->field($model, 'password')->passwordInput([
                    "placeholder" => "Password" , 'class' => 'form-control input-sofan'])->label(false) ?>
                    <div class="row">
                        <div class="col-12 sign-data-name">
                            <h6><?= Yii::t('signup', 'Data di nascita'); ?></h6>
                            <p>
                               <?= Yii::t('signup', 'Per registrati devi aver compiuto almeno 18 anni. La data del tuo compleanno non sara visibile per altre persone.'); ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-2" style=" padding-right: 0px;">
                        <?php $model->terms1 = 1;?>
                        <?= $form->field($model, 'terms1')->widget(CheckboxX::classname(), [
                            'pluginOptions'=>['threeState'=>false]
                        ])->label(false); ?>
                        <?php //= $form->field($model, 'terms1')->checkbox()->label(false) ?>
                    </div>
                    <div class="col-10" style="font-size: 12px;">
                        <?= Yii::t('signup', 'Vorrei ricevere coupon, promozioni, sondaggi e aggiornamenti via email su Tvmates e i suoi partner.'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2" style="text-align: right; padding-right: 0px;">
                        <?php $model->terms2 = 1;?>
                        <?= $form->field($model, 'terms2')->widget(CheckboxX::classname(), [
                            'pluginOptions'=>['threeState'=>false]
                        ])->label(false); ?>
                    </div>
                    <div class="col-10 sign-href" style="font-size: 12px;">
                        <?= Yii::t('signup', 'Facendo clic su Registrati o Continua con, dischiaro di accettare i '); ?>
                        <a href="/page/term_1"><?= Yii::t('signup', 'Termini del Servizio'); ?> </a>
                        , i 
                        <a href="/page/term_1"><?= Yii::t('signup', 'Termini de servizio sui pagamenti'); ?></a>
                            <?= Yii::t('signup', ', la '); ?>
                        <a href="/page/term_1"><?= Yii::t('signup', 'Informative sulla Privacy'); ?></a>
                         <?= Yii::t('signup', 'e la '); ?>
                         <a href="/page/term_2"><?= Yii::t('signup', 'Politiche di non discriminazione'); ?></a>
                          <?= Yii::t('signup', 'di'); ?> 
                        <text style="font-weight: bold;"><?= Yii::t('signup', 'TVmates'); ?>  </text>
                    </div>
                </div>

                    <div class="form-group">
                        <?= Html::submitButton('Registrati', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <hr style="margin-left: 15px; margin-right: 15px;">
        </div>
        <div class="row" style="margin: 0px 15px 15px 15px;">
            <div class="col-8 sign-text-bottom">
                    <?= Yii::t('signup', 'Hai gia un account Su TVmates?'); ?>    
            </div>
            <div class="col-4 button-login-sign">
                <?= Html::a(Yii::t('login', 'Accedi'), null, ['class' => 'btn btn-primary js-go-to-login']) ?>
            </div>
        </div>
<?php Pjax::end();  ?>
<script>
    $(function(){
        $(document).on('click', '.js-open-signup', function(){
            $('.site-signup').removeClass('d-none')
            $('.site-signup-social').addClass('d-none')
        })
    })
</script>