<?php
use kartik\widgets\ActiveForm,
    frontend\models\EventsSearch,
    kartik\widgets\Select2,
    common\models\TvShowCategories,
    yii\helpers\Html,
    kartik\date\DatePicker,
    yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Sofan Home';
$eventsSearch = new EventsSearch();
$tvArray = TvShowCategories::getAllWithCategory();
?>
<section class="home-img-block">
</section>

<section class="home-content-menu">
    <div class="container">
        <div class="section-3-input">
            <div class="row">
                <div class="col-11" data-wow-duration="1s" data-wow-delay="0.1s">
                        <h4>
                            <?= Yii::t('home', 'Cerca il tuo Evento'); ?>
                        </h4>
                        <p>
                            <?= Yii::t('home', 'Guarda il tuo programma TV preferito a casa di altri appassionati come te!'); ?>
                        </p>
                </div>
            </div>
            <?php $form = ActiveForm::begin(['id' => 'events-search-form', 'action' => Url::to(['/events']), 'method' => 'get']); ?>

                <div class="row index-input-block" data-wow-duration="2s" data-wow-delay="0.1s" style="padding-top: 35.3px;padding-bottom: 67.8px;">
                    <div class="col-12 col-sm-12 col-lg-5 col-xl-5 search-block">
                        <?= $form->field($eventsSearch, 'tv_show_id')->hiddenInput()->label(false); ?>
                        <?= $form->field($eventsSearch, 'tv_show_name')->textInput(['placeholder' => 'Tutti Eventi'])->label(false); ?>
                        <div style="position:relative;" class="js-search-tv-block">
                            <ul class="js-search-tv-ul show-dropdown">
                            </ul>
                        </div>
                    </div>
                    
                    
                    <div class="col-12 col-lg-7 col-xl-7 input-block">
                        <div class="row input-block-sof">
                            <div class="col-12 col-sm-6 col-lg-4 col-xl-4  dove-block">
                                <?= $form->field($eventsSearch, 'cityName')->textInput(["placeholder" => 'Dove', 'class' => 'form-control input-sofan form-dove'])->label(false); ?>
                            </div>

                            <div class="col-12 col-sm-6 col-lg-4  col-xl-4 data-block">
                            <?= $form->field($eventsSearch, 'date_start')
                                ->widget(DatePicker::className(), [
                                    'removeButton' => false,
                                    'value' => date('Y-m-d'),
                                    'readonly' => true,
                                    'options' => ["placeholder" => 'Data', 'class' => 'input-sofan form-data'],
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'class' => '2222222222',
                                        'format' => 'yyyy-mm-dd',
                                        'startDate'=> '0d',
                                    ]
                                ])->label(false); ?>
                            </div>
                            
                            <div class="col-12 col-sm-12 col-lg-4 col-xl-4 cedra-block">
                                <?= Html::submitButton('Cerca<img src="/images/up-button.png" style="width: 10px;height: 15px;box-sizing: initial;padding-left:12px;margin-top:-1px;">', 
                                ['class' => 'cersa btn-sofan']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
            
        </div>
    </div>
</section>
<section class="hai-una" style="background-color:#F6F8FA;">
	<div class="container">
        <div class="una-padding">
            <div class="row">
                <div class="col-12 col-sm-8 col-md-8 main-triangle">
                        <img src="/images/Rectangle.png" style="height: 362.45px;width:578.12px" class="animated icons">
                </div>
                <div class="col-12 col-sm-4 col-md-4 rigt-block-text">
                    <h5 class="wow animate fadeInRight" data-wow-duration="1s" data-wow-delay="0.1s">
                        <?= Yii::t('home', 'Hai una TV <br> e una location?'); ?>
                    </h5>
                    <p class="wow animate fadeInRight" data-wow-duration="1s" data-wow-delay="0.1s">
                        <?= Yii::t('home', 'Con Sofan guadagni e <br>trovi nuovi amici !'); ?>
                    </p>
                    <a href="<?= Url::to(['/profile/host']); ?>" class="btn btn-primary btn-sofan wow animate fadeInRight" data-wow-duration="2s" data-wow-delay="0.05s">
                        <?= Yii::t('home', 'Diventa Host'); ?>
                    </a>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="rerche-sofan">
	<div class="container">
		<div class="row perche-block">
			<div class="col-12 features-section">
				<h2>
                    <?= Yii::t('home', 'Perché Sofan?'); ?>
                </h2>
			</div>
			<div class="col-12 col-sm-4 hosts wow animate fadeInRight" data-wow-duration="1s" data-wow-delay="0.1s">
				<img src="/images/piggy-bank.png" style="height: 106.67px; width: 82.3px"
				 class="animated icons">
				<h4>
                    <?= Yii::t('home', 'Hosts'); ?>
                </h4>
				<p>
                    <?= Yii::t('home', "Possono recuperare una parte del costo dell'abbonamento pay TV"); ?>
                </p>

			</div>
			<div class="col-12 col-sm-4 hosts wow animate fadeInRight" data-wow-duration="2s" data-wow-delay="0.2s">
				<img src="/images/trusting-in-someone.png" class="animated icons" 
				style="height: 106.67px; width: 82.3px">
				<h4>
                    <?= Yii::t('home', 'Guests'); ?>
                </h4>
				<p>
                    <?= Yii::t('home', 'Hanno la possibilità di trovare un luogo confortevole ed economico dove guardare i loro programmi televesivi preferiti.'); ?>
                </p>
			</div>
			<div class="col-12 col-sm-4 hosts wow animate fadeInRight" data-wow-duration="3s" data-wow-delay="0.3">
				<img src="/images/team.png" class="animated icons"
				style="height: 106.67px; width: 100.3px">
				<h4>
                    <?= Yii::t('home', 'Entrambi'); ?>
                </h4>
				<p><?= Yii::t('home', 'condividono le proprie paassioni in un ambiente giovane e internazionale'); ?>
                </p>
			</div>
		</div>
	</div>
</section>
<script>
    $(function(){
        $(document).click(function(){
            $('.js-search-tv-ul').hide();
        });

        $(document).on('click', '.js-search-tv-ul .js-tv-show', function(event){
            $('.js-search-tv-ul').hide()
            $('.js-search-tv-ul').html('');
            var id = $(this).data('id');
            var text = $(this).text();
            $('#eventssearch-tv_show_id').val(id);
            $('#eventssearch-tv_show_name').val(text);
            event.stopPropagation();
        });

        $(document).on('click', '.js-search-tv-ul .js-tv-show-category, #eventssearch-tv_show_name', function(e){
            e.stopPropagation();
        });

        $(document).on('keyup', '#eventssearch-tv_show_name', function(){
            $('#eventssearch-tv_show_id').val('')
            var search_val = $(this).val();
            if (search_val.length > 2) {
                $.ajax({
                    url: '<?= Url::to(['/site/get-tv-show']) ?>/',
                    method: 'get',
                    dataType: 'json',
                    data: {value:search_val},
                    success: function (response) {
                        if (response.status && response.data) {
                            $('.js-search-tv-ul').show();
                            $('.js-search-tv-ul').html('')
                            if (response.data.length != 0) {
                                var group = response.data;
                                $.each(group, function( index, value ) {
                                    console.log('index ', index);
                                    if(value){
                                        $('.js-search-tv-ul').append('<li class="js-tv-show-category category-name ' + index.toLowerCase() + '">' + index + '</li>')
                                        console.log(value)
                                        $.each(value, function( i, v ) {
                                            $('.js-search-tv-ul').append('<li class="js-tv-show show-item" data-id="'+i+'">' + v + '</li>')
                                        })
                                    }
                                    //$('.js-search-tv-ul').append('<li class="js-tv-show" data-id="'+index+'">' + value + '</li>')
                                });
                            } else {
                                $('.js-search-tv-ul').append('<li class="text-center">Not found</li>')
                            }
                        } else {
                            $('.js-search-tv-ul').hide()
                            $('.js-search-tv-ul').html('')
                        }
                    }
                });
            } else {
                $('.js-search-tv-ul').hide()
                $('.js-search-tv-ul').html('')
            }
        })
    })
</script>