<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row no-margin site-error">
        <div class="col-12 offset-0 col-md-6 offset-md-3 text-center">
            <h1 class="h1-title-request"><?= Html::encode($this->title) ?></h1>

            <p class="p-error">Please fill out your email. A link to reset password will be sent there.</p>

            <div class="row">
                <div class="col-12 offset-0 col-md-10 offset-md-1">
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
