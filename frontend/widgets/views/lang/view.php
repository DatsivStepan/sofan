<?php
use yii\helpers\Html;
?>
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link menu" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?= $current->name;?>
        </a>
        <div class="dropdown-menu menu-drop" aria-labelledby="navbarDropdownMenuLink">
            <?php foreach ($langs as $lang):?>
                <?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl(), [
                    'class' => 'dropdown-item'
                    ]) ?>
            <?php endforeach;?>
        </div>
      </li>
    </ul>