<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "tv_show_categories".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class TvShowCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tv_show_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    private function add($categName)
    {
        $category = new TvShowCategories();
        $category->name = $categName;
        $category->save();
        return $category->id;
    }  

    public function issetCategory($categName)
    {
        $row = TvShowCategories::find()->where(['name' => $categName])->one();
        if (!$row)
        {
            return $this->add($categName);
        }else {
            return $row->id;
        }

    }
}
