<?php

namespace console\models;

use Yii;
use yii\db\ActiveRecord;
use \console\models\TvShowCategories;

/**
 * This is the model class for table "tv_show".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property string $date_start
 * @property string $time_start
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class TvShow extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tv_show';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['category_id', 'status'], 'integer'],
            [['date_start', 'time_start', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category ID',
            'date_start' => 'Date Start',
            'time_start' => 'Time Start',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    private function addMovi($item, $categoryId)
    {
        $show = new TvShow();
        $show->name = $item['title'];
        $show->category_id = $categoryId;
        $show->status = ($item['status'] == 'Released') ? 1 : 0;
        $show->api_id = $item['id'];
        $show->created_at = date("Y-m-d");
        $show->updated_at = date("Y-m-d");

        $show->save();
    }

    private function addTv($item, $categoryId)
    {
        $show = new TvShow();
        $show->name = $item['name'];
        $show->category_id = $categoryId;
        $show->status = 0;
        $show->api_id = $item['id'];
        $show->created_at = date("Y-m-d");
        $show->updated_at = date("Y-m-d");

        $show->save();
    }

    private function addSport($item, $categoryId)
    {
        $show = new TvShow();
        $show->name = $item->competitors[0]->name . ' VS ' . $item->competitors[1]->name;
        $show->category_id = $categoryId;
        $show->date_start = date("Y-m-d", strtotime($item->scheduled));
        $show->time_start = date("H:i:s", strtotime($item->scheduled));
        $show->status = 0;
        $show->api_id = explode(":", $item->id)[2];
        $show->created_at = date("Y-m-d");
        $show->updated_at = date("Y-m-d");
        $show->save();
    }

    private function isRowCountById($item, $categoryId, $type) 
    {
        $count = TvShow::find()->where(['api_id' => ($item->id) ? explode(":", $item->id)[2] : $item['id']])->exists();
        if (!$count)
        {
            switch($type){
                case 'movi': {
                    $this->addMovi($item, $categoryId);
                    break;
                }
                case 'tv': {
                    $this->addTv($item, $categoryId);
                    break;
                }
                case 'sport': {
                    $this->addSport($item, $categoryId);
                    break;
                }
            }
            
        }
    }

    public function checkIsset($data, $category, $type = 'movi')
    {
        $showCategories = new TvShowCategories();
        $categoryId =  $showCategories->issetCategory($category);
        $this->isRowCountById($data, $categoryId, $type);
    }
}
