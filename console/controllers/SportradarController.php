<?php
namespace console\controllers;
set_time_limit(0);

use Yii;
use yii\console\Controller;
use \console\models\TvShow;
use \Tmdb\Api,
    \Tmdb\Client;

class SportradarController extends Controller
{
  const API_KEY = 'v2g38s3ckf8asttat2mccsxr';
  private $lang = 'it';
  private $category = 'Sport';

  public function actionSports()
  {
    $model = new TvShow();
    $dates = array(date("Y-" . '04-01', strtotime('-1 year')), date("Y-" . '04-01'), date("Y-" . '04-01', strtotime('+1 year')));
    $this->getDataByDate(date("Y-01-01"), $model);
    foreach($dates as $date){
      $this->getDataByDate($date, $model);
    }
  }
  private function getDataByDate($date, $model){
    if(date_diff(date_create(date("Y-01-01")), date_create($date))->y < 1){
      sleep(1);
      $url = "https://api.sportradar.us/soccer-t3/eu/en/schedules/" . $date ."/results.json?api_key=" . self::API_KEY;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_URL,$url);
      $result=curl_exec($ch);
      curl_close($ch);

      $sports = json_decode($result);
      if($sports->schema !=''){
        foreach($sports->results as $sport){
          $model->checkIsset($sport->sport_event, $this->category, 'sport');
        }
      }
      $date = date("Y-m-d", strtotime(date($date) . '+1 day'));
      var_dump($date);
      $this->getDataByDate($date, $model);
    }
  }
}