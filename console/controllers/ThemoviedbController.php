<?php
namespace console\controllers;
set_time_limit(0);
use Yii;
use yii\console\Controller;
use \console\models\TvShow;
use \Tmdb\Api,
    \Tmdb\Client;

class ThemoviedbController extends Controller
{
  const API_KEY = 'd92587312562c66133deef2d4bc8b176';
  private $client = null;
  private $lang = 'it';
  private $categories = array('Movies', 'Tv');

  public function actionMovies()
  {
    $model = new TvShow();
    $token  = new \Tmdb\ApiToken(self::API_KEY);
		$this->client = new \Tmdb\Client($token);

		/** Get movies */
		$movies = $this->client->getMoviesApi()->getLatest(['language' => $this->lang]);
    $model->checkIsset($movies, $this->categories[0]);

		$this->getUpcoming($model, $this->categories[0]);

		$this->getNowPlaying($model, $this->categories[0]);

		$this->getPopular($model, $this->$ategories[0]);

		$this->getTopRated($model, $this->categories[0]);

		/** Get TV */
		$movies = $this->client->getTvApi()->getLatest(['language' => $this->lang]);
    $model->checkIsset($movies, $this->categories[1], 'tv');

		$this->getPopularTv($model, $this->categories[1]);

		$this->getTopRatedTv($model, $this->categories[1]);

		$this->getOnTheAirTv($model, $this->categories[1]);

		$this->getAiringTodayTv($model, $this->categories[1]);
	}

	/** add parse function */
	private function parseData($model, $data, $category, $cat) {
		if (count($data) > 0){
			foreach($data as $item){
				$model->checkIsset($item, $category, $cat);
			}
		}
	}
	
	private function getPopularTv($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getTvApi()->getPopular(['language' => $this->lang, 'page' => $page]);
			$this->parseData($model, $movies['results'], $category, 'tv');
			if($page == 1){
				var_dump('PopularTv');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getPopularTv($model, $category, $page, $movies['total_pages']);
    }
  }
	private function getTopRatedTv($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getTvApi()->getTopRated(['language' => $this->lang, 'page' => $page]);
      $this->parseData($model, $movies['results'], $category, 'tv');
			if($page == 1){
				var_dump('PopularTv');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getTopRatedTv($model, $category, $page, $movies['total_pages']);
    }
  }
	private function getOnTheAirTv($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getTvApi()->getOnTheAir(['language' => $this->lang, 'page' => $page]);
      $this->parseData($model, $movies['results'], $category, 'tv');
			if($page == 1){
				var_dump('OnTheAirTv');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getOnTheAirTv($model, $category, $page, $movies['total_pages']);
    }
  }
	private function getAiringTodayTv($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getTvApi()->getAiringToday(['language' => $this->lang, 'page' => $page]);
      $this->parseData($model, $movies['results'], $category, 'tv');
			if($page == 1){
				var_dump('AiringTodayTv');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getAiringTodayTv($model, $category, $page, $movies['total_pages']);
    }
  }

  private function getUpcoming($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getMoviesApi()->getUpcoming(['language' => $this->lang, 'page' => $page]);
			$this->parseData($model, $movies['results'], $category, 'movi');
			if($page == 1){
				var_dump('Upcoming');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getUpcoming($model, $category, $page, $movies['total_pages']);
    }
	}
	
	private function getNowPlaying($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getMoviesApi()->getNowPlaying(['language' => $this->lang, 'page' => $page]);
      $this->parseData($model, $movies['results'], $category, 'movi');
			if($page == 1){
				var_dump('NowPlaying');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getNowPlaying($model, $category, $page, $movies['total_pages']);
    }
  }
	private function getPopular($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getMoviesApi()->getPopular(['language' => $this->lang, 'page' => $page]);
      $this->parseData($model, $movies['results'], $category, 'movi');
			if($page == 1){
				var_dump('Popular');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getPopular($model, $category, $page, $movies['total_pages']);
    }
	}
	
	private function getTopRated($model, $category, $page = 1, $limit = 2) {
    if($page <= $limit){
      $movies = $this->client->getMoviesApi()->getTopRated(['language' => $this->lang, 'page' => $page]);
      $this->parseData($model, $movies['results'], $category, 'movi');
			if($page == 1){
				var_dump('TopRated');
				var_dump(' count in category: ' . (int)count($movies['results']) * (int)$movies['total_pages'] );
			}
			$page ++;
			$limit = $movies['total_pages'];
      $this->getTopRated($model, $category, $page, $movies['total_pages']);
    }
	}
}