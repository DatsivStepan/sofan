<?php

use yii\db\Migration,
    common\models\User;

/**
 * Class m180430_181137_add_column_to_user_table
 */
class m180430_181137_add_column_to_user_table extends Migration
{
     /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'user_type', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'user_type');
    }
}
