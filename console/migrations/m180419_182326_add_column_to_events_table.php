<?php

use yii\db\Migration,
    common\models\Events;

/**
 * Class m180419_182326_add_column_to_events_table
 */
class m180419_182326_add_column_to_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Events::tableName(), 'balcony', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Events::tableName(), 'balcony');
    }
}
