<?php

use yii\db\Migration,
    common\models\UserInfo;

/**
 * Class m180424_205000_add_column_to_user_info_table
 */
class m180424_205000_add_column_to_user_info_table extends Migration
{
     /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(UserInfo::tableName(), 'avatar', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(UserInfo::tableName(), 'avatar');
    }
}
