<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message`.
 */
class m180423_165428_create_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('messages', [
            'id' => $this->primaryKey(),
            'dialog_id' => $this->integer()->null(),
            'from_user_id' => $this->integer()->null(),
            'to_user_id' => $this->integer()->null(),
            'message' => $this->text()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('message');
    }
}
