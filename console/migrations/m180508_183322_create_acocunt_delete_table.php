<?php

use yii\db\Migration;

/**
 * Handles the creation of table `acocunt_delete`.
 */
class m180508_183322_create_acocunt_delete_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('acocunt_delete', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'why' => $this->integer()->null(),
            'why_not' => $this->text()->null(),
            'created_at' => $this->date()->null(),
            'updated_at' => $this->date()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('acocunt_delete');
    }
}
