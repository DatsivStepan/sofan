<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_verification`.
 */
class m180502_133238_create_user_verification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_verification', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'object' => $this->string()->null(),
            'type' => $this->integer()->null(),
            'hash' => $this->string()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_verification');
    }
}
