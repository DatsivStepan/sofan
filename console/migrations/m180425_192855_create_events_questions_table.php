<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events_comments`.
 */
class m180425_192855_create_events_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('events_questions', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->null(),
            'user_id' => $this->integer()->null(),
            'question' => $this->text()->null(),
            'answer' => $this->text()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('events_comments');
    }
}
