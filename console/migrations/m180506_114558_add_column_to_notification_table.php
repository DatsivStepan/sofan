<?php

use yii\db\Migration,
    common\models\Notification;

/**
 * Class m180506_114558_add_column_to_notification_table
 */
class m180506_114558_add_column_to_notification_table extends Migration
{
     /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Notification::tableName(), 'parent_object_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Notification::tableName(), 'parent_object_id');
    }
}
