<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message_dialog`.
 */
class m180423_165351_create_message_dialog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('messages_dialog', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->null(),
            'to_user_id' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('message_dialog');
    }
}
