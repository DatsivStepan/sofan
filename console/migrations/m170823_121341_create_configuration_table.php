<?php

use yii\db\Migration;
use yii\db\Schema;

class m170823_121341_create_configuration_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%configuration}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'key' => Schema::TYPE_STRING . '(255) NULL',
            'value' => Schema::TYPE_STRING . '(255) NULL',
            'date_create' => Schema::TYPE_DATETIME,
            'date_update' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        
        $this->batchInsert('{{%configuration}}', ['name', 'key', 'value', 'date_create', 'date_update'], 
            [
                ['email', 'email', 'dativStepan@gmail.com', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['phone number', 'phone', '+38096 66 66 999', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")]
            ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%setting}}');
    }
}
