<?php

use yii\db\Migration,
    common\models\Host;

/**
 * Class m180414_104337_add_column_to_host_table
 */
class m180414_104337_add_column_to_host_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Host::tableName(), 'street_number', $this->string(100)->null());
        $this->addColumn(Host::tableName(), 'street_name', $this->string(100)->null());
        $this->addColumn(Host::tableName(), 'created_at', $this->dateTime()->null());
        $this->addColumn(Host::tableName(), 'updated_at', $this->dateTime()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Host::tableName(), 'street_number');
        $this->dropColumn(Host::tableName(), 'street_name');
        $this->dropColumn(Host::tableName(), 'created_at');
        $this->dropColumn(Host::tableName(), 'updated_at');
    }

}
