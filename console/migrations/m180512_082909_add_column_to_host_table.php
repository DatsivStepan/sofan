<?php

use yii\db\Migration,
    common\models\Host;

/**
 * Class m180512_082909_add_column_to_host_table
 */
class m180512_082909_add_column_to_host_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn(Host::tableName(), 'status', $this->integer()->null());
         $this->addColumn(Host::tableName(), 'why', $this->integer()->null());
         $this->addColumn(Host::tableName(), 'location_pets_exist', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Host::tableName(), 'status');
        $this->dropColumn(Host::tableName(), 'why');
        $this->dropColumn(Host::tableName(), 'location_pets_exist');
    }

}
