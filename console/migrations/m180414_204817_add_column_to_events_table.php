<?php

use yii\db\Migration,
    common\models\Events;

/**
 * Class m180414_204817_add_column_to_events_table
 */
class m180414_204817_add_column_to_events_table extends Migration
{
     /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Events::tableName(), 'tv_show_id', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Events::tableName(), 'tv_show_id');
    }
}
