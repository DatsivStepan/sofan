<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events`.
 */
class m180413_205813_create_events_table extends Migration
{
    public $table = '{{%events}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'date_start' => $this->date()->notNull(),
            'time_start' => $this->time()->notNull(),
            'live' => $this->tinyInteger(1)->null(),
            'name' => $this->string(255)->null(),
            'host_id' => $this->integer()->null(),
            'description' => $this->text()->null(),
            'max_places_count' => $this->integer()->null(),
            'min_places_count' => $this->integer()->null(),
            'registration_end' => $this->integer()->null(),
            'payment_per_person' => $this->integer()->null(),
            'confirm_status' => $this->tinyInteger(1),
            'step' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'wifi' => $this->tinyInteger(1),
            'smoke' => $this->tinyInteger(1),
            'food' => $this->tinyInteger(1),
            'conditioner' => $this->tinyInteger(1),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ], 'ENGINE=InnoDB CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
