<?php

use yii\db\Migration;
use yii\db\Schema;

class m170823_121253_create_pages_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pages}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'content' => Schema::TYPE_TEXT . '(255) NULL',
            'sort' => Schema::TYPE_STRING . '(255) NULL',
            'date_create' => Schema::TYPE_DATETIME,
            'date_update' => Schema::TYPE_DATETIME,
            'slug' => Schema::TYPE_STRING . '(255) NULL',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%pages}}');
    }
}
