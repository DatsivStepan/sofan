<?php

use yii\db\Migration,
    common\models\UserInfo;

/**
 * Class m180502_130250_change_column_in_user_info_table
 */
class m180502_130250_change_column_in_user_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(UserInfo::tableName(), 'phone_pref', $this->string(5)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180502_130250_change_column_in_user_info_table cannot be reverted.\n";

        return false;
    }

}
