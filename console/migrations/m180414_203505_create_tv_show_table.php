<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tv_show`.
 */
class m180414_203505_create_tv_show_table extends Migration
{
    public $table = '{{%tv_show}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'category_id' => $this->integer()->null(),
            'date_start' => $this->date()->null(),
            'time_start' => $this->time()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
