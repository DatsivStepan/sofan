<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m180421_152711_create_feedback_table extends Migration
{
    public $table = '{{%feedback}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'location' => $this->float()->null(),
            'hoster' => $this->float()->null(),
            'organization' => $this->float()->null(),
            'hospitality' => $this->float()->null(),
            'cleaning' => $this->float()->null(),
            'fun' => $this->float()->null(),
            'total' => $this->float()->null(),
            'status' => $this->integer()->null(),
            'note' => $this->text()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
