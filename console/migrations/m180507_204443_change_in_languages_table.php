<?php

use yii\db\Migration,
    common\models\Lang;

/**
 * Class m180507_204443_change_in_languages_table
 */
class m180507_204443_change_in_languages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model1 = Lang::findOne(1);
        $model1->default = 0;
        $model1->save();
        
        $model = Lang::findOne(2);
        $model->url = 'it';
        $model->local = 'it-IT';
        $model->name = 'Italiano';
        $model->default = 1;
        $model->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180507_204443_change_in_languages_table cannot be reverted.\n";

        return false;
    }
}
