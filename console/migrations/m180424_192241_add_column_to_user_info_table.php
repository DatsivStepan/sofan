<?php

use yii\db\Migration,
    common\models\UserInfo;

/**
 * Class m180424_192241_add_column_to_user_info_table
 */
class m180424_192241_add_column_to_user_info_table extends Migration
{
     /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(UserInfo::tableName(), 'gender', $this->integer()->null());
        $this->addColumn(UserInfo::tableName(), 'phone_pref', $this->integer()->null());
        $this->addColumn(UserInfo::tableName(), 'phone', $this->string()->null());
        $this->addColumn(UserInfo::tableName(), 'lang', $this->integer()->null());
        $this->addColumn(UserInfo::tableName(), 'about', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(UserInfo::tableName(), 'gender');
        $this->dropColumn(UserInfo::tableName(), 'phone_pref');
        $this->dropColumn(UserInfo::tableName(), 'phone');
        $this->dropColumn(UserInfo::tableName(), 'lang');
        $this->dropColumn(UserInfo::tableName(), 'about');
    }
}
