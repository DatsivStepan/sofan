<?php

use yii\db\Migration;

/**
 * Class m180405_203740_image
 */
class m180405_203740_image extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(11),
            'path' => $this->string(255)->notNull(),
            'relation_id' => $this->integer(11),
            'name' => $this->string(255)->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%image}}');
    }
}
