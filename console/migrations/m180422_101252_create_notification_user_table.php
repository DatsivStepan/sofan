<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification_user`.
 */
class m180422_101252_create_notification_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification_user', [
            'id' => $this->primaryKey(),
            'notification_id' => $this->integer()->null(),
            'user_id' => $this->integer()->null(),
            'status' => $this->tinyInteger(1)->null(),
            'note' => $this->text()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notification_user');
    }
}
