<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification`.
 */
class m180422_101226_create_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->null(),
            'object_id' => $this->integer()->null(),
            'user_id' => $this->integer()->null(),
            'note' => $this->text()->null(),
            'status' => $this->tinyInteger(1)->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notification');
    }
}
