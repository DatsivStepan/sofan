<?php

use yii\db\Migration,
    common\models\Events;

/**
 * Class m180418_184522_add_column_for_events
 */
class m180418_184522_add_column_for_events extends Migration
{
     /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Events::tableName(), 'why_delete', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Events::tableName(), 'why_delete');
    }
}
