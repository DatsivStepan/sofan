<?php

use yii\db\Migration;

/**
 * Class m180402_225128_host
 */
class m180402_225128_host extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%host}}', [
            'id' => $this->primaryKey(11),
            'title' => $this->string(255)->notNull(),
            'country' => $this->string(2)->notNull(),
            'city' => $this->string(255)->notNull(),
            'user_id' => $this->integer(11),
            'place_id' => $this->string(255),
            'lat' => $this->decimal(10, 7),
            'lng' => $this->decimal(10, 7),
            'location_type' => $this->tinyInteger(1),
            'location_seats' => $this->tinyInteger(1),
            'location_size' => $this->tinyInteger(1),
            'location_pets' => $this->tinyInteger(1),
            'device_type' => $this->tinyInteger(1),
            'device_size' => $this->tinyInteger(1),
            'device_resolution' => $this->tinyInteger(1),
            'wifi' => $this->tinyInteger(1),
            'smoke' => $this->tinyInteger(1),
            'food' => $this->tinyInteger(1),
            'conditioner' => $this->tinyInteger(1)
        ], $tableOptions);

        $this->addForeignKey('fk-host-user_id', '{{%host}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-post-user_id', '{{%host}}');
        $this->dropTable('{{%host}}');
    }
}
