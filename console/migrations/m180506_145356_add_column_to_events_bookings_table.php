<?php

use yii\db\Migration,
    common\models\EventsBookings;

/**
 * Class m180506_145356_add_column_to_events_bookings_table
 */
class m180506_145356_add_column_to_events_bookings_table extends Migration
{
     /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(EventsBookings::tableName(), 'why', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(EventsBookings::tableName(), 'why');
    }
}
