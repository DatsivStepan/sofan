<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events_booking_item`.
 */
class m180416_173101_create_events_booking_item_table extends Migration
{
    public $table = '{{%events_booking_item}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'event_booking_id' => $this->integer()->null(),
            'name' => $this->string(255)->null(),
            'gender' => $this->tinyInteger(1)->null(),
            'age' => $this->integer()->null(),
            'status' => $this->tinyInteger(1)->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
