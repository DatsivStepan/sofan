<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events_images`.
 */
class m180413_214529_create_events_images_table extends Migration
{
    public $table = '{{%events_images}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'path' => $this->string(255)->notNull(),
            'event_id' => $this->integer(11),
            'name' => $this->string(255)->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
