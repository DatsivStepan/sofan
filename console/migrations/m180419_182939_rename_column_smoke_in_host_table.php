<?php

use yii\db\Migration,
    common\models\Host;

/**
 * Class m180419_182939_rename_column_smoke_in_host_table
 */
class m180419_182939_rename_column_smoke_in_host_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn(Host::tableName(), 'smoke', 'balcony');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180419_182939_rename_column_smoke_in_host_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180419_182939_rename_column_smoke_in_host_table cannot be reverted.\n";

        return false;
    }
    */
}
