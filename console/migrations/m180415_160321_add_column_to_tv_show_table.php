<?php

use yii\db\Migration,
    common\models\TvShow;

/**
 * Class m180415_160321_add_column_to_tv_show_table
 */
class m180415_160321_add_column_to_tv_show_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(TvShow::tableName(), 'api_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(TvShow::tableName(), 'api_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180415_160321_add_column_to_tv_show_table cannot be reverted.\n";

        return false;
    }
    */
}
