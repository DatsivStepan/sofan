<?php
/**
 * @author DatsivStepan
 */

namespace common\components;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Lang;

class AdminGetParameter
{
    
    public function getLanguageArray()
    {
        $modelLanguage = Lang::find()->all();
        $arrayLanguage = ArrayHelper::map($modelLanguage, 'local', 'name');
        return $arrayLanguage;
    }
}