<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="password-reset">
    <p>Hello dear friend <?= Html::encode($model ? $model->getUsernameWithSurname() : '') ?>, welcome you to register on our site - SOFAN</p>
</div>
