<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 */
class Country extends \yii\db\ActiveRecord
{
    public $phonePre;
    const ITALY_CODE = 'IT';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phonecode', 'iso3'], 'integer', 'max' => 2],
            [['iso'], 'string', 'max' => 2],
            [['name', 'nicename', 'numcode'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iso' => 'Code',
            'name' => 'Name',
        ];
    }

    public static function getPhonePreList()
    {
        $models = self::find()
                ->select(['iso', 'phonePre' => 'CONCAT("+", phonecode, " (", name, ")")'])
                ->all();

        return ArrayHelper::map($models, 'iso', 'phonePre');
    }

    public static function getList()
    {
        $models = self::find()->all();

        return ArrayHelper::map($models, 'iso', 'name');
    }
}
