<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%events_bookings}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $event_id
 * @property integer $status
 * @property integer $why
 * @property string $created_at
 * @property string $updated_at
 */
class EventsBookings extends \yii\db\ActiveRecord
{
    const STATUS_ON_CONFIRM = 1;
    const STATUS_CONFIRM = 2;
    const STATUS_REJECTED = 3;
    const STATUS_NOT_ACTIVE = 4;

    public static $reasonDeletion = [
        1 => 'reason for deletion 1',
        2 => 'reason for deletion 2',
        3 => 'reason for deletion 3',
        4 => 'reason for deletion 4',
        5 => 'reason for deletion 5',
    ];

    public $items;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%events_bookings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id', 'event_id', 'status', 'why'
                ], 
                'integer'
            ],
            [
                [
                    'created_at', 'updated_at', 'items'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @return bool
     */
    public function saveWithLog($type = null)
    {
        if ($this->save()) {
            if ($type == self::STATUS_CONFIRM) {
                if ($this->userModel && ($email = $this->userModel->email)) {
                    Notification::saveNotification(
                        Notification::TYPE_EVENT_BOOKING_CONFIRM,
                        [$this->userModel->id => $email],
                        $this->event_id,
                        $this->id
                    );
                }
            }elseif ($type == self::STATUS_REJECTED) {
                if ($this->userModel && ($email = $this->userModel->email)) {
                    Notification::saveNotification(
                        Notification::TYPE_EVENT_BOOKING_REJECT,
                        [$this->userModel->id => $email],
                        $this->event_id,
                        $this->id
                    );
                }
            }elseif ($type == self::STATUS_NOT_ACTIVE) {
                if ($this->eventsModel && $this->eventsModel->userModel && ($email = $this->eventsModel->userModel->email)) {
                    Notification::saveNotification(
                        Notification::TYPE_EVENT_BOOKING_DELETE,
                        [$this->eventsModel->userModel->id => $email],
                        $this->event_id,
                        $this->id
                    );
                }
            }

            return true;
        }
    }
    
    /**
     * @return string
     */
    public function getDateCreate($format = 'd.m.Y H:i')
    {
        return date($format, strtotime($this->created_at));
    }

    /**
     * @return string
     */
    public function saveWithItem($post, $confirm_status)
    {
        if ($confirm_status == Events::STATUS_CONFIRM_NO) {
            $this->status = self::STATUS_CONFIRM;
        } else {
            $this->status = self::STATUS_ON_CONFIRM;
        }
        if ($this->save()) {
            foreach ($post['name'] as $key => $data) {
                $model = new EventsBookingItem;
                $model->event_booking_id = $this->id;;
                $model->name = $data;
                $model->age = array_key_exists($key, $post['age']) ? $post['age'][$key] : null;
                $model->gender = array_key_exists($key, $post['gender']) ? $post['gender'][$key] : null;
                $model->save();
            }
            
            if ($this->eventsModel && $this->eventsModel->userModel && ($email = $this->eventsModel->userModel->email)) {
                Notification::saveNotification(
                    Notification::TYPE_EVENT_BOOKING,
                    [$this->eventsModel->userModel->id => $email],
                    $this->event_id,
                    $this->id
                );
            }
            if ($this->userModel && ($email = $this->userModel->email)) {
                Notification::saveNotification(
                    Notification::TYPE_EVENT_BOOKING_ADD,
                    [$this->userModel->id => $email],
                    $this->event_id,
                    $this->id
                );
            }
            
            return true;
        }
        
        return false;
    }   
    
    /**
     * @return string
     */
    public function getName()
    {
        $count = $this->getItemCount();
        return $count . ($count == 1 ? ' posto ' : ' posti ' ) . $this->getStatus();
    }
    
    /**
     * @return string
     */
    public function getStatus()
    {
        $status = '';
        switch ($this->status) {
            case self::STATUS_ON_CONFIRM:
                $status = 'da confirmare';
                break;
            case self::STATUS_CONFIRM:
                $status = 'confermati';
                break;
            case self::STATUS_REJECTED:
                $status = 'Cancellati';
                break;
        }
        return $status;
    }
    
    /**
     * @return string
     */
    public function getItemCount()
    {
        return EventsBookingItem::find()->where(['event_booking_id' => $this->id])->count();
    }
    
    /**
     * @return string
     */
    public static function getEventBookedCount($eventId)
    {
        return EventsBookingItem::find()
                ->alias('ebi')
                ->joinWith(['eventsBookingsModel eb'])
                ->where(['eb.event_id' => $eventId])->count();

    }
    
    /**
     * @return string
     */
    public static function getUserBookedCount($eventId, $userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return self::find()
                ->where([
                    'event_id' => $eventId,
                    'user_id' => $userId,
                ])
                ->count();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsModel()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItemsModel()
    {
        return $this->hasMany(EventsBookingItem::className(), ['event_booking_id' => 'id']);
    }

}
