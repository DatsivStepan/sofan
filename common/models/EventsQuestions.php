<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%events_questions}}".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $user_id
 * @property string $question
 * @property string $answer
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class EventsQuestions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%events_questions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id', 'status'], 'integer'],
            [['question', 'answer'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @return string
     */
    public function saveWithNotification()
    {
        if ($this->save()) {
            if ($this->eventsModel && $this->eventsModel->userModel && ($email = $this->eventsModel->userModel->email)) {
                Notification::saveNotification(
                    Notification::TYPE_EVENT_QUESTION_ADDED,
                    [$this->eventsModel->userModel->id => $email],
                    $this->event_id,
                    $this->id
                );
            }
        }
    }
    
    /**
     * @return string
     */
    public function getDateUpdate($format = 'd.m.Y H:i')
    {
        return date($format, strtotime($this->updated_at));
    }
    
    /**
     * @return string
     */
    public function getDateCreate($format = 'd.m.Y H:i')
    {
        return date($format, strtotime($this->created_at));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsModel()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
