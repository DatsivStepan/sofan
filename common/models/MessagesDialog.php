<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%messages_dialog}}".
 *
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class MessagesDialog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%messages_dialog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_user_id' => Yii::t('app', 'From User ID'),
            'to_user_id' => Yii::t('app', 'To User ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
            $this->updated_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function getUserData()
    {
        $user = null;
        if ($this->from_user_id != Yii::$app->user->id) {
            $user = User::findOne($this->from_user_id);
        }

        if ($this->to_user_id != Yii::$app->user->id) {
            $user = User::findOne($this->to_user_id);
        }
        return $user;
    }
    
    public function setReadAll($userId)
    {
        return Messages::updateAll(['status' => Messages::STATUS_READ], ['dialog_id' => $this->id, 'to_user_id' => $userId]);
    }
    
    public function lastMessage($userId)
    {
        return Messages::find()
                ->where([
                    'to_user_id' => $userId,
                    'dialog_id' => $this->id
                ])
                ->orderBy(['id' => SORT_DESC])
                ->one();
    }

    /**
     * @param $userId
     * @return \yii\db\ActiveQuery
     */
    public function getNotReadCount($userId)
    {
        return Messages::find()
                ->where(['dialog_id' => $this->id, 'to_user_id' => $userId])
                ->andWhere([
                        'OR',
                        ['status' => null],
                        ['status' => 0]
                    ])
                ->count();
    }

    /**
     * @param $userId
     * @return \yii\db\ActiveQuery
     */
    public static function getNotReadByUser($userId, $limit=3)
    {
        $query = self::find()
                ->alias('md')
                ->joinWith(['messagesModel m'])
                ->where([
                    'AND',
                    ['md.to_user_id' => $userId],
                    [
                        'OR',
                        ['m.to_user_id' => $userId, 'm.status' => null],
                        ['m.to_user_id' => $userId, 'm.status' => 0]
                    ],
                ])
                ->orWhere([
                    'AND',
                    ['md.from_user_id' => $userId],
                    [
                        'OR',
                        ['m.to_user_id' => $userId, 'm.status' => null],
                        ['m.to_user_id' => $userId, 'm.status' => 0]
                    ],
                ])
                ->limit($limit)
                ->distinct();

        return $query->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessagesModel()
    {
        return $this->hasMany(Messages::className(), ['dialog_id' => 'id']);
    }
    
}
