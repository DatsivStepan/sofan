<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%notification_user}}".
 *
 * @property integer $id
 * @property integer $notification_id
 * @property integer $user_id
 * @property integer $status
 * @property string $note
 * @property string $created_at
 * @property string $updated_at
 */
class NotificationUser extends \yii\db\ActiveRecord
{
    const STATUS_READ = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'user_id', 'status'], 'integer'],
            [['note'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'notification_id' => Yii::t('app', 'Notification ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'note' => Yii::t('app', 'Note'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getLink()
    {
        return '/profile/notifications/view/' . $this->id;
    }

    /**
     * @return string
     */
    public function getDateCreate($format = 'd.m.Y H:i')
    {
        return date($format, strtotime($this->created_at));
    }
    
    /**
     * @inheritdoc
     */
    public function getShortNote()
    {
        return substr(strip_tags($this->note), 0, 100) . '...';
    }
    
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @param integer $userId
     * @param integer $limit
     * @return \yii\db\ActiveQuery
     */
    public static function getNotReadByUser($userId, $limit=3)
    {
        $query = self::find()
                ->where(['user_id' => $userId])
                ->andWhere([
                    'OR',
                    ['status' => null],
                    ['status' => 0],
                ])
                ->orderBy(['created_at' => SORT_DESC])
                ->limit($limit);

        return $query->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationModel()
    {
        return $this->hasOne(Notification::className(), ['id' => 'notification_id']);
    }
}
