<?php

namespace common\models;

use Yii,
    Authy\AuthyApi;

// $response = $authy_api->phoneVerificationStart('+380961414994', '380', 'sms');

class VerifyPhone
{
    const AUTH_API_KEY = 'SR9d99YRiBMcY8bx1a7zdnQXW9Z9Zv8o';

    /**
     * @param $phonePref
     * @param $phone
     * @return bool
     */
    public static function verificationStart($phonePref, $phone)
    {
        $authy_api = new AuthyApi(self::AUTH_API_KEY);
        $response = $authy_api->phoneVerificationStart($phone, $phonePref, 'sms');

        return $response->ok();
    }

    /**
     * @param $phonePref
     * @param $phone
     * @param $code
     * @return bool
     */
    public static function verificationCheck($phonePref, $phone, $code)
    {
        $authy_api = new AuthyApi(self::AUTH_API_KEY);
        $response = $authy_api->phoneVerificationCheck($phone, $phonePref, $code);

        return $response->ok();
    }

}