<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%events_booking_item}}".
 *
 * @property integer $id
 * @property integer $event_booking_id
 * @property string $name
 * @property integer $gender
 * @property integer $age
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class EventsBookingItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%events_booking_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_booking_id', 'age'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['gender', 'status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_booking_id' => Yii::t('app', 'Event Booking ID'),
            'name' => Yii::t('app', 'Name'),
            'gender' => Yii::t('app', 'Gender'),
            'age' => Yii::t('app', 'Age'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsBookingsModel()
    {
        return $this->hasOne(EventsBookings::className(), ['id' => 'event_booking_id']);
    }
}
