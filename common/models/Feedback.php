<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $event_id
 * @property integer $location
 * @property integer $hoster
 * @property integer $organization
 * @property integer $hospitality
 * @property integer $cleaning
 * @property integer $fun
 * @property integer $total
 * @property integer $status
 * @property string $note
 * @property string $created_at
 * @property string $updated_at
 */
class Feedback extends \yii\db\ActiveRecord
{
    public $locSum;
    public $hostSum;
    public $orgSum;
    public $hospSum;
    public $clearSum;
    public $funSum;

    public $sum;
    public $count;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id'], 'required'],
            [
                [
                    'user_id', 'event_id', 'location', 'hoster', 'organization',
                    'hospitality', 'cleaning', 'fun', 'total', 'status'
                ],
                'integer'
            ],
            [['created_at', 'updated_at', 'note'], 'safe'],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'location' => Yii::t('app', 'Location'),
            'hoster' => Yii::t('app', 'Hoster'),
            'organization' => Yii::t('app', 'Organization'),
            'hospitality' => Yii::t('app', 'Hospitality'),
            'cleaning' => Yii::t('app', 'Cleaning'),
            'fun' => Yii::t('app', 'Fun'),
            'total' => Yii::t('app', 'Total'),
            'status' => Yii::t('app', 'Status'),
            'note' => Yii::t('app', 'Note'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return string
     */
    public function saveWithNotification()
    {
        if ($this->save()) {
            if ($this->eventsModel && $this->eventsModel->userModel && ($email = $this->eventsModel->userModel->email)) {
                Notification::saveNotification(
                    Notification::TYPE_EVENT_FEEDBACK,
                    [$this->eventsModel->userModel->id => $email],
                    $this->event_id,
                    $this->id
                );
            }
        }
    }
    
    /**
     * @return string
     */
    public function getStarRatingCount()
    {
        return ($this->location + $this->hoster + $this->organization + 
                    $this->hospitality + $this->cleaning + $this->fun) / 6;
    }
    
    /**
     * @return string
     */
    public function getDateCreate($format = 'd.m.Y H:i')
    {
        return date($format, strtotime($this->created_at));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventModel()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }
}
