<?php
namespace common\models;

use common\models\query\UserQuery;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $description
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public $profile;

    public $password;
    public $old_password;
    public $password_repeat;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['description', 'string'],
            
            [['old_password', 'password', 'password_repeat'], 'required', 'on' => 'password_change'],
            [['password', 'password_repeat'], 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    public function scenarios()
    {
        return [
            'password_change' => ['password', 'password_repeat', 'old_password'],
            'default' => ['email', 'description'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()->has('user-'.$id)) {
            return new self(Yii::$app->getSession()->get('user-'.$id));
        }else {
            return self::findOne($id);
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->joinWith('tokens t')
            ->andWhere(['t.token' => $token])
            ->andWhere(['>', 't.expired_at', time()])
            ->one();
    }
    
     public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            'username' => $service->getAttribute('name'),
            'authKey' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return integer
     */
    public function getHostCount()
    {
        return Host::find()
                ->where([
                    'user_id' => $this->id,
                    'status' => null
                ])
                ->count();
    }

    /**
     * @return integer
     */
    public function getEventNotFinished()
    {
        return Events::find()->where([
            'user_id' => Yii::$app->user->id,
            'status' => Events::STATUS_START_CREATE,
        ])
        ->andWhere(['<', 'step', 4])
        ->all();
    }
    
    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->userInfoModel ? $this->userInfoModel->getAvatar() : '/images/user.png';
    }

    /**
     * @return string
     */
    public function getAllDialogs()
    {
        return MessagesDialog::find()
                ->where([
                    'OR',
                    ['from_user_id' => Yii::$app->user->id],
                    ['to_user_id' => Yii::$app->user->id]
                ])
                ->orderBy(['updated_at' => SORT_DESC])
                ->all();
        
    }

    /**
     * @return string
     */
    public function getEventsCount()
    {
        return Events::find()->where(['user_id' => $this->id])->count();
    }

    public function getLink()
    {
        return '/public-profile/' . $this->id;
    }
    
    public function getAge()
    {
        return $this->userInfoModel ? 
                ($this->userInfoModel->getAge()) : '';
    }
    
    /**
     * @return string
     */
    public function getUserFeedback($limit = 100)
    {
        $eventsId = ArrayHelper::getColumn(Events::find()->where(['user_id' => $this->id])->all(), 'id');
        $feedback = null;
        if ($eventsId) {
            $feedback = Feedback::find()
                ->alias('f')
                ->where(['event_id' => $eventsId])
                ->orderBy(['id' => DESC])
                ->limit($limit)
                ->all();
        }

        return $feedback;
    }
    
    /**
     * @return string
     */
    public function getFeedbackStarts()
    {
        $eventsId = ArrayHelper::getColumn(Events::find()->where(['user_id' => $this->id])->all(), 'id');
        $result = [
            'location' => 0,
            'hoster' => 0,
            'organization' => 0,
            'hospitality' => 0,
            'cleaning' => 0,
            'fun' => 0,
        ];

        $model = null;
        if ($eventsId) {
            $model = Feedback::find()
                ->alias('f')
                ->select([
                    'locSum' => '(sum(f.location) / count(f.id))',
                    'hostSum' => '(sum(f.hoster) / count(f.id))',
                    'orgSum' => '(sum(f.organization) / count(f.id))',
                    'hospSum' => '(sum(f.hospitality) / count(f.id))',
                    'clearSum' => '(sum(f.cleaning) / count(f.id))',
                    'funSum' => '(sum(f.fun) / count(f.id))',
                ])
                ->where(['event_id' => $eventsId])
                ->one();
            $result = [
                'location' => $model->locSum,
                'hoster' => $model->hostSum,
                'organization' => $model->orgSum,
                'hospitality' => $model->hospSum,
                'cleaning' => $model->clearSum,
                'fun' => $model->funSum,
            ];
        }

        return $result;
        
    }
    
    /**
     * @return string
     */
    public function getStarRatingCount()
    {
        $eventsId = ArrayHelper::getColumn(Events::find()->where(['user_id' => $this->id])->all(), 'id');
        $model = null;
        if ($eventsId) {
            $model = Feedback::find()
                    ->alias('f')
                    ->select([
                        'sum' => '(((sum(f.location) / count(f.id)) + (sum(f.hoster) / count(f.id)) + (sum(f.organization) / count(f.id)) + (sum(f.hospitality) / count(f.id)) + (sum(f.cleaning) / count(f.id)) + (sum(f.fun) / count(f.id))) / 6)',
                        'count' => 'count(f.id)'
                    ])
                    ->where(['event_id' => $eventsId])
                    ->one();
        }
        $res = ['count' => 0, 'sum' => 0];
        if ($model) {
            $res = [
                'sum' => $model->sum,
                'count' => $model->count
            ];
        }
        return $res;
    }

    /**
     * @return string
     */
    public function getNotReadCount()
    {
        return (int)$this->getNotificationsCount() + (int)$this->getMessagesCount();
    }

    /**
     * @return string
     */
    public function getNotificationsCount()
    {
        return NotificationUser::find()
                ->where(['user_id' => $this->id])
                ->andWhere([
                    'OR',
                    ['status' => null],
                    ['status' => 0],
                ])
                ->count();
    }

    /**
     * @return string
     */
    public function getMessagesCount()
    {
        return Messages::find()
                ->where(['to_user_id' => $this->id])
                ->andWhere([
                    'OR',
                    ['status' => null],
                    ['status' => 0],
                ])
                ->count();
    }

    /**
     * @return string
     */
    public function getDateCreate($format = 'd.m.Y H:i')
    {
        return $this->created_at ? date($format, $this->created_at) : '';
    }
    
    /**
     * @return string
     */
    public function getUsernameWithSurname($full = null)
    {
        return $this->userInfoModel ? 
                ($this->userInfoModel->getUsernameWithSurname($full)) : $this->email;
    }

    /**
     * @return string
     */
    public function getUserVerificationStatus($type)
    {
        $object = null;
        switch ($type) {
            case UserVerification::TYPE_EMAIL:
                $object = $this->email;
                break;
            case UserVerification::TYPE_PHONE:
                $object = $this->getPhone();
                break;
            case UserVerification::TYPE_FACEBOOK:
                $object = $this->email;
                break;

            default:
                break;
        }
        return UserVerification::getVerificationStatus($type, $object, $this->id);
    }

    /**
     * @return string
     */
    public function checkDeleteAccount()
    {
        return $this->accountDeleteModel ? true : false;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->userInfoModel ? 
                ($this->userInfoModel->getPhone()) : null;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountDeleteModel()
    {
        return $this->hasOne(AccountDelete::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfoModel()
    {
        return $this->hasOne(UserInfo::className(), ['user_id' => 'id']);
    }

    public function fields()
    {
        return [
            'id' => 'id',
            'username' => 'username',
            'email' => 'email',
            'description' => 'description',
        ];
    }
}
