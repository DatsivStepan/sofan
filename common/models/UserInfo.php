<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_info}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $lang
 * @property integer $phone_pref
 * @property integer $gender
 * @property string $name
 * @property string $avatar
 * @property string $phone
 * @property string $about
 * @property string $surname
 * @property string $birthday
 * @property string $created_at
 * @property string $updated_at
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'gender', 'lang'], 'integer'],
            [['birthday', 'created_at', 'updated_at', 'about'], 'safe'],
            [['name', 'surname', 'phone', 'avatar'], 'string', 'max' => 255],
            [['phone_pref'], 'string', 'max' => 5],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'user_id' => Yii::t('app', 'User'),
            'surname' => Yii::t('app', 'Surname'),
            'birthday' => Yii::t('app', 'Birthday'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar ? $this->avatar : '/images/user.png';
    }

    /**
     * @return string
     */
    public function getAge()
    {
        $age = '';
        if ($this->birthday) {
            $interval = date_diff(date_create(date('Y-m-d')), date_create($this->birthday));
            $age = $interval->y;
        }
        return $age;
    }

    /**
     * @return string
     */
    public function getPhonePref()
    {
        return $this->phonePrefModel ? $this->phonePrefModel->phonecode : null;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return ($this->getPhonePref() ? '+' . $this->getPhonePref() : '') .  $this->phone;
    }

    /**
     * @return string
     */
    public function getUsernameWithSurname($full=null)
    {
        return $this->name || $this->surname ?  
                $this->name . ' ' .  ($this->surname ? (strtoupper($this->surname[0])) . '.': '') : 
            '';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhonePrefModel()
    {
        return $this->hasOne(Country::className(), ['iso' => 'phone_pref']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
}
