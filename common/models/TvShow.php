<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tv_show}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_start
 * @property string $time_start
 * @property integer $status
 * @property integer $category_id
 * @property string $created_at
 * @property string $updated_at
 */
class TvShow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tv_show}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['date_start', 'time_start', 'created_at', 'updated_at'], 'safe'],
            [['status', 'category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'category_id' => Yii::t('app', 'Category'),
            'date_start' => Yii::t('app', 'Date Start'),
            'time_start' => Yii::t('app', 'Time Start'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTvShowCategoriesModel()
    {
        return $this->hasOne(TvShowCategories::className(), ['id' => 'category_id']);
    }
}
