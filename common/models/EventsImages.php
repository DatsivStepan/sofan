<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%events_images}}".
 *
 * @property integer $id
 * @property string $path
 * @property integer $event_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class EventsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%events_images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path'], 'required'],
            [['event_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['path', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Path'),
            'event_id' => Yii::t('app', 'Event ID'),
            'name' => Yii::t('app', 'Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
