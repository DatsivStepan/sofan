<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_verification}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $hash
 * @property string $object
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class UserVerification extends \yii\db\ActiveRecord
{
    const TYPE_EMAIL = 1;
    const TYPE_PHONE = 2;
    const TYPE_FACEBOOK = 3;
    
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_NOT = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_verification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['hash', 'object'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'hash' => Yii::t('app', 'Hash'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public static function saveData($type, $object, $userId, $status = self::STATUS_ACTIVE, $hash = '')
    {
        $model = new self;
        $model->user_id = $userId;
        $model->object = $object;
        $model->type = $type;
        $model->hash = $hash;
        $model->status = $status;
        return $model->save();
    }
    
    public static function getVerificationStatus($type, $object, $userId)
    {
        $model =  self::find()
            ->where([
                'user_id' => $userId,
                'object' => $object,
                'type' => $type,
            ])->one();
        
        return $model ? ($model->status == self::STATUS_ACTIVE ? self::STATUS_ACTIVE : self::STATUS_NOT_ACTIVE) : self::STATUS_NOT;
    }
    
}
