<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "host".
 *
 * @property integer $id
 * @property string $title
 * @property string $country
 * @property string $city
 * @property integer $user_id
 * @property string $lat
 * @property string $lng
 * @property string $province
 * @property integer $status
 * @property integer $why
 * @property integer $location_type
 * @property integer $location_seats
 * @property integer $location_size
 * @property integer $location_pets
 * @property integer $device_type
 * @property integer $device_size
 * @property integer $device_resolution
 * @property integer $wifi
 * @property integer $balcony
 * @property integer $food
 * @property integer $conditioner
 * @property string $street_number
 * @property string $street_name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class Host extends \yii\db\ActiveRecord
{
    const SCENARIO_ADDRESS = 'address';
    const SCENARIO_LOCATION = 'location';
    const SCENARIO_DEVICE = 'device';
    const SCENARIO_COMFORT = 'comfort';
    const IMAGE_UPLOAD_PATH = 'host';
    const STATUS_DELETE = 2;

    public static $locationSizeArray = [
        1 => 'meno di 10mq', 2 => '10mq - 20mq', 3 => '20mq - 30mq', 4 => '30mq - 40mq', 
        5 => '40mq - 50mq', 6 => '40mq - 50mq', 7 => 'più di 10 m2'
    ];

    public static $animalsArray = [
        1 => 'cane',
        2 => 'gatto',
        3 => 'rettili',
        4 => 'altro ',
    ];

    public static $deviceSizeArray = [
        1 => '12"',
        2 => '30"',
        3 => '51"',
    ];

    public static $reasonDeletion = [
        1 => 'reason for deletion 1',
        2 => 'reason for deletion 2',
        3 => 'reason for deletion 3',
        4 => 'reason for deletion 4',
        5 => 'reason for deletion 5',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'host';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'country'], 'required'],
            [['lat', 'lng'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [
                [
                    'street_name', 'street_number', 'province', 'zip_code'
                ], 'string', 'max' => 100
            ],
            [['country', 'city', 'title'], 'string', 'max' => 255],
            [
                [
                    'user_id', 'status', 'why',
                    'location_type', 'location_seats', 'location_pets_exist', 'location_size', 'location_pets',
                    'device_type', 'device_size', 'device_resolution', 'wifi', 'balcony', 'food', 'conditioner'
                ],
                'integer'
            ],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'country' => 'Country',
            'city' => 'City',
            'user_id' => 'User ID',
            'place_id' => 'Place ID',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'location_type' => 'Location Type',
            'location_seats' => 'Location Seats',
            'location_size' => 'Location Size',
            'location_pets' => 'Location Pets',
            'device_type' => 'Device Type',
            'device_size' => 'Device Size',
            'device_resolution' => 'Device Resolution',
            'wifi' => 'Wifi',
            'balcony' => 'balcony',
            'food' => 'Food',
            'conditioner' => 'Conditioner',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function scenarios()
    {
        return array_merge([
            'default' => [
                'status', 'title', 'country', 'city', 'user_id', 'zip_code',
                'place_id', 'lat', 'lng', 'location_type', 'location_seats', 'location_pets_exist', 'location_size', 'location_pets',
                'device_type', 'device_size', 'device_resolution', 'wifi', 'balcony', 'food', 'conditioner',
            ],
            'delete' => [
                'status', 'why'
            ]
        ], parent::scenarios());
    }
    
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return string|bool
     */
    public function getAnimals()
    {
        return array_key_exists($this->location_pets, self::$animalsArray) ? 
                    self::$animalsArray[$this->location_pets] : null;
    }

    /**
     * @return string|bool
     */
    public function getDeviceSize()
    {
        return array_key_exists($this->device_size, self::$deviceSizeArray) ? 
                    self::$deviceSizeArray[$this->device_size] : null;
    }

    /**
     * @return string|bool
     */
    public function getDefaultImage()
    {
        $image = Image::findOne(['relation_id' => $tihs->id]);
        return $image ? '/' . $image->path : '/images/default_avatar.png';
    }
    
    /**
     * @return string
     */
    public function getStarRatingCount()
    {
        $eventsId = ArrayHelper::getColumn(Events::find()->where(['host_id' => $this->id])->all(), 'id');
        $model = null;
        if ($eventsId) {
            $model = Feedback::find()
                    ->alias('f')
                    ->select([
                        'sum' => '(((sum(f.location) / count(f.id)) + (sum(f.hoster) / count(f.id)) + (sum(f.organization) / count(f.id)) + (sum(f.hospitality) / count(f.id)) + (sum(f.cleaning) / count(f.id)) + (sum(f.fun) / count(f.id))) / 6)',
                        'count' => 'count(f.id)'
                    ])
                    ->where(['event_id' => $eventsId])
                    ->one();
        }
        $res = ['count' => 0, 'sum' => 0];
        if ($model) {
            $res = [
                'sum' => $model->sum,
                'count' => $model->count
            ];
        }
        return $res;
    }

    /**
     * @return string|bool
     */
    public function getSize()
    {
        return array_key_exists($this->location_size, self::$locationSizeArray) ? 
                    self::$locationSizeArray[$this->location_size] : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAll($userId = null)
    {
        $query = self::find()
                ->where(['status' => null]);
        if ($userId) {
            $query->andWhere(['user_id' => $userId]);
        }

        return $query->all();
    }

    /**
     * @return string
     */
    public function deleteWithLog()
    {
        if ($this->save()) {
            Events::deleteAll(['host_id' => $this->id]);
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getEventsModel()
    {
        return $this->hasMany(Events::className(), ['host_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->city . ', ' . $this->street_name . ' ' . $this->street_number;
    }
    
    /**
     * @return array
     */
    public static function getAllInArrayMap($userId = null)
    {
        return ArrayHelper::map(self::getAll($userId), 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['relation_id' => 'id', 'path' => self::IMAGE_UPLOAD_PATH]);
    }
}
