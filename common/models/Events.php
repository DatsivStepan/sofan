<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    common\models\Image;

/**
 * This is the model class for table "{{%events}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $host_id
 * @property integer $live
 * @property string $name
 * @property string $date_start
 * @property string $time_start
 * @property string $description
 * @property integer $max_places_count
 * @property integer $min_places_count
 * @property integer $confirm_status
 * @property integer $registration_end
 * @property integer $payment_per_person
 * @property integer $step
 * @property integer $status
 * @property integer $wifi
 * @property integer $smoke
 * @property integer $food
 * @property integer $balcony
 * @property integer $tv_show_id
 * @property integer $conditioner
 * @property string $created_at
 * @property string $updated_at
 */
class Events extends \yii\db\ActiveRecord
{
    const STATUS_START_CREATE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_NOT_ACTIVE = 3;
    const STATUS_DELETED = 4;

    const STATUS_CONFIRM_YES = 1;
    const STATUS_CONFIRM_NO = 2;

    const SMOKE_YES = 1;
    const SMOKE_NO = 2;

    const SCENARIO_STEP_EVENT = 'events';
    const SCENARIO_STEP_LOCATION = 'location';
    const SCENARIO_STEP_REG_AND_PAY = 'reg_and_pay';
    const SCENARIO_STEP_COMFORT_AND_PREFERENCES = 'comfort_and_preferences';

    public $images;
    public $tv_show_name;

    public static $registrationEnd = [
        2 => 2, 4 => 4, 6 => 6, 8 => 8, 10 => 10,
        12 => 12, 14 => 14, 16 => 16, 18 => 18, 20 => 20,
        22 => 22, 24 => 24
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%events}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'host_id', 'date_start', 'time_start'], 'required'],
            [
                [
                    'user_id', 'host_id', 'max_places_count', 'min_places_count', 'registration_end',
                    'payment_per_person', 'step', 'status', 'confirm_status', 'tv_show_id', 'why_delete',
                    'balcony'
                ],
                'integer'
            ],
            [['date_start', 'time_start', 'created_at', 'updated_at', 'images'], 'safe'],
            [['description'], 'string'],
            [['live', 'wifi', 'smoke', 'food', 'conditioner'], 'string', 'max' => 1],
            [['max_places_count', 'min_places_count'], 'string', 'min' => 1],
            [['name'], 'string', 'max' => 255],
            
            [['tv_show_id'], 'required', 'on' => self::SCENARIO_STEP_EVENT],
            [
                [
                    'max_places_count', 'confirm_status', 'registration_end', 'min_places_count', 'payment_per_person'
                ],
                'required', 'on' => self::SCENARIO_STEP_REG_AND_PAY
            ],
        ];
    }
    
    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_STEP_EVENT => ['tv_show_id', 'images', 'date_start', 'time_start', 'live', 'description'],
            self::SCENARIO_STEP_LOCATION => ['host_id'],
            'delete' => ['why_delete', 'status'],
            'update' => ['wifi', 'food', 'conditioner', 'balcony', 'description'],
            self::SCENARIO_STEP_REG_AND_PAY => ['max_places_count', 'images', 'confirm_status', 'registration_end', 'min_places_count', 'payment_per_person'],
            self::SCENARIO_STEP_COMFORT_AND_PREFERENCES => ['wifi', 'smoke', 'food', 'conditioner', 'balcony'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'host_id' => Yii::t('app', 'Host ID'),
            'live' => Yii::t('app', 'Live'),
            'name' => Yii::t('app', 'Name'),
            'date_start' => Yii::t('app', 'Data'),
            'time_start' => Yii::t('app', 'Ore'),
            'description' => Yii::t('app', 'Descrizione'),
            'confirm_status' => Yii::t('app', 'Confirm status'),
            'max_places_count' => Yii::t('app', 'Posti disponbili'),
            'min_places_count' => Yii::t('app', 'Minimo persone per aviazione evento'),
            'registration_end' => Yii::t('app', 'Scadenza Iscrizione'),
            'payment_per_person' => Yii::t('app', 'Costo a Persona'),
            'step' => Yii::t('app', 'Step'),
            'status' => Yii::t('app', 'Status'),
            'wifi' => Yii::t('app', 'WIFI'),
            'smoke' => Yii::t('app', 'Si può ordinare cibo a casa'),
            'food' => Yii::t('app', 'Si può ordinare cibo a casa'),
            'conditioner' => Yii::t('app', 'Aria condizionata'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'balcony' => Yii::t('app', 'Si può fumare fuori'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        $this->wifi = $this->wifi === '' ? 0 : $this->wifi;
        $this->balcony = $this->balcony === '' ? 0 : $this->balcony;
        $this->food = $this->food === '' ? 0 : $this->food;
        $this->conditioner = $this->conditioner === '' ? 0 : $this->conditioner;

        return parent::beforeSave($insert);
    }
    
    /**
     * @return bool|string
     */
    public function getBookedCount()
    {
        return EventsBookings::getEventBookedCount($this->id);
    }
    
    /**
     * @return bool|string
     */
    public function getTimeToStart($userId = null)
    {
        $res = '';
        $now = date('Y-m-d H:i');
        $date_start = date('Y-m-d H:i', strtotime($this->date_start . ' ' . $this->time_start . ' +' . $this->registration_end . ' hour'));
        if ($now <= $date_start) {
            $datetime1 = date_create($now);
            $datetime2 = date_create($date_start);
            $interval = date_diff($datetime1, $datetime2);
            $res = '';
            if ($interval->d >= 1) {
                $res = '+24o';
            } else {
                $res = 
                    ($interval->h ? $interval->h . 'o ' : '') .
                    ($interval->i ? $interval->i . 'm ' : '');
            }
                    
        } else {
            $res = 'Register finished';
        }
        
        return $res;
    }
    
    /**
     * @return integer
     */
    public function getAllNotificationsCount($userId)
    {
        return NotificationUser::find()
                ->alias('nu')
                ->joinWith(['notificationModel n'])
                ->where(['nu.user_id' => $userId, 'n.parent_object_id' => $this->id])
                ->andWhere(['n.type' => Notification::$typesEventsArray])
                ->andWhere([
                    'OR',
                    ['nu.status' => null],
                    ['nu.status' => 0],
                ])
                ->count();
    }
    
    /**
     * @return bool|string
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : (!Yii::$app->user->isGuest ? Yii::$app->user->id : null);

        return $userId == $this->user_id ? true : null;
         
    }
    
    /**
     * @return bool|string
     */
    public function getEventsImages()
    {
        $arrayEventsImage = $this->eventsImagesModel ? ArrayHelper::map($this->eventsImagesModel, 'id', 'path') : [];
        $arrayHostsImage = $this->hostModel && ($images = Image::findAll(['relation_id' => $this->hostModel->id])) ?  
                ArrayHelper::map($images, 'id', 'path') : [];
        $defaultArray = [];
        if (!$arrayEventsImage && !$arrayHostsImage) {
            $defaultArray = ['images/default_avatar.png'];
        }
        return array_merge($arrayEventsImage, $arrayHostsImage, $defaultArray);
    }
    
    /**
     * @return bool|string
     */
    public function getName()
    {
        return $this->tvShowModel ? $this->tvShowModel->name : $this->name;
    }
    
    /**
     * @return string
     */
    public function getDefaultImage()
    {
        $image = EventsImages::find()->where(['event_id' => $this->id])->one();
        return $image ? '/' . $image->path : '/images/default_avatar.png';
    }
    
    
    /**
     * @return string
     */
    public function getPaymentPerPerson()
    {
        return '€' . number_format($this->payment_per_person, 2, ',', '');
    }
    
    /**
     * @return string
     */
    public function getNotFinishedLink()
    {
        return \yii\helpers\Url::to(['/profile/events/create', 'id' => $this->id, 'step' => ((int)$this->step + 1)]);
    }

    /**
     * @return bool
     */
    public function getFeedbackResult()
    {
        $result = [
            'location' => 0,
            'hoster' => 0,
            'organization' => 0,
            'hospitality' => 0,
            'cleaning' => 0,
            'fun' => 0,
        ];
                
        if ($this->feedbackModel) {
            $model = Feedback::find()
                ->alias('f')
                ->select([
                    'locSum' => '(sum(f.location) / count(f.id))',
                    'hostSum' => '(sum(f.hoster) / count(f.id))',
                    'orgSum' => '(sum(f.organization) / count(f.id))',
                    'hospSum' => '(sum(f.hospitality) / count(f.id))',
                    'clearSum' => '(sum(f.cleaning) / count(f.id))',
                    'funSum' => '(sum(f.fun) / count(f.id))',
                ])
                ->where(['event_id' => $this->id])
                ->one();

            $result = [
                'location' => $model->locSum,
                'hoster' => $model->hostSum,
                'organization' => $model->orgSum,
                'hospitality' => $model->hospSum,
                'cleaning' => $model->clearSum,
                'fun' => $model->funSum,
            ];
        }

        return $result;
    }
    
    /**
     * @return bool
     */
    public function checkPermitToFeedback()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        $model = Feedback::findOne(['event_id' => $this->id, 'user_id' => Yii::$app->user->id]);
        
        return (!$model && ($this->status == self::STATUS_NOT_ACTIVE) && $this->getMyEventsBooking()) ? true : false;
    }

    /**
     * @return string
     */
    public function checkPermitToShowAddress()
    {
        $status = false;
        $modelBooking = EventsBookings::find()
                ->where([
                    'status' => EventsBookings::STATUS_CONFIRM,
                    'event_id' => $this->id,
                    'user_id' => Yii::$app->user->id
                ])
                ->exists();

        if ($this->isOwner() || $modelBooking) {
            $status = true;
        }

        return $status;
    }

    /**
     * @return string
     */
    public static function getMyEventsInMapArray()
    {
        $query = self::find()
                ->alias('event')
                ->select(['id' => 'event.id', 'tv_show_name' => 'tvm.name'])
                ->joinWith(['tvShowModel tvm'])
                ->where(['user_id' => Yii::$app->user->id]);

        return ArrayHelper::map($query->all(), 'id', 'tv_show_name');
    }

    /**
     * @return string
     */
    public function getAddress($full=null)
    {
        return $this->getCityName() . ' ' . ($this->checkPermitToShowAddress() || $full ? $this->getStreetName() . ' ' . $this->getStreetNumber() : '');
    }
    
    /**
     * @return string
     */
    public function getDateStart()
    {
        return date('d F', strtotime($this->date_start)) . ' - ' . date('H:i', strtotime($this->time_start));
    }
    
    /**
     * @return string
     */
    public function getDateCreate($format = 'd.m.Y H:i')
    {
        return date($format, strtotime($this->created_at));
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->hostModel ? $this->hostModel->zip_code : '';
    }

    /**
     * @return string
     */
    public function getStreetNumber()
    {
        return $this->hostModel ? $this->hostModel->street_number : '';
    }

    /**
     * @return string
     */
    public function getStreetName()
    {
        return $this->hostModel ? $this->hostModel->street_name : '';
    }

    /**
     * @return string
     */
    public function getProvince()
    {
        return $this->hostModel ? $this->hostModel->province : '';
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->hostModel ? $this->hostModel->city : '';
    }

    public function getAvailablePlacesCount()
    {
        return (int)$this->max_places_count - (int)$this->getBookedCount();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyEventsBooking()
    {
        return EventsBookings::find()
                ->where([
                    'event_id' => $this->id,
                    'user_id' => Yii::$app->user->id
                ])
                ->all();
            
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsBookings()
    {
        return EventsBookings::find()
                ->where([
                    'event_id' => $this->id,
                    'status' => [
                        EventsBookings::STATUS_ON_CONFIRM,
                        EventsBookings::STATUS_CONFIRM,
                        EventsBookings::STATUS_REJECTED,
                    ]
                ])
                ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsBookingModel()
    {
        return $this->hasMany(EventsBookings::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackModel()
    {
        return $this->hasMany(Feedback::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsQuestionsModel()
    {
        return $this->hasMany(EventsQuestions::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsImagesModel()
    {
        return $this->hasMany(EventsImages::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTvShowModel()
    {
        return $this->hasOne(TvShow::className(), ['id' => 'tv_show_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostModel()
    {
        return $this->hasOne(Host::className(), ['id' => 'host_id']);
    }

    public function saveImages()
    {
        if ($this->images) {
            EventsImages::deleteAll(['event_id' => $this->id]);
            if (is_array($this->images)) {
                foreach ($this->images as $image) {
                    $image = json_decode($image);
                    $name = $image->name;
                    $path = $image->path;
                    if ($name && $path) {
                        $model = new EventsImages();
                        $model->event_id = $this->id;
                        $model->path = $path;
                        $model->name = $name;
                        $model->save();
                    }
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function saveStepData()
    {
        $oldData = self::findOne($this->id);
        switch ($this->step) {
            case 1 :
                $this->status = self::STATUS_START_CREATE;
                if ($oldData && $oldData->step > $this->step) {
                    $this->step = $oldData->step;
                }
                if ($this->save()) {
                    $this->saveImages();
                }
                break;
            case 2 :
                if ($oldData && $oldData->step > $this->step) {
                    $this->step = $oldData->step;
                }
                break;
            case 3 :
                if ($oldData && $oldData->step > $this->step) {
                    $this->step = $oldData->step;
                }
                $this->saveImages();
                break;
            case 4 :
                if ($oldData && $oldData->step > $this->step) {
                    $this->step = $oldData->step;
                }
                $this->status = self::STATUS_ACTIVE;
                break;
        }

        return $this->save();
    }
    
    public function getLink($full=null)
    {
        return $full ? \Yii::$app->urlManager->createAbsoluteUrl('/events/' . $this->id) : '/events/' . $this->id;
    }
    
}
