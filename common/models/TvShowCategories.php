<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tv_show_categories}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class TvShowCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tv_show_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public static function getAllWithCategory()
    {
        $query = self::find();

        $arrayRes = [];
        $selected = 0;
        foreach ($query->all() as $model) {
            if ($tvModels = $model->tvShowModel) {
                $selected = $selected ? $selected : $tvModels[0]->id;
                $arrayRes[$model->name] = ArrayHelper::map($tvModels, 'id', 'name');
            }
        }

        return ['selected' => $selected, 'data' => $arrayRes];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTvShowModel()
    {
        return $this->hasMany(TvShow::className(), ['category_id' => 'id']);
    }
}
