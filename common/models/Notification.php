<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%notification}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $object_id
 * @property integer $parent_object_id
 * @property string $note
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Notification extends \yii\db\ActiveRecord
{
    //for host
    const TYPE_ADMIN = 1;
    const TYPE_EVENT_BOOKING = 2; 
    const TYPE_EVENT_INFO = 3;
    const TYPE_MESSAGE = 4;
    const TYPE_EVENT_BOOKING_DELETE = 5;
    const TYPE_EVENT_QUESTION_ADDED = 6;
    const TYPE_EVENT_FEEDBACK = 7;

    //for user
    const TYPE_EVENT_BOOKING_ADD = 8;
    const TYPE_EVENT_DELETE = 9;
    const TYPE_EVENT_BOOKING_CONFIRM = 10;
    const TYPE_EVENT_BOOKING_REJECT = 11;
    
    const STATUS_READ = 1;

    public static $typesEventsArray = [
        self::TYPE_EVENT_BOOKING,
        self::TYPE_EVENT_BOOKING_DELETE,
        self::TYPE_EVENT_QUESTION_ADDED,
        self::TYPE_EVENT_FEEDBACK,
        self::TYPE_EVENT_BOOKING_ADD,
        self::TYPE_EVENT_DELETE,
        self::TYPE_EVENT_BOOKING_CONFIRM,
        self::TYPE_EVENT_BOOKING_REJECT
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'user_id', 'object_id', 'parent_object_id'], 'integer'],
            [['note'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'user_id' => Yii::t('app', 'User ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'note' => Yii::t('app', 'Note'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function getTemplateMessage()
    {
        $message = '';
        switch ($this->type) {
            case self::TYPE_EVENT_BOOKING_CONFIRM:
                if($eventsBooking = EventsBookings::findOne($this->object_id)){
                    $message = ($eventsBooking->eventsModel && $eventsBooking->eventsModel->userModel ? $eventsBooking->eventsModel->userModel->getUsernameWithSurname() : '') . ' ' .
                             'ha accettato la tua richiesta di prenotazione di '
                            . $eventsBooking->getItemCount()
                            . ' posti nel suo evento ' .
                                ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getName() : '') . 
                            ' del ' . ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getDateStart() : '')
                            . ' Ricordati che il prezzo totale da pagare и '
                            . ((int)$eventsBooking->getItemCount() * ($eventsBooking->eventsModel ? (float)$eventsBooking->eventsModel->payment_per_person : 1)) . ','
                            . ' controlla la posta ti sono state mandati maggior informazione sul hoster e la location dell`evento ';
                }

                break;
            case self::TYPE_EVENT_BOOKING_REJECT:
                if($eventsBooking = EventsBookings::findOne($this->object_id)){
                    $message = ($eventsBooking->eventsModel && $eventsBooking->eventsModel->userModel ? $eventsBooking->eventsModel->userModel->getUsernameWithSurname() : '') . ' ' .
                        'ha appena cancellato la tua prenotazione nel suo evento ' . 
                            ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getName() : '') . 
                        ' del ' . ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getDateStart() : '')
                        . ' ti verrа rimborsata la somme pagata senza penali. ';
                }
                break;
            case self::TYPE_EVENT_DELETE:
                if($events = Events::findOne($this->object_id)){
                    $message = ($events->userModel ? $events->userModel->getUsernameWithSurname() : '') . 
                            ' ha appena cancellato il suo evento ' . $events->getName() . ' del ' . 
                            $events->getDateStart(). ' ti verrа rimborsata la somme pagata senza penali.';
                }
                break;
            case self::TYPE_EVENT_BOOKING_ADD:
                if($eventsBooking = EventsBookings::findOne($this->object_id)){
                    $confirmStatus = $eventsBooking->eventsModel ? $eventsBooking->eventsModel->confirm_status : '';
                    $message = '<div>Hai appenna prenotato ' . ($eventsBooking->getItemCount()) .
                            ' posti nell`evento di ' . 
                            ($eventsBooking->eventsModel && $eventsBooking->eventsModel->userModel ? $eventsBooking->eventsModel->userModel->getUsernameWithSurname() : '') . ' ' .
                            ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getName() : '') . 
                            ' del ' . ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getDateStart() : '') .
                            ' Ricordati che il prezzo totale da pagare и ' . 
                            ((int)$eventsBooking->getItemCount() * ($eventsBooking->eventsModel ? (float)$eventsBooking->eventsModel->payment_per_person : 1)) .
                            ', inoltre controlla la posta ' .
                            'ti sono state mandati maggior informazione sul hoster e la location dell`evento.'
                            . ' Location dell`evento: ' . ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getAddress(true) : '')
                            . '</div>';
                }
                break;

            case self::TYPE_EVENT_FEEDBACK:
                if($eventFeedback = Feedback::findOne($this->object_id)){
                    $message = ($eventFeedback->userModel ? $eventFeedback->userModel->getUsernameWithSurname() : '') .
                            ' ti ha lasciato un feedback ';
                }
                break;

            case self::TYPE_EVENT_QUESTION_ADDED:
                if($eventQuestion = EventsQuestions::findOne($this->object_id)){
                    $message = ($eventQuestion->userModel ? $eventQuestion->userModel->getUsernameWithSurname() : '') 
                        . ' ti Ha fatto una domanda pubblica sulla bacheca del tuo evento ' . 
                            ($eventQuestion->eventsModel ? $eventQuestion->eventsModel->getName() : '') 
                            . ' del ' . 
                                ($eventQuestion->eventsModel ? $eventQuestion->eventsModel->getDateStart() : '');
                }
                break;

            case self::TYPE_EVENT_BOOKING_DELETE:
                if($eventsBooking = EventsBookings::findOne($this->object_id)){
                    $message =  ($eventsBooking->userModel ? $eventsBooking->userModel->getUsernameWithSurname() : '') .
                            ' Ha cancellato la prenotazione di ' . $eventsBooking->getItemCount() . ' posti nel'.
                            ' tuo evento '.
                            ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getName() : '') .
                            'del' .
                            ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getDateStart() : '') .
                            ' Verrai remborsato con le somme di penali in caso previsti.';
                }
                break;

            case self::TYPE_EVENT_BOOKING:
                                
                if($eventsBooking = EventsBookings::findOne($this->object_id)){
                    $items = '';
                    if ($eventsBooking->bookingItemsModel) {
                        foreach ($eventsBooking->bookingItemsModel as $bookItem) {
                            $items .= ' - ' . $bookItem->name . ' ' .
                                    ($bookItem->gender == 0 ? 'M' : 'F') . ' ' . 
                                    ($bookItem->age) . '<br>';
                        }
                    }

                    $confirmStatus = $eventsBooking->eventsModel ? $eventsBooking->eventsModel->confirm_status : '';
                    $text = ' Ha prenotato ';
                    if ($confirmStatus == Events::STATUS_CONFIRM_YES) {
                        $text = ' Chiede di prenotare ';
                    }
                    $message = '<div>' .
                            ($eventsBooking->userModel ? $eventsBooking->userModel->getUsernameWithSurname() : '') . 
                            ' ' . $text . ' '. ($eventsBooking->getItemCount()) . 
                            ' nel tuo evento ' . 
                            ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getName() : '') 
                            . ' del ' . 
                                ($eventsBooking->eventsModel ? $eventsBooking->eventsModel->getDateStart() : '')
                            . ' i participanti sono: <br>'
                            . $items . 
                        '</div>';
                }
                
                break;
        }

        return $message;
    }

    public static function saveNotification($type, $for, $parent_object_id, $object_id = null, $user_id = null)
    {
        $model = new self;
        $model->type = $type;
        $model->user_id = $user_id;
        $model->object_id = $object_id;
        $model->parent_object_id = $parent_object_id;
        if ($model->save()) {
            $message = $model->getTemplateMessage();
            if ($for && is_array($for)) {
                foreach ($for as $key => $email) {
                    $modelNew = new NotificationUser();
                    $modelNew->notification_id = $model->id;
                    $modelNew->user_id = $key;
                    $modelNew->note = $message;
                    if ($modelNew->save() && $modelNew->userModel && $modelNew->userModel->email) {
                        EmailSender::sendEmail($modelNew->userModel->email, 'Notification', 'text', ['text' => $message]);
                    }
                }
            }
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationUserModel()
    {
        return $this->hasMany(NotificationUser::className(), ['user_id' => 'id']);
    }

}