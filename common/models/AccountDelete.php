<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%acocunt_delete}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $why
 * @property string $why_not
 * @property string $created_at
 * @property string $updated_at
 */
class AccountDelete extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acocunt_delete}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'why'], 'integer'],
            [['why_not'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'why' => Yii::t('app', 'Why'),
            'why_not' => Yii::t('app', 'Why Not'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
