<?php

namespace common\helpers;

use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;

class UploadHelper
{
    const UPLOAD_PATH = 'uploads';
    const TEMP_PATH = 'tmp';

    public static function getTempFilePath($filename)
    {
        return Yii::getAlias("@webroot/" . self::UPLOAD_PATH . '/' . self::TEMP_PATH . "/" . $filename);
    }

    public static function getTempPath()
    {
        return Yii::getAlias("@webroot/" . self::UPLOAD_PATH . '/' . self::TEMP_PATH);
    }

    public static function getUploadPath($path = null)
    {
        return Yii::getAlias("@webroot/" . self::UPLOAD_PATH . ($path === null ? "" : "/" . $path));
    }

    public static function getUploadProtectedPath($path = null)
    {
        return Yii::getAlias("@app/../" . self::UPLOAD_PATH . ($path === null ? "" : "/" . $path));
    }

    public static function getUploadUrl($path = null)
    {
        return Url::to(self::UPLOAD_PATH . ($path === null ? "" : "/" . $path), true);
    }

    public static function getTmpUrl($path = null)
    {
        return Yii::getAlias("@web/" . self::UPLOAD_PATH . '/' . self::TEMP_PATH . ($path === null ? "" : "/" . $path));
    }

    public static function saveImage($imgPath)
    {
        $result = [];
        $error = 'Init error';
        $types = ['jpg', 'jpeg', 'png'];
        if (isset($_FILES) && !empty($_FILES)) {

            //@todo Implement one file uploading
            $files = $_FILES['file'];

            $i = 0;
            foreach($files['name'] as $fileName) {

                if ($files['size'][$i] <= 5242880) { //5 Mb

                    $name = static::generateRandomName();
                    if (file_exists($files['tmp_name'][$i])) {
                        $type = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
                        if (in_array($type, $types)) {
                            try {
                                $im = $type == "png" ? imagecreatefrompng($files['tmp_name'][$i]) : imagecreatefromjpeg($files['tmp_name'][$i]);

                                if ($type == 'png') {
                                    try {
                                        imagealphablending($im, true);
                                        imagesavealpha($im, true);
                                    } catch (\Exception $e) {
                                        Yii::error("GD library not available");
                                    }
                                }

                                //output image
                                $path = UploadHelper::getUploadPath() . "/" . $imgPath;
                                $filePath = $path . "/" . $name . "." . $type;
                                if (!file_exists($path)) FileHelper::createDirectory($path, 777);
                                $type == "png" ? imagepng($im, $filePath) : imagejpeg($im, $filePath);
                                $result[] = [true, $name . '.' . $type];
                            } catch (\Exception $e) {
                                $error = "Can't process file '" . $fileName . "'";
                                Yii::warning($error . ": " . $e->getMessage(), 'S3');
                            }
                        } else {
                            $error = "This file format is not allowed!";
                        }
                    } else {
                        $error = "Upload error '" . $fileName . "'";
                    }
                } else {
                    $error = "Max file size allowed 10Mb. File '" . $fileName . "' not uploaded";
                }
                $i++;
            }
            if (!empty($result)) {
                return [true, $result];
            }
            return [false, $error];
        }
        return [false, 'No file uploaded!'];
    }

    public static function saveBase64Image($base64Image, $imgPath)
    {
        $name = static::generateRandomName();
        $path = UploadHelper::getUploadPath() . "/" . $imgPath;
        $type = 'png';
        $filePath = $path . "/" . $name . "." . $type;

        ImageHelper::base64ToFile($base64Image, $filePath);
        return $name . "." . $type;
    }

    public static function generateRandomName()
    {
        return round(microtime(true) * 1000) . rand(100, 999);
    }
}