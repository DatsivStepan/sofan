<?php

namespace common\helpers;

use Yii;
use yii\console\Application as ConsoleApplication;

class ImageHelper
{
    /*
     * Crop-to-fit PHP-GD
     * http://salman-w.blogspot.com/2009/04/crop-to-fit-image-using-aspphp.html
     *
     * Resize and center crop an arbitrary size image to fixed width and height
     * e.g. convert a large portrait/landscape image to a small square thumbnail
     */
    public static function CropToFit($source_path, $destination_path, $desiredImageWidth, $desiredImageHeight)
    {
        list($source_width, $source_height, $source_type) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;
            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
            default:
                return false;
        }

        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $desiredImageWidth / $desiredImageHeight;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            /*
             * Triggered when source image is wider
             */
            $temp_height = $desiredImageHeight;
            $temp_width = ( int )($desiredImageHeight * $source_aspect_ratio);
        } else {
            /*
             * Triggered otherwise (i.e. source image is similar or taller)
             */
            $temp_width = $desiredImageWidth;
            $temp_height = intval($desiredImageWidth / $source_aspect_ratio);
        }

        /*
         * Resize the image into a temporary GD image
         */

        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);

        $background = imagecolorallocatealpha($temp_gdim, 255, 255, 255, 127);
        imagecolortransparent($temp_gdim, $background);
        imagealphablending($temp_gdim, false);
        imagesavealpha($temp_gdim, true);

        imagecopyresampled(
            $temp_gdim,
            $source_gdim,
            0, 0,
            0, 0,
            $temp_width, $temp_height,
            $source_width, $source_height
        );

        /*
         * Copy cropped region from temporary image into the desired GD image
         */

        $x0 = ($temp_width - $desiredImageWidth) / 2;
        $y0 = ($temp_height - $desiredImageHeight) / 2;
        $desired_gdim = imagecreatetruecolor($desiredImageWidth, $desiredImageHeight);

        $background = imagecolorallocatealpha($desired_gdim, 255, 255, 255, 127);
        imagecolortransparent($desired_gdim, $background);
        imagealphablending($desired_gdim, false);
        imagesavealpha($desired_gdim, true);

        imagecopy(
            $desired_gdim,
            $temp_gdim,
            0, 0,
            $x0, $y0,
            $desiredImageWidth, $desiredImageHeight
        );

        switch ($source_type) {
            case IMAGETYPE_GIF:
                imagegif($desired_gdim, $destination_path);
                return true;
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($desired_gdim, $destination_path);
                return true;
                break;
            case IMAGETYPE_PNG:
                imagepng($desired_gdim, $destination_path);
                return true;
                break;
        }
        return false;
    }

    public static function ResizeToFit($source_path, $destination_path, $maxImageWidth, $maxImageHeight = null)
    {
        list($source_width, $source_height, $source_type) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;
            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
            default:
                return false;
        }

        $source_aspect_ratio = $source_width / $source_height;
        $temp_width = $source_width;
        $temp_height = $source_height;
        if ($temp_width > $maxImageWidth) {
            $temp_width = $maxImageWidth;
            $temp_height = $temp_width / $source_aspect_ratio;
        }
        if ($maxImageHeight !== null && $temp_height > $maxImageHeight) {
            $temp_height = $maxImageHeight;
            $temp_width = $temp_height * $source_aspect_ratio;
        }
        /*
         * Copy cropped region from temporary image into the desired GD image
         */
        $desired_gdim = imagecreatetruecolor($temp_width, $temp_height);

        $background = imagecolorallocatealpha($desired_gdim, 255, 255, 255, 127);
        imagecolortransparent($desired_gdim, $background);
        imagealphablending($desired_gdim, false);
        imagesavealpha($desired_gdim, true);

        imagecopyresampled(
            $desired_gdim,
            $source_gdim,
            0, 0,
            0, 0,
            $temp_width, $temp_height,
            $source_width, $source_height
        );

        switch ($source_type) {
            case IMAGETYPE_GIF:
                imagegif($desired_gdim, $destination_path);
                return true;
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($desired_gdim, $destination_path);
                return true;
                break;
            case IMAGETYPE_PNG:
                imagepng($desired_gdim, $destination_path);
                return true;
                break;
        }
        return false;
    }

    /**
     * @todo imlemet methods from http://salman-w.blogspot.com/2008/10/resize-images-using-phpgd-library.html
     */

    /**
     * @param $src
     * @param int $width
     * @param int $height
     * @return bool|string
     */
    public static function ThumbImage($src, $width = 256, $height = null, $cropp = false)
    {
        if (!file_exists($src)) {
            return $src;
        }
        $dist = (Yii::$app instanceof ConsoleApplication ? Yii::getAlias("@webroot") . '/' : '') . UploadHelper::UPLOAD_PATH . "/thumbs";
        if (!file_exists($dist)) {
            mkdir($dist);
        }

        $pathinfo = pathinfo($src);
        $basename = $pathinfo['basename'];
        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : false;
        if (strlen($ext) > 4) {
            $basename .= "." . $ext;
            $ext = false;
        }

        $base = UploadHelper::getUploadPath();
        $r = strpos($src, $base . "/");
        $image = $src;
        if ($r !== false) {
            $image = substr($src, strlen($base) + 1);
        }
        // Removes special chars.
        $pattern = '/[^A-Za-z0-9\-\_]/';
        $distFile = preg_replace($pattern, '-', str_replace(' ', '-', $image)) . "-" . $width . "_" . (!$height ? "r" : $height) . "." . ($ext ? preg_replace($pattern, '', $ext) : "jpg");

        $distFolder = $distFile[0] . $distFile[1];

        $destination = $dist . "/" . $distFolder . "/" . $distFile;
        if (!file_exists($destination)) {
            if (!file_exists($dist . "/" . $distFolder)) {
                mkdir($dist . "/" . $distFolder, 0777, true);
            }
            if (!$height) {
                $imageinfo = getimagesize($src);
                $height = $width * $imageinfo[0] / $imageinfo[1];
            }

            if ($cropp) {
                self::CropToFit($src, $destination, $width, $height);
            } else {
                self::ResizeToFit($src, $destination, $width, $height);
            }
        }

        return Yii::$app instanceof ConsoleApplication ?
            UploadHelper::getUploadUrl(substr($destination, strlen(Yii::getAlias("@webroot") . '/' . UploadHelper::UPLOAD_PATH) + 1)) :
            UploadHelper::getUploadUrl(substr($destination, strlen(UploadHelper::UPLOAD_PATH) + 1));//remove "uploads/"
    }

    public static function base64ToFile($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        return true;
    }
}