<?php

use yii\helpers\Html;
use common\models\Configuration;
use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/admin/images/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= \Yii::$app->user->identity->username; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ADMIN NAVIGATION</li>
        <li>
          <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Pages</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('pages/pages/create'); ?>"><a href="/admin/pages/pages/create"><i class="fa fa-circle-o"></i> Add page</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('pages/pages/index'); ?>"><a href="/admin/pages/pages/index"><i class="fa fa-circle-o"></i> List pages</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('users/users/create'); ?>"><a href="/admin/users/users/create"><i class="fa fa-circle-o"></i> Add user</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('users/users/index'); ?>"><a href="/admin/users/users/index"><i class="fa fa-circle-o"></i> List users</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span>Settings</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('configuration/configuration/index'); ?>">
                <a href="/admin/configuration/configuration/index"><i class="fa fa-circle-o"></i> Configuration</a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('language/source-message/index'); ?>">
                <a href="/admin/language/source-message/index"><i class="fa fa-circle-o"></i> Translation</a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('language/language/index'); ?>">
                <a href="/admin/language/language/index"><i class="fa fa-circle-o"></i> Languages</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>Posts</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('posts/posts/create'); ?>"><a href="/admin/posts/posts/create"><i class="fa fa-circle-o"></i> Add post</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('posts/posts/index'); ?>"><a href="/admin/posts/posts/index"><i class="fa fa-circle-o"></i> List posts</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>