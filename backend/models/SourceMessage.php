<?php

namespace backend\models;

use Yii;
use common\models\Message;
use common\components\AdminGetParameter;
/**
 * This is the model class for table "{{%source_message}}".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends \common\models\SourceMessage
{
    public $messagesModels;
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        Message::deleteAll(['id' => $this->id]);
        foreach($this->messagesModels as $language => $translation){
            $modelNewMessage = new Message();
            $modelNewMessage->id = $this->id;
            $modelNewMessage->language = $language;
            $modelNewMessage->translation = $translation;
            $modelNewMessage->save();
        }
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->loadMessages();
    }
    
    public function loadMessages() {
        $modelGetLang = new AdminGetParameter();
        $arrayMessage = \yii\helpers\ArrayHelper::map($this->messages, 'language', 'translation');
        foreach($modelGetLang->getLanguageArray() as $lang_code => $lang){
            if(array_key_exists($lang_code, $arrayMessage)){
                $this->messagesModels[$lang_code] = $arrayMessage[$lang_code];
            }else{
                $this->messagesModels[$lang_code] = '';                
            }
        }
    }
    
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['messagesModels'], 'safe'],

        ]);
    }
}
