<?php

namespace backend\modules\language\controllers;

use yii\web\Controller;

/**
 * Default controller for the `language` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
