<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Configuration */

$this->title = Yii::t('app', 'Create Configuration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Configurations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
    
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="configuration-create">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
        
    </section>
