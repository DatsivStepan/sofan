<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $model common\models\Pages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View "<?= Html::encode($this->title) ?>"
      </h1>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?php /* ?>
            <ol class="breadcrumb">
              <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a href="index">Pages</a></li>
              <li class="active"><?= $this->title; ?></li>
            </ol>
        <?php */ ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        <div class="pages-view">

            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'content:ntext',
                    'sort',
                    'date_create',
                    'date_update',
                    'slug',
                ],
            ]) ?>

        </div>    
    </section>  
